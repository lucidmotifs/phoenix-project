import json
from anytree import Node, RenderTree, Resolver, NodeMixin
from anytree.importer import JsonImporter


class Resource(Message, NodeMixin):
	pass


class TypeNode(type, NodeMixin):
	pass


class Ontology(object):

	def __init__(self, type, data):
		importer = JsonImporter()
		self.node = Node('root', parent=None)
		self.node.children = [importer.import_(data)]


def main():
	data = 'google_ads_ontology.json'
	graph = GoogleAdsOntology(data)
	print(RenderTree(tree))

