import json

from anytree.importer import JsonImporter
from anytree import RenderTree


data = {
    "name": "productDimension",
    "children": [{
        "tag": "brand",
        "name": "ProductBrand",
        "children": [{
            "tag": "brand",
            "name": "BRAND",
        }],
    }, {
        "tag": "item_id",
        "name": "ProductItemId",
        "children": [{
            "tag": "id",
            "name": "ITEM_ID",
        }],
    }, {
        "tag": "product_type",
        "name": "ProductType",
        "category": "Taxonomy",
        "source": "User",
        "children": [
            {"tag": "product_type_l1", "name": "PRODUCT_TYPE_L1"},
            {"tag": "product_type_l2", "name": "PRODUCT_TYPE_L2"},
            {"tag": "product_type_l3", "name": "PRODUCT_TYPE_L3"},
            {"tag": "product_type_l4", "name": "PRODUCT_TYPE_L4"},
            {"tag": "product_type_l5", "name": "PRODUCT_TYPE_L5"},
        ],
    }, {
        "tag": "category",
        "name": "ProductBiddingCategory",
        "category": "Taxonomy",
        "source": "Platform",
        "children": [
            {"tag": "category_l1", "name": "BIDDING_CATEGORY_L1"},
            {"tag": "category_l2", "name": "BIDDING_CATEGORY_L2"},
            {"tag": "category_l3", "name": "BIDDING_CATEGORY_L3"},
            {"tag": "category_l4", "name": "BIDDING_CATEGORY_L4"},
            {"tag": "category_l5", "name": "BIDDING_CATEGORY_L5"},
        ],
    }],
}

importer = JsonImporter()

base = importer.import_(json.dumps(data))

print(RenderTree(base))
