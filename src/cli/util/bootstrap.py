# adspert cli app
import aenum
import asyncio
import dataclasses
import enum
import functools
import logging
import prompt_toolkit as prompt
import subprocess
import sys
import textwrap
from typing import Any
from typing import Dict
from typing import List

import fire
import tabulate
import pandas as pd
from devtools import debug
from pssh.utils import load_private_key
from pssh.clients import ParallelSSHClient

from adspert.base.app import adspert_app
from adspert.database.db import configure_db, dbs, close_dbs
from adspert.scripts.utils import get_account
from adspert.database.models import main
from adspert.database.models import account
from adspert.base.utils import masked
from adspert.database.models.account import AdGroup

"""
Use the following magic function in ipython to reload modules.
%load_ext autoreload
%autoreload 2
"""

class AccountDbHost(aenum.Enum):

    ADB01 = aenum.auto(), 'adb-01.dc1.adspert.de', 9001
    ADB02 = aenum.auto(), 'adb-02.dc1.adspert.de', 9002
    ADB03 = aenum.auto(), 'adb-03.dc1.adspert.de', 9003
    ADB04 = aenum.auto(), 'adb-04.dc1.adspert.de', 9004
    ADB05 = aenum.auto(), 'adb-05.dc1.adspert.de', 9005
    ADB06 = aenum.auto(), 'adb-06.dc1.adspert.de', 9006
    ADB07 = aenum.auto(), 'adb-07.dc1.adspert.de', 9007
    ADB08 = aenum.auto(), 'adb-08.dc1.adspert.de', 9008
    ADB09 = aenum.auto(), 'adb-09.dc1.adspert.de', 9009
    ADB10 = aenum.auto(), 'adb-10.dc1.adspert.de', 9010
    ADB11 = aenum.auto(), 'adb-11.dc1.adspert.de', 9011


class Ip(str, aenum.Enum):

     ACCOUNT =      '192.168.1.112'
     MAIN =         '192.168.1.107'
     LOCAL =        '127.0.0.1'


class db:

    @classmethod
    def connect_main(cls):
        print('Configuring MainDB')
        configure_db()


class app:

    @classmethod
    def init(cls):
        print('Initializing AdspertApp')
        adspert_app.init('scripts', 'production')


def render(data, paging: bool = True):

    def datatable(data):
        table = tabulate.tabulate(data, headers='keys')
        return table

    def pager(out):
        pager = subprocess.Popen(
            ['less', '-F', '-R', '-S', '-X', '-K'],
            stdin=subprocess.PIPE,
            stdout=sys.stdout)
        pager.stdin.write(out.encode())
        pager.stdin.close

    output = datatable(data)
    if paging:
        return pager(output)
    print(output)


def adspert_init():
    print('Initializing AdspertApp')
    adspert_app.init('scripts', 'production')
    print('Configuring MainDB')
    configure_db()


def account_setup(account: main.Account):
    print('Configuring account DB')
    dbs.account.database = account.db_name
    dbs.account.setup(account)
    dbs.account.connect_params.update({
        'port': 5432,
    })
    debug(dbs.account.connect_params)
    # dbs.account.connect()
    return dbs


def account_adgroups(adgroup_type=None):
    q = AdGroup.select()
    if adgroup_type:
        q = q.where(AdGroup.adgroup_type == adgroup_type)
    return q

def smoke_test():
    testdata: List[Dict[str, Any]]
    testdata = account.AccountDbModel.fetchall(
        """
        SELECT
            campaign_id,
            campaign_name,
            aw_campaign_type,
            status FROM campaign
        WHERE status = 'Active'
    """)
    render(testdata, paging=True)


def bootstrap(adspert_id=3281857):

    try:
        adspert_init()
        print(f'Fetching account record for {adspert_id}')
        acc = get_account(adspert_id)
        conn = account_setup(acc)

        # smoke tests should not require
        # optimization schema
        print('Running smoke test')
        smoke_test()
    except Exception as e:
        debug(e)
    finally:
        print('Ready.')

    # sync_product_partition_structure(acc)
    # debug(task)
    # stree = ProductPartitionTree()
    # stree = build_product_group_structure()
    # debug(stree.render())


if __name__ == '__main__':
    bootstrap()
