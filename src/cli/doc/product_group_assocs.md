```SQL
select * from adw_product_group_associations_v;
```

Interesting to note that the parent id column isn't relevant here because the same case value ends up with the same number of associations regardless.

What this query really does is map `case_value` to `criterion_id` and then provide a list of product that would be associated, were it an option for them.

| criterion_id | parent_id    | count | dimension_type     | dimension_value |
| ------------ | ------------ | ----- | ------------------ | --------------- |
| 301687087156 | 293946777986 | 3487  | BRAND              | ikea            |
| 863888060079 | 293946777986 | 15    | BRAND              | möbeldena       |
| 294680686006 | 293946777986 | 0     | BRAND              | Other           |
| 310724742898 | 293946777986 | 17042 | CUSTOM_ATTRIBUTE_0 | 0 - 5           |
| 580215119015 | 293946777986 | 58    | CUSTOM_ATTRIBUTE_0 | > 10000         |
| 387072654879 | 293946777986 | 2012  | CUSTOM_ATTRIBUTE_0 | 1000 - 2000     |
| 306907463254 | 293946777986 | 11872 | CUSTOM_ATTRIBUTE_0 | 100 - 150       |
| 314511198135 | 293946777986 | 25060 | CUSTOM_ATTRIBUTE_0 | 10 - 15         |
| 306907463414 | 293946777986 | 7014  | CUSTOM_ATTRIBUTE_0 | 150 - 200       |
| 312275739253 | 293946777986 | 25277 | CUSTOM_ATTRIBUTE_0 | 15 - 20         |
| 580164442962 | 293946777986 | 129   | CUSTOM_ATTRIBUTE_0 | 5000 - 10000    |
| 317965195853 | 293946777986 | 4039   | CUSTOM_ATTRIBUTE_0 | 500 - 1000              |
| 296408203571 | 293946777986 | 18823  | CUSTOM_ATTRIBUTE_0 | 50 - 75                 |
| 311676057355 | 293946777986 | 26781  | CUSTOM_ATTRIBUTE_0 | 5 - 10                  |
| 426458883731 | 293946777986 | 11828  | CUSTOM_ATTRIBUTE_0 | 75 - 100                |
| 297612067635 | 293946777986 | 0      | CUSTOM_ATTRIBUTE_0 | Other                   |
| 299390633675 | 293946777986 | 1678   | PRODUCT_TYPE_L1    | auto & motorrad         |
| 388319393711 | 293946777986 | 7368   | PRODUCT_TYPE_L1    | baby & spielwaren       |
| 388319393751 | 293946777986 | 16138  | PRODUCT_TYPE_L1    | computer & software     |
| 346331357662 | 293946777986 | 233    | PRODUCT_TYPE_L1    | erotik                  |
| 623064510896 | 293946777986 | 371    | PRODUCT_TYPE_L1    | filme, serien & bücher  |
| 310540236951 | 293946777986 | 9770   | PRODUCT_TYPE_L1    | fotografie              |
| 385316812210 | 293946777986 | 1584   | PRODUCT_TYPE_L1    | freizeit & musik        |
| 388319393911 | 293946777986 | 84680  | PRODUCT_TYPE_L1    | gesundheit & kosmetik   |
| 388319393951 | 293946777986 | 22088  | PRODUCT_TYPE_L1    | handy & telefon         |
| 299425407582 | 293946777986 | 18161  | PRODUCT_TYPE_L1    | haushalt                |
| 309813390890 | 293946777986 | 91236  | PRODUCT_TYPE_L1    | heimwerken & garten     |
| 326394989968 | 293946777986 | 207    | PRODUCT_TYPE_L1    | lebensmittel & getränke |
| 388319393991 | 293946777986 | 104    | PRODUCT_TYPE_L1    | medien & games          |
| 325709062256 | 293946777986 | 176042 | PRODUCT_TYPE_L1    | mode & schuhe           |
| 296303633664 | 293946777986 | 0      | PRODUCT_TYPE_L1    | Other                |
| 385316812930  | 293946777986 | 47556  | PRODUCT_TYPE_L1     | sonstige angebote unserer händler |
| 294573402416  | 293946777986 | 10160  | PRODUCT_TYPE_L1     | sport & outdoor                   |
| 302793279256  | 293946777986 | 9346   | PRODUCT_TYPE_L1     | unterhaltungselektronik           |
| 376597344917  | 293946777986 | 115768 | PRODUCT_TYPE_L1     | wohnen & lifestyle                |
| 657102943869  | 326527232041 | 93068  | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          |
| 660110087216  | 306907463414 | 93068  | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          |
| 655831601670  | 306907463414 | 57277  | BIDDING_CATEGORY_L1 | Heim & Garten                     |
| 655831602630  | 326527232041 | 57277  | BIDDING_CATEGORY_L1 | Heim & Garten                     |
| 655831603110  | 317965195853 | 57277  | BIDDING_CATEGORY_L1 | Heim & Garten                     |
| 730455482316  | 378628286044 | 57277  | BIDDING_CATEGORY_L1 | Heim & Garten                     |
| 655831601710  | 306907463414 | 35987  | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  |
| 655831602670  | 326527232041 | 35987  | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  |
| 655831603150  | 317965195853 | 35987  | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  |
| 761766328231  | 378628286044 | 35987  | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  |
| 655831602950  | 317965195853 | 24567  | BIDDING_CATEGORY_L1 | Möbel                             |
| 657678040965  | 326527232041 | 24567  | BIDDING_CATEGORY_L1 | Möbel                             |
| 660110087416  | 306907463414 | 24567  | BIDDING_CATEGORY_L1 | Möbel                             |
| 761766327991  | 378628286044 | 24567  | BIDDING_CATEGORY_L1 | Möbel                             |
| 655831601470  | 306907463414 | 0      | BIDDING_CATEGORY_L1 | Other                             |
| 655831602430  | 326527232041 | 0      | BIDDING_CATEGORY_L1 | Other                             |
| 655831602870  | 317965195853 | 0      | BIDDING_CATEGORY_L1 | Other                             |
| 730455481916  | 378628286044 | 0      | BIDDING_CATEGORY_L1 | Other                             |
| 823840761803  | 388319393711 | 0      | BIDDING_CATEGORY_L1 | Other                             |
| 823840761963  | 388319393711 | 4100   | BIDDING_CATEGORY_L1 | Taschen & Gepäck                  |
| 824722569485  | 299390633675 | 6      | BRAND               | carbon wrap                       |
| 824800896911  | 306907463414 | 35     | BRAND               | deante                            |
| 1017069993992 | 426458883731 | 13     | BRAND               | dema design                       |
| 824722569525  | 299390633675 | 10     | BRAND               | foliatec                          |
| 824722569565  | 299390633675 | 1      | BRAND               | germot                            |
| 823905166531  | 388319393711 | 18     | BRAND               | heimtexland                       |
| 824722569725  | 299390633675 | 20     | BRAND               | montola                           |





| criterion_id       | parent_id        | count       | dimension_type       | dimension_value         | depth   |
|----------------+---------------+---------+---------------------+---------------------------+----|
| 301687087156   | 293946777986  | 3487    | BRAND            | ikea | 2|
| 863888060079   | 293946777986  | 15      | BRAND            | möbeldena| 2       |
| 294680686006   | 293946777986  | 0       | BRAND            | Other | 2       |
| 310724742898   | 293946777986  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 2       |
| 580215119015   | 293946777986  | 58      | CUSTOM_ATTRIBUTE_0  | > 10000                           | 2       |
| 387072654879   | 293946777986  | 2012    | CUSTOM_ATTRIBUTE_0  | 1000 - 2000                       | 2       |
| 306907463254   | 293946777986  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 2       |
| 314511198135   | 293946777986  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 2       |
| 306907463414   | 293946777986  | 7014    | CUSTOM_ATTRIBUTE_0  | 150 - 200                         | 2       |
| 312275739253   | 293946777986  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 2       |
| 588293885172   | 293946777986  | 1486    | CUSTOM_ATTRIBUTE_0  | 2000 - 5000                       | 2       |
| 378628286044   | 293946777986  | 6338    | CUSTOM_ATTRIBUTE_0  | 200 - 250                         | 2       |
| 383474279466   | 293946777986  | 16925   | CUSTOM_ATTRIBUTE_0  | 20 - 25                           | 2       |
| 326527232041   | 293946777986  | 7362    | CUSTOM_ATTRIBUTE_0  | 250 - 500                         | 2       |
| 326458289436   | 293946777986  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 2       |
| 580164442962   | 293946777986  | 129     | CUSTOM_ATTRIBUTE_0  | 5000 - 10000                      | 2       |
| 317965195853   | 293946777986  | 4039    | CUSTOM_ATTRIBUTE_0  | 500 - 1000                        | 2       |
| 296408203571   | 293946777986  | 18823   | CUSTOM_ATTRIBUTE_0  | 50 - 75                           | 2       |
| 311676057355   | 293946777986  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 2       |
| 426458883731   | 293946777986  | 11828   | CUSTOM_ATTRIBUTE_0  | 75 - 100                          | 2       |
| 297612067635   | 293946777986  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 2       |
| 299390633675   | 293946777986  | 1678    | PRODUCT_TYPE_L1     | auto & motorrad                   | 2       |
| 388319393711   | 293946777986  | 7368    | PRODUCT_TYPE_L1     | baby & spielwaren                 | 2       |
| 388319393751   | 293946777986  | 16138   | PRODUCT_TYPE_L1     | computer & software               | 2       |
| 346331357662   | 293946777986  | 233     | PRODUCT_TYPE_L1     | erotik                            | 2       |
| 623064510896   | 293946777986  | 371     | PRODUCT_TYPE_L1     | filme, serien & bücher            | 2       |
| 310540236951   | 293946777986  | 9770    | PRODUCT_TYPE_L1     | fotografie                        | 2       |
| 385316812210   | 293946777986  | 1584    | PRODUCT_TYPE_L1     | freizeit & musik                  | 2       |
| 388319393911   | 293946777986  | 84680   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 2       |
| 388319393951   | 293946777986  | 22088   | PRODUCT_TYPE_L1     | handy & telefon                   | 2       |
| 299425407582   | 293946777986  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 2       |
| 309813390890   | 293946777986  | 91236   | PRODUCT_TYPE_L1     | heimwerken & garten               | 2       |
| 326394989968   | 293946777986  | 207     | PRODUCT_TYPE_L1     | lebensmittel & getränke           | 2       |
| 388319393991   | 293946777986  | 104     | PRODUCT_TYPE_L1     | medien & games                    | 2       |
| 325709062256   | 293946777986  | 176042  | PRODUCT_TYPE_L1     | mode & schuhe                     | 2       |
| 296303633664   | 293946777986  | 0       | PRODUCT_TYPE_L1     | Other                             | 2       |
| 385316812930   | 293946777986  | 47556   | PRODUCT_TYPE_L1     | sonstige angebote unserer händler | 2       |
| 294573402416   | 293946777986  | 10160   | PRODUCT_TYPE_L1     | sport & outdoor                   | 2       |
| 302793279256   | 293946777986  | 9346    | PRODUCT_TYPE_L1     | unterhaltungselektronik           | 2       |
| 376597344917   | 293946777986  | 115768  | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 2       |
| 657102943869   | 326527232041  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 3       |
| 660110087216   | 306907463414  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 3       |
| 655831601670   | 306907463414  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 3       |
| 655831602630   | 326527232041  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 3       |
| 655831603110   | 317965195853  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 3       |
| 730455482316   | 378628286044  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 3       |
| 655831601710   | 306907463414  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 3       |
| 655831602670   | 326527232041  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 3       |
| 655831603150   | 317965195853  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 3       |
| 761766328231   | 378628286044  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 3       |
| 655831602950   | 317965195853  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 3       |
| 657678040965   | 326527232041  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 3       |
| 660110087416   | 306907463414  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 3       |
| 761766327991   | 378628286044  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 3       |
| 655831601470   | 306907463414  | 0       | BIDDING_CATEGORY_L1 | Other                             | 3       |
| 655831602430   | 326527232041  | 0       | BIDDING_CATEGORY_L1 | Other                             | 3       |
| 655831602870   | 317965195853  | 0       | BIDDING_CATEGORY_L1 | Other                             | 3       |
| 730455481916   | 378628286044  | 0       | BIDDING_CATEGORY_L1 | Other                             | 3       |
| 823840761803   | 388319393711  | 0       | BIDDING_CATEGORY_L1 | Other                             | 3       |
| 823840761963   | 388319393711  | 4100    | BIDDING_CATEGORY_L1 | Taschen & Gepäck                  | 3       |
| 824722569485   | 299390633675  | 6       | BRAND               | carbon wrap                       | 3       |
| 824800896911   | 306907463414  | 35      | BRAND               | deante                            | 3       |
| 1017069993992  | 426458883731  | 13      | BRAND               | dema design                       | 3       |
| 824722569525   | 299390633675  | 10      | BRAND               | foliatec                          | 3       |
| 824722569565   | 299390633675  | 1       | BRAND               | germot                            | 3       |
| 823905166531   | 388319393711  | 18      | BRAND               | heimtexland                       | 3       |
| 824722569725   | 299390633675  | 20      | BRAND               | montola                           | 3       |
| 823905166691   | 388319393711  | 133     | BRAND               | moonworks                         | 3       |
| 1017069994032  | 426458883731  | 8       | BRAND               | navahoo premium                   | 3       |
| 1017069994072  | 426458883731  | 299     | BRAND               | nike                              | 3       |
| 745997691486   | 426458883731  | 0       | BRAND               | Other                             | 3       |
| 747450555029   | 306907463414  | 0       | BRAND               | Other                             | 3       |
| 790352685820   | 388319393711  | 0       | BRAND               | Other                             | 3       |
| 824722569325   | 299390633675  | 0       | BRAND               | Other                             | 3       |
| 825903361821   | 388319393911  | 0       | BRAND               | Other                             | 3       |
| 825903361861   | 388319393911  | 28      | BRAND               | schaumstoff-meister               | 3       |
| 823905166731   | 388319393711  | 54      | BRAND               | shirt-panda                       | 3       |
| 824800896951   | 306907463414  | 162     | BRAND               | solingen                          | 3       |
| 825903361901   | 388319393911  | 14      | BRAND               | soummé                            | 3       |
| 824722569765   | 299390633675  | 45      | BRAND               | speedwerk                         | 3       |
| 824722569805   | 299390633675  | 2335    | BRAND               | vhbw                              | 3       |
| 824800897111   | 306907463414  | 108     | BRAND               | wiltec                            | 3       |
| 872643235560   | 387072654879  | 0       | CANONICAL_CONDITION | neu                               | 3       |
| 872643235400   | 387072654879  | 0       | CANONICAL_CONDITION | Other                             | 3       |
| 865559821823   | 301687087156  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 3       |
| 865559822063   | 301687087156  | 2012    | CUSTOM_ATTRIBUTE_0  | 1000 - 2000                       | 3       |
| 864466582264   | 863888060079  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 3       |
| 865559821903   | 301687087156  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 3       |
| 865559821863   | 301687087156  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 3       |
| 864466582304   | 863888060079  | 7014    | CUSTOM_ATTRIBUTE_0  | 150 - 200                         | 3       |
| 865559822143   | 301687087156  | 7014    | CUSTOM_ATTRIBUTE_0  | 150 - 200                         | 3       |
| 865559822103   | 301687087156  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 3       |
| 864466582464   | 863888060079  | 3169    | CUSTOM_ATTRIBUTE_0  | 200 - 250                         | 3       |
| 865559822343   | 301687087156  | 3169    | CUSTOM_ATTRIBUTE_0  | 200 - 250                         | 3       |
| 865559822303   | 301687087156  | 16925   | CUSTOM_ATTRIBUTE_0  | 20 - 25                           | 3       |
| 864466582544   | 863888060079  | 7362    | CUSTOM_ATTRIBUTE_0  | 250 - 500                         | 3       |
| 865559822543   | 301687087156  | 7362    | CUSTOM_ATTRIBUTE_0  | 250 - 500                         | 3       |
| 864466582504   | 863888060079  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 3       |
| 865559822383   | 301687087156  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 3       |
| 864466582744   | 863888060079  | 4039    | CUSTOM_ATTRIBUTE_0  | 500 - 1000                        | 3       |
| 865559822783   | 301687087156  | 4039    | CUSTOM_ATTRIBUTE_0  | 500 - 1000                        | 3       |
| 864466582704   | 863888060079  | 18823   | CUSTOM_ATTRIBUTE_0  | 50 - 75                           | 3       |
| 865559822623   | 301687087156  | 18823   | CUSTOM_ATTRIBUTE_0  | 50 - 75                           | 3       |
| 865559822583   | 301687087156  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 3       |
| 865559822823   | 301687087156  | 11828   | CUSTOM_ATTRIBUTE_0  | 75 - 100                          | 3       |
| 764441227848   | 301687087156  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 3       |
| 864466582224   | 863888060079  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 3       |
| 867944330351   | 387072654879  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 3       |
| 867944330391   | 387072654879  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 3       |
| 455566126933   | 387072654879  | 0       | PRODUCT_TYPE_L1     | Other                             | 3       |
| 867944330551   | 387072654879  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 3       |
| 385316813690   | 376597344917  | 4295    | PRODUCT_TYPE_L2     | badezimmer                        | 3       |
| 325117783334   | 309813390890  | 21953   | PRODUCT_TYPE_L2     | bauen & renovieren                | 3       |
| 606408691432   | 376597344917  | 5046    | PRODUCT_TYPE_L2     | esszimmer                         | 3       |
| 606408691592   | 376597344917  | 1295    | PRODUCT_TYPE_L2     | flur & diele                      | 3       |
| 606408691632   | 376597344917  | 2510    | PRODUCT_TYPE_L2     | kinder- & jugendzimmer            | 3       |
| 302793279296   | 302793279256  | 0       | PRODUCT_TYPE_L2     | Other                             | 3       |
| 309256998619   | 309813390890  | 0       | PRODUCT_TYPE_L2     | Other                             | 3       |
| 385316813650   | 376597344917  | 0       | PRODUCT_TYPE_L2     | Other                             | 3       |
| 468349289207   | 325709062256  | 0       | PRODUCT_TYPE_L2     | Other                             | 3       |
| 606408691872   | 376597344917  | 2675    | PRODUCT_TYPE_L2     | schlafzimmer                      | 3       |
| 413232284093   | 302793279256  | 1492    | PRODUCT_TYPE_L2     | tv & zubehör                      | 3       |
| 394329862059   | 325709062256  | 15264   | PRODUCT_TYPE_L2     | uhren & schmuck                   | 3       |
| 413351137649   | 309813390890  | 7352    | PRODUCT_TYPE_L2     | werkstatt & werkzeug              | 3       |
| 606408692152   | 376597344917  | 10547   | PRODUCT_TYPE_L2     | wohnzimmer                        | 3       |
| 820878221707   | 425995585169  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 822867359750   | 297072975809  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 823777242160   | 309728275410  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 823850958919   | 780426909743  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 824176979240   | 824722569325  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 824716705370   | 310724742938  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 825506726591   | 311676057515  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 902488209314   | 314892675297  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 4       |
| 823850958959   | 780426909743  | 25596   | BIDDING_CATEGORY_L1 | Elektronik                        | 4       |
| 824176979400   | 824722569325  | 25596   | BIDDING_CATEGORY_L1 | Elektronik                        | 4       |
| 824677853979   | 314892675297  | 25596   | BIDDING_CATEGORY_L1 | Elektronik                        | 4       |
| 813414124003   | 425995585169  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 823777242200   | 309728275410  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 825506726631   | 311676057515  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 867410646205   | 747450555029  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 873886467241   | 872643235400  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 880154085591   | 867944330391  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 880426845529   | 400183786016  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 4       |
| 814280010135   | 425995585169  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 867410646365   | 747450555029  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 873886467281   | 872643235400  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 878806289708   | 865559821823  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 880154085751   | 867944330391  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 880426845569   | 400183786016  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 4       |
| 813414123963   | 425995585169  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 4       |
| 822867359910   | 297072975809  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 4       |
| 867410646165   | 747450555029  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 4       |
| 879468956876   | 865559822583  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 4       |
| 902488209354   | 314892675297  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 4       |
| 824716705410   | 310724742938  | 917     | BIDDING_CATEGORY_L1 | Nahrungsmittel, Getränke & Tabak  | 4       |
| 813414123803   | 425995585169  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 822749168809   | 314892675297  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 822867359710   | 297072975809  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 823933246468   | 309728275410  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 824176979200   | 824722569325  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 824716705330   | 310724742938  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 824927254920   | 400183786016  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 825506726551   | 311676057515  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 825902536461   | 780426909743  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 867410646125   | 747450555029  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 873886467081   | 872643235400  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 878806289668   | 865559821823  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 879468956836   | 865559822583  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 880154085551   | 867944330391  | 0       | BIDDING_CATEGORY_L1 | Other                             | 4       |
| 824716705570   | 310724742938  | 4100    | BIDDING_CATEGORY_L1 | Taschen & Gepäck                  | 4       |
| 866726937761   | 425995585169  | 2325    | BIDDING_CATEGORY_L1 | Tiere & Tierbedarf                | 4       |
| 823805461846   | 325507474244  | 1       | BRAND               | 13 1/2                            | 4       |
| 823984064561   | 781221701981  | 1       | BRAND               | 13 1/2                            | 4       |
| 867476270520   | 760091152538  | 71      | BRAND               | 360                               | 4       |
| 822752845889   | 309256998619  | 83      | BRAND               | abus                              | 4       |
| 824798995671   | 309256998619  | 186     | BRAND               | aeg                               | 4       |
| 822752846049   | 309256998619  | 6       | BRAND               | ahd amazing home design           | 4       |
| 825219320785   | 309728275410  | 12      | BRAND               | aleppo                            | 4       |
| 823984064601   | 781221701981  | 478     | BRAND               | apple                             | 4       |
| 824798995831   | 309256998619  | 22      | BRAND               | arcadia                           | 4       |
| 822752846089   | 309256998619  | 30      | BRAND               | attack                            | 4       |
| 823805461886   | 325507474244  | 5       | BRAND               | beachers                          | 4       |
| 867476270560   | 760091152538  | 128     | BRAND               | bega-gruppe                       | 4       |
| 823805462046   | 325507474244  | 5       | BRAND               | bfs europe                        | 4       |
| 823805462086   | 325507474244  | 53      | BRAND               | bijou karat                       | 4       |
| 824273422531   | 823971471241  | 71      | BRAND               | bodenmeister                      | 4       |
| 823805462126   | 325507474244  | 14      | BRAND               | cool4                             | 4       |
| 823948470190   | 824147670393  | 6       | BRAND               | coomax                            | 4       |
| 822752846129   | 309256998619  | 12      | BRAND               | dapra                             | 4       |
| 847246113754   | 325507474244  | 89      | BRAND               | depesche                          | 4       |
| 867476270600   | 760091152538  | 11      | BRAND               | dl                                | 4       |
| 867476270760   | 760091152538  | 48      | BRAND               | dobroplast                        | 4       |
| 823984064761   | 781221701981  | 256     | BRAND               | dq-pp                             | 4       |
| 824798995871   | 309256998619  | 256     | BRAND               | dq-pp                             | 4       |
| 824798995911   | 309256998619  | 354     | BRAND               | drutex                            | 4       |
| 823805462286   | 325507474244  | 101     | BRAND               | eaks®                             | 4       |
| 867476270800   | 760091152538  | 72      | BRAND               | eco-worthy                        | 4       |
| 867476270840   | 760091152538  | 4       | BRAND               | empula                            | 4       |
| 888820372717   | 342972403147  | 665     | BRAND               | [en.casa]®                        | 4       |
| 822752846289   | 309256998619  | 94      | BRAND               | exo terra                         | 4       |
| 823805462326   | 325507474244  | 10      | BRAND               | foliatec                          | 4       |
| 867476271000   | 760091152538  | 26      | BRAND               | fuchs design                      | 4       |
| 867476271040   | 760091152538  | 121     | BRAND               | ghs                               | 4       |
| 867476271080   | 760091152538  | 1       | BRAND               | goodenergytools                   | 4       |
| 824798996071   | 309256998619  | 24      | BRAND               | grafelstein                       | 4       |
| 867476271240   | 760091152538  | 55      | BRAND               | hager                             | 4       |
| 867476271280   | 760091152538  | 168     | BRAND               | handarbeit                        | 4       |
| 823948470230   | 824147670393  | 44      | BRAND               | hq                                | 4       |
| 822459167270   | 311676057515  | 3487    | BRAND               | ikea                              | 4       |
| 822752846329   | 309256998619  | 3487    | BRAND               | ikea                              | 4       |
| 823805462366   | 325507474244  | 3487    | BRAND               | ikea                              | 4       |
| 823984064801   | 781221701981  | 6974    | BRAND               | ikea                              | 4       |
| 824273422691   | 823971471241  | 3487    | BRAND               | ikea                              | 4       |
| 825219320825   | 309728275410  | 3487    | BRAND               | ikea                              | 4       |
| 824798996111   | 309256998619  | 348     | BRAND               | intex                             | 4       |
| 824054759659   | 427666518663  | 7       | BRAND               | junghans                          | 4       |
| 867476271320   | 760091152538  | 80      | BRAND               | jvmoebel                          | 4       |
| 823984064841   | 781221701981  | 4168    | BRAND               | keine angabe                      | 4       |
| 824709886215   | 822421702762  | 4168    | BRAND               | keine angabe                      | 4       |
| 824148939104   | 342972403147  | 153     | BRAND               | lignau                            | 4       |
| 823805462526   | 325507474244  | 15      | BRAND               | lillabelle                        | 4       |
| 823805462566   | 325507474244  | 3       | BRAND               | lip smacker                       | 4       |
| 823984065001   | 781221701981  | 3       | BRAND               | lip smacker                       | 4       |
| 822752846369   | 309256998619  | 68      | BRAND               | livolo                            | 4       |
| 822752846529   | 309256998619  | 6       | BRAND               | marchioro                         | 4       |
| 823805462606   | 325507474244  | 187     | BRAND               | markenware                        | 4       |
| 822752846569   | 309256998619  | 6       | BRAND               | master                            | 4       |
| 823805462766   | 325507474244  | 3       | BRAND               | mmmedia                           | 4       |
| 822752846609   | 309256998619  | 33      | BRAND               | monster                           | 4       |
| 823805462806   | 325507474244  | 702     | BRAND               | mosaik-netzwerk                   | 4       |
| 823984065041   | 781221701981  | 5       | BRAND               | murray & lanman                   | 4       |
| 824704474535   | 325507474244  | 5       | BRAND               | murray & lanman                   | 4       |
| 824054759699   | 427666518663  | 8       | BRAND               | navahoo premium                   | 4       |
| 822752846769   | 309256998619  | 26      | BRAND               | nes                               | 4       |
| 824273422731   | 823971471241  | 22      | BRAND               | nobilia                           | 4       |
| 867476271480   | 760091152538  | 15      | BRAND               | non                               | 4       |
| 823805462846   | 325507474244  | 23      | BRAND               | nostalgie                         | 4       |
| 825898300998   | 310724742938  | 20      | BRAND               | only                              | 4       |
| 298446599881   | 325507474244  | 0       | BRAND               | Other                             | 4       |
| 812093932502   | 760091152538  | 0       | BRAND               | Other                             | 4       |
| 812114915020   | 342972403147  | 0       | BRAND               | Other                             | 4       |
| 813520120211   | 427666518663  | 0       | BRAND               | Other                             | 4       |
| 821304297802   | 309728275410  | 0       | BRAND               | Other                             | 4       |
| 823948470150   | 824147670393  | 0       | BRAND               | Other                             | 4       |
| 823984064521   | 781221701981  | 0       | BRAND               | Other                             | 4       |
| 824273422491   | 823971471241  | 0       | BRAND               | Other                             | 4       |
| 824709886055   | 822421702762  | 0       | BRAND               | Other                             | 4       |
| 824791853191   | 311676057515  | 0       | BRAND               | Other                             | 4       |
| 824798995591   | 309256998619  | 0       | BRAND               | Other                             | 4       |
| 825898300958   | 310724742938  | 0       | BRAND               | Other                             | 4       |
| 842572684244   | 780426909743  | 0       | BRAND               | Other                             | 4       |
| 824798996151   | 309256998619  | 3       | BRAND               | ozeanos                           | 4       |
| 822752846809   | 309256998619  | 16      | BRAND               | piramida                          | 4       |
| 867476271520   | 760091152538  | 76      | BRAND               | pol-power                         | 4       |
| 824798995631   | 309256998619  | 24      | BRAND               | [pro.tec]®                        | 4       |
| 823984065081   | 781221701981  | 9       | BRAND               | renoplast                         | 4       |
| 824798996311   | 309256998619  | 69      | BRAND               | reolink                           | 4       |
| 824148939264   | 342972403147  | 65      | BRAND               | rider                             | 4       |
| 867476271560   | 760091152538  | 136     | BRAND               | roborock                          | 4       |
| 823948470390   | 824147670393  | 891     | BRAND               | samsung                           | 4       |
| 824709886255   | 822421702762  | 891     | BRAND               | samsung                           | 4       |
| 821333809629   | 760091152538  | 195     | BRAND               | scheppach                         | 4       |
| 822752846849   | 309256998619  | 1       | BRAND               | smartthings                       | 4       |
| 825219320865   | 309728275410  | 60      | BRAND               | sols teamsport                    | 4       |
| 824709886295   | 822421702762  | 178     | BRAND               | subtel                            | 4       |
| 847246113794   | 325507474244  | 178     | BRAND               | subtel                            | 4       |
| 824148939304   | 342972403147  | 40      | BRAND               | support 81                        | 4       |
| 822752847009   | 309256998619  | 8       | BRAND               | sureflap                          | 4       |
| 822752847049   | 309256998619  | 27      | BRAND               | tgn                               | 4       |
| 847246113834   | 325507474244  | 106     | BRAND               | tierhausschuhe                    | 4       |
| 823984065241   | 781221701981  | 9       | BRAND               | tobar                             | 4       |
| 824929351160   | 780426909743  | 45      | BRAND               | trachtenkini                      | 4       |
| 867476271720   | 760091152538  | 11      | BRAND               | trend micro                       | 4       |
| 822752847089   | 309256998619  | 45      | BRAND               | us-aquaristik                     | 4       |
| 823805463006   | 325507474244  | 49      | BRAND               | value line                        | 4       |
| 825219321025   | 309728275410  | 2335    | BRAND               | vhbw                              | 4       |
| 867476271760   | 760091152538  | 73      | BRAND               | vigour                            | 4       |
| 824798996351   | 309256998619  | 151     | BRAND               | vintage line                      | 4       |
| 825898301038   | 310724742938  | 1       | BRAND               | viola farma                       | 4       |
| 822459167430   | 311676057515  | 25      | BRAND               | vodafone                          | 4       |
| 825898301198   | 310724742938  | 25      | BRAND               | vodafone                          | 4       |
| 824798996391   | 309256998619  | 108     | BRAND               | wiltec                            | 4       |
| 824054759739   | 427666518663  | 141     | BRAND               | winkhaus                          | 4       |
| 824148939344   | 342972403147  | 141     | BRAND               | winkhaus                          | 4       |
| 824798996551   | 309256998619  | 141     | BRAND               | winkhaus                          | 4       |
| 823805463046   | 325507474244  | 17      | BRAND               | woz                               | 4       |
| 824929351320   | 780426909743  | 55      | BRAND               | xx                                | 4       |
| 825564252433   | 822421702762  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 4       |
| 825564252473   | 822421702762  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 4       |
| 825564252633   | 822421702762  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 4       |
| 825564252393   | 822421702762  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 4       |
| 870984416598   | 655831603150  | 8069    | PRODUCT_TYPE_L1     | computer & software               | 4       |
| 880202796429   | 655831602950  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 4       |
| 836180939714   | 427957489861  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 4       |
| 867121895513   | 761766328231  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 4       |
| 870984416638   | 655831603150  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 4       |
| 881725909351   | 425995585169  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 4       |
| 816558932155   | 314892675297  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 823465885199   | 657678040965  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 826788928937   | 425995585169  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 836180939674   | 427957489861  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 866726416208   | 865559822383  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 867121895473   | 761766328231  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 870984416558   | 655831603150  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 880202796389   | 655831602950  | 0       | PRODUCT_TYPE_L1     | Other                             | 4       |
| 821440288459   | 314892675297  | 23778   | PRODUCT_TYPE_L1     | sonstige angebote unserer händler | 4       |
| 823465885359   | 657678040965  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 826441305548   | 314892675297  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 836180939914   | 427957489861  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 866726416248   | 865559822383  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 867121895673   | 761766328231  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 880202796469   | 655831602950  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 881725909391   | 425995585169  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 4       |
| 1015907348792  | 821986979926  | 2403    | PRODUCT_TYPE_L2     | diät & ernährung                  | 4       |
| 1015907348832  | 821986979926  | 6302    | PRODUCT_TYPE_L2     | gesundheit & pflege               | 4       |
| 1015907348872  | 821986979926  | 31163   | PRODUCT_TYPE_L2     | kosmetik                          | 4       |
| 1015907348632  | 821986979926  | 0       | PRODUCT_TYPE_L2     | Other                             | 4       |
| 431863668620   | 385316813690  | 2753    | PRODUCT_TYPE_L3     | badaccessoires                    | 4       |
| 415045214551   | 413351137649  | 2521    | PRODUCT_TYPE_L3     | elektrowerkzeug                   | 4       |
| 497404331957   | 606408691432  | 129     | PRODUCT_TYPE_L3     | essgruppen                        | 4       |
| 483430660007   | 325117783334  | 7606    | PRODUCT_TYPE_L3     | farben & lacke                    | 4       |
| 394644788854   | 413232284093  | 53      | PRODUCT_TYPE_L3     | fernseher                         | 4       |
| 625000038632   | 606408691632  | 1955    | PRODUCT_TYPE_L3     | kinderzimmermöbel                 | 4       |
| 624989146272   | 606408691632  | 351     | PRODUCT_TYPE_L3     | kinderzimmer-sets                 | 4       |
| 432976343491   | 606408691872  | 293     | PRODUCT_TYPE_L3     | lattenroste                       | 4       |
| 433573944231   | 606408691872  | 38      | PRODUCT_TYPE_L3     | matratzen                         | 4       |
| 324639004510   | 325117783334  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 385316813730   | 385316813690  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 415045214311   | 413351137649  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 415532400623   | 413232284093  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 432482872014   | 606408692152  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 432837302320   | 606408691432  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 433284131944   | 606408691592  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 433455649585   | 394329862059  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 433573944191   | 606408691872  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 624989146232   | 606408691632  | 0       | PRODUCT_TYPE_L3     | Other                             | 4       |
| 432482872054   | 606408692152  | 3723    | PRODUCT_TYPE_L3     | polstermöbel                      | 4       |
| 456654194354   | 606408692152  | 2990    | PRODUCT_TYPE_L3     | regale                            | 4       |
| 433284131984   | 606408691592  | 626     | PRODUCT_TYPE_L3     | schuhschränke                     | 4       |
| 432837302360   | 606408691432  | 2486    | PRODUCT_TYPE_L3     | stühle                            | 4       |
| 488117666807   | 325117783334  | 2428    | PRODUCT_TYPE_L3     | tapeten & tapezierzubehör         | 4       |
| 433455649625   | 394329862059  | 6697    | PRODUCT_TYPE_L3     | uhren                             | 4       |
| 823835554079   | 812114915020  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 5       |
| 865194101940   | 813520120211  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 5       |
| 868679597685   | 825898300958  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 5       |
| 825098381245   | 823984064521  | 25596   | BIDDING_CATEGORY_L1 | Elektronik                        | 5       |
| 825098381445   | 823984064521  | 978     | BIDDING_CATEGORY_L1 | Fahrzeuge & Teile                 | 5       |
| 868679598125   | 825898300958  | 978     | BIDDING_CATEGORY_L1 | Fahrzeuge & Teile                 | 5       |
| 868679597725   | 825898300958  | 44185   | BIDDING_CATEGORY_L1 | Gesundheit & Schönheit            | 5       |
| 924401654127   | 826441305548  | 44185   | BIDDING_CATEGORY_L1 | Gesundheit & Schönheit            | 5       |
| 812501075419   | 836180939714  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 814169578690   | 812093932502  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 819943720286   | 836180939914  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 823540981402   | 824791853191  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 823877124169   | 824798995591  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 825098381405   | 823984064521  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 868679597885   | 825898300958  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 871823273943   | 842572684244  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 924401654287   | 826441305548  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 5       |
| 812160820973   | 812114915020  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 822856073990   | 813520120211  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 823540981562   | 824791853191  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 823637271062   | 836180939674  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 823877124209   | 824798995591  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 868679597925   | 825898300958  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 886736203363   | 812093932502  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 5       |
| 865194101980   | 813520120211  | 3308    | BIDDING_CATEGORY_L1 | Kunst & Unterhaltung              | 5       |
| 868679597965   | 825898300958  | 3308    | BIDDING_CATEGORY_L1 | Kunst & Unterhaltung              | 5       |
| 819943720246   | 836180939914  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 822856073910   | 813520120211  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 823637271022   | 836180939674  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 823835554239   | 812114915020  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 871823273903   | 842572684244  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 886736203323   | 812093932502  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 924401654087   | 826441305548  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 5       |
| 812160820773   | 812114915020  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 812501075259   | 836180939714  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 814169578530   | 812093932502  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 819943720206   | 836180939914  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 822856073750   | 813520120211  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 823540981362   | 824791853191  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 823637270862   | 836180939674  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 823877124129   | 824798995591  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 825098381205   | 823984064521  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 868679597645   | 825898300958  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 871823273743   | 842572684244  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 924401654047   | 826441305548  | 0       | BIDDING_CATEGORY_L1 | Other                             | 5       |
| 825098381485   | 823984064521  | 1433    | BIDDING_CATEGORY_L1 | Sportartikel                      | 5       |
| 868679598165   | 825898300958  | 1433    | BIDDING_CATEGORY_L1 | Sportartikel                      | 5       |
| 877584069316   | 873886467281  | 10776   | BIDDING_CATEGORY_L2 | Baumaterialien                    | 5       |
| 868496178448   | 823850958919  | 36533   | BIDDING_CATEGORY_L2 | Bekleidung                        | 5       |
| 868496178608   | 823850958919  | 9491    | BIDDING_CATEGORY_L2 | Bekleidungsaccessoires            | 5       |
| 875292478282   | 873886467241  | 3497    | BIDDING_CATEGORY_L2 | Haushaltsgeräte                   | 5       |
| 868496178648   | 823850958919  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires             | 5       |
| 877584069516   | 873886467281  | 4630    | BIDDING_CATEGORY_L2 | Küchen- und Sanitärinstallationen | 5       |
| 868496178408   | 823850958919  | 0       | BIDDING_CATEGORY_L2 | Other                             | 5       |
| 875292478242   | 873886467241  | 0       | BIDDING_CATEGORY_L2 | Other                             | 5       |
| 877584069276   | 873886467281  | 0       | BIDDING_CATEGORY_L2 | Other                             | 5       |
| 875292478442   | 873886467241  | 854     | BIDDING_CATEGORY_L2 | Pool & Spa                        | 5       |
| 877584069356   | 873886467281  | 5145    | BIDDING_CATEGORY_L2 | Werkzeuge                         | 5       |
| 824375369441   | 877086325567  | 5       | BRAND               | 2store                            | 5       |
| 867588761024   | 823777242160  | 37      | BRAND               | 2store24                          | 5       |
| 867119020504   | 816558932155  | 2       | BRAND               | amasava                           | 5       |
| 867588761184   | 823777242160  | 284     | BRAND               | anladia                           | 5       |
| 867588761224   | 823777242160  | 3       | BRAND               | bhs binkert                       | 5       |
| 867119020544   | 816558932155  | 3       | BRAND               | bibi und tina                     | 5       |
| 852970137004   | 433455649585  | 53      | BRAND               | bijou karat                       | 5       |
| 825085897855   | 877086325567  | 14      | BRAND               | cool4                             | 5       |
| 867119020704   | 816558932155  | 14      | BRAND               | cottelli collection lingerie      | 5       |
| 867119020744   | 816558932155  | 63      | BRAND               | dayplus®                          | 5       |
| 824375369481   | 877086325567  | 61      | BRAND               | demar                             | 5       |
| 852970137044   | 433455649585  | 89      | BRAND               | depesche                          | 5       |
| 824375369641   | 877086325567  | 2       | BRAND               | freiwild                          | 5       |
| 867119020784   | 816558932155  | 2       | BRAND               | fruit of the loom kult-klamotten  | 5       |
| 867588761264   | 823777242160  | 2       | BRAND               | fruit of the loom kult-klamotten  | 5       |
| 847623442474   | 1015907348832 | 2       | BRAND               | f.s.d. sicherheitsdienste         | 5       |
| 824375369681   | 877086325567  | 722     | BRAND               | gildan                            | 5       |
| 867119020944   | 816558932155  | 722     | BRAND               | gildan                            | 5       |
| 867588761424   | 823777242160  | 4       | BRAND               | ips international parcel service  | 5       |
| 824375369721   | 877086325567  | 242     | BRAND               | karnevalsteufel                   | 5       |
| 867119020984   | 816558932155  | 242     | BRAND               | karnevalsteufel                   | 5       |
| 824375369881   | 877086325567  | 4168    | BRAND               | keine angabe                      | 5       |
| 847623442514   | 1015907348832 | 3       | BRAND               | koromonto                         | 5       |
| 867119021024   | 816558932155  | 23      | BRAND               | kostümpalast                      | 5       |
| 867119021184   | 816558932155  | 308     | BRAND               | kultfaktor gmbh                   | 5       |
| 867588761464   | 823777242160  | 27      | BRAND               | kult-klamotten                    | 5       |
| 867588761504   | 823777242160  | 25      | BRAND               | lafairy                           | 5       |
| 824375369921   | 877086325567  | 1       | BRAND               | lining                            | 5       |
| 867119021224   | 816558932155  | 6       | BRAND               | lupilu                            | 5       |
| 867588761664   | 823777242160  | 16      | BRAND               | maboobie                          | 5       |
| 867119021264   | 816558932155  | 3       | BRAND               | micoland                          | 5       |
| 824375369961   | 877086325567  | 9       | BRAND               | nobel league                      | 5       |
| 867119021424   | 816558932155  | 55      | BRAND               | oblique unique                    | 5       |
| 867119021464   | 816558932155  | 54      | BRAND               | orion                             | 5       |
| 867119021504   | 816558932155  | 472     | BRAND               | orlob                             | 5       |
| 824375369401   | 877086325567  | 0       | BRAND               | Other                             | 5       |
| 847623442314   | 1015907348832 | 0       | BRAND               | Other                             | 5       |
| 852970136844   | 433455649585  | 0       | BRAND               | Other                             | 5       |
| 867119020464   | 816558932155  | 0       | BRAND               | Other                             | 5       |
| 867588760984   | 823777242160  | 0       | BRAND               | Other                             | 5       |
| 867588761704   | 823777242160  | 24      | BRAND               | pfüller                           | 5       |
| 867119021664   | 816558932155  | 35      | BRAND               | prinz                             | 5       |
| 867588761744   | 823777242160  | 37      | BRAND               | royal shirt                       | 5       |
| 867119021704   | 816558932155  | 14      | BRAND               | slimmaxx                          | 5       |
| 825085897895   | 877086325567  | 81      | BRAND               | squared & cubed                   | 5       |
| 867588761904   | 823777242160  | 14      | BRAND               | stone island                      | 5       |
| 867119021744   | 816558932155  | 4       | BRAND               | strotex                           | 5       |
| 824375370161   | 877086325567  | 40      | BRAND               | support 81                        | 5       |
| 852970137084   | 433455649585  | 12      | BRAND               | sylvie monthule                   | 5       |
| 867588761944   | 823777242160  | 106     | BRAND               | tierhausschuhe                    | 5       |
| 824375370201   | 877086325567  | 279     | BRAND               | tma                               | 5       |
| 867588761984   | 823777242160  | 22      | BRAND               | uomo                              | 5       |
| 867119021904   | 816558932155  | 49      | BRAND               | value line                        | 5       |
| 824375370361   | 877086325567  | 6       | BRAND               | w.k.tex                           | 5       |
| 825085898055   | 877086325567  | 17      | BRAND               | woz                               | 5       |
| 823912483171   | 624989146232  | 0       | CANONICAL_CONDITION | neu                               | 5       |
| 823912483131   | 624989146232  | 0       | CANONICAL_CONDITION | Other                             | 5       |
| 864681839416   | 821304297802  | 0       | CHANNEL             | online                            | 5       |
| 864681839256   | 821304297802  | 0       | CHANNEL             | Other                             | 5       |
| 823129108286   | 888036472997  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 5       |
| 824491997949   | 824273422491  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 5       |
| 824831943345   | 415045214311  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 5       |
| 823129108446   | 888036472997  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 5       |
| 823243997270   | 824273422691  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 5       |
| 825430868583   | 823984064801  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 5       |
| 824831943505   | 415045214311  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 5       |
| 825430868543   | 823984064801  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 5       |
| 822946854694   | 456654194354  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 5       |
| 823129108486   | 888036472997  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 5       |
| 824491997989   | 824273422491  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 5       |
| 824831943545   | 415045214311  | 25277   | CUSTOM_ATTRIBUTE_0  | 15 - 20                           | 5       |
| 823129108526   | 888036472997  | 743     | CUSTOM_ATTRIBUTE_0  | 2000 - 5000                       | 5       |
| 824831943585   | 415045214311  | 3169    | CUSTOM_ATTRIBUTE_0  | 200 - 250                         | 5       |
| 822946854854   | 456654194354  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 5       |
| 823129108686   | 888036472997  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 5       |
| 824831943745   | 415045214311  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 5       |
| 823243997470   | 824273422691  | 4039    | CUSTOM_ATTRIBUTE_0  | 500 - 1000                        | 5       |
| 824831943825   | 415045214311  | 18823   | CUSTOM_ATTRIBUTE_0  | 50 - 75                           | 5       |
| 823129108726   | 888036472997  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 5       |
| 823243997430   | 824273422691  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 5       |
| 824491998149   | 824273422491  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 5       |
| 824831943785   | 415045214311  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 5       |
| 822946854654   | 456654194354  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 823129108246   | 888036472997  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 823243997230   | 824273422691  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 824491997909   | 824273422491  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 824831943305   | 415045214311  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 825430868383   | 823984064801  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 5       |
| 869486108540   | 825902536461  | 3684    | PRODUCT_TYPE_L1     | baby & spielwaren                 | 5       |
| 870798582382   | 823777242200  | 3684    | PRODUCT_TYPE_L1     | baby & spielwaren                 | 5       |
| 871710464299   | 867410646365  | 3684    | PRODUCT_TYPE_L1     | baby & spielwaren                 | 5       |
| 872378374208   | 813414123803  | 8069    | PRODUCT_TYPE_L1     | computer & software               | 5       |
| 823908383938   | 812114915020  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 5       |
| 869486108580   | 825902536461  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 5       |
| 870798582422   | 823777242200  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 5       |
| 883473169103   | 880426845529  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 5       |
| 865558768242   | 812114915020  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 867653899655   | 814280010135  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 869486108620   | 825902536461  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 870319352129   | 867410646205  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 871074080608   | 867410646165  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 871710464339   | 867410646365  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 872378374248   | 813414123803  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 883473169143   | 880426845529  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 949205969757   | 880426845569  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 5       |
| 825245657196   | 823933246468  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 5       |
| 865558768282   | 812114915020  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 5       |
| 869486108780   | 825902536461  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 5       |
| 871074080768   | 867410646165  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 5       |
| 816320045705   | 813414123963  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 823908383898   | 812114915020  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 825245657036   | 823933246468  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 867653899615   | 814280010135  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 869486108380   | 825902536461  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 870319352089   | 867410646205  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 870798582222   | 823777242200  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 871074080568   | 867410646165  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 871710464139   | 867410646365  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 872378374048   | 813414123803  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 883473168943   | 880426845529  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 949205969717   | 880426845569  | 0       | PRODUCT_TYPE_L1     | Other                             | 5       |
| 871074080808   | 867410646165  | 23778   | PRODUCT_TYPE_L1     | sonstige angebote unserer händler | 5       |
| 911998964844   | 813414123963  | 23778   | PRODUCT_TYPE_L1     | sonstige angebote unserer händler | 5       |
| 949205969917   | 880426845569  | 5080    | PRODUCT_TYPE_L1     | sport & outdoor                   | 5       |
| 825245657236   | 823933246468  | 4673    | PRODUCT_TYPE_L1     | unterhaltungselektronik           | 5       |
| 816320045905   | 813414123963  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 865558768442   | 812114915020  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 867653899815   | 814280010135  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 869486108820   | 825902536461  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 870319352289   | 867410646205  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 870798582462   | 823777242200  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 871074080848   | 867410646165  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 872378374288   | 813414123803  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 5       |
| 935170918037   | 870984416638  | 3521    | PRODUCT_TYPE_L2     | garten & balkon                   | 5       |
| 935170918077   | 870984416638  | 4591    | PRODUCT_TYPE_L2     | haustechnik & -elektronik         | 5       |
| 1059406845592  | 866726416248  | 2510    | PRODUCT_TYPE_L2     | kinder- & jugendzimmer            | 5       |
| 935170917997   | 870984416638  | 0       | PRODUCT_TYPE_L2     | Other                             | 5       |
| 1059406845552  | 866726416248  | 0       | PRODUCT_TYPE_L2     | Other                             | 5       |
| 1059406845752  | 866726416248  | 10547   | PRODUCT_TYPE_L2     | wohnzimmer                        | 5       |
| 461920052124   | 415045214551  | 1065    | PRODUCT_TYPE_L4     | bohrmaschinen & schrauber         | 5       |
| 427933750976   | 433455649625  | 1762    | PRODUCT_TYPE_L4     | chronographen                     | 5       |
| 434110132956   | 433573944231  | 20      | PRODUCT_TYPE_L4     | kaltschaummatratzen               | 5       |
| 431614582216   | 432482872054  | 0       | PRODUCT_TYPE_L4     | Other                             | 5       |
| 433202656450   | 431863668620  | 0       | PRODUCT_TYPE_L4     | Other                             | 5       |
| 434110132796   | 433573944231  | 0       | PRODUCT_TYPE_L4     | Other                             | 5       |
| 461919849724   | 433455649625  | 0       | PRODUCT_TYPE_L4     | Other                             | 5       |
| 461920052084   | 415045214551  | 0       | PRODUCT_TYPE_L4     | Other                             | 5       |
| 431614582256   | 432482872054  | 1632    | PRODUCT_TYPE_L4     | sessel                            | 5       |
| 435744024958   | 433455649625  | 2346    | PRODUCT_TYPE_L4     | uhrenarmbänder                    | 5       |
| 433202656490   | 431863668620  | 1006    | PRODUCT_TYPE_L4     | wc-sitze                          | 5       |
| 871764473445   | 867119020464  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 6       |
| 871764473485   | 867119020464  | 25596   | BIDDING_CATEGORY_L1 | Elektronik                        | 6       |
| 865221479907   | 823908383938  | 44185   | BIDDING_CATEGORY_L1 | Gesundheit & Schönheit            | 6       |
| 871764473685   | 867119020464  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 6       |
| 824224770403   | 824831943505  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 6       |
| 824545749520   | 823964913561  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 6       |
| 871764473645   | 867119020464  | 24567   | BIDDING_CATEGORY_L1 | Möbel                             | 6       |
| 824224770363   | 824831943505  | 0       | BIDDING_CATEGORY_L1 | Other                             | 6       |
| 824545749480   | 823964913561  | 0       | BIDDING_CATEGORY_L1 | Other                             | 6       |
| 865221479747   | 823908383938  | 0       | BIDDING_CATEGORY_L1 | Other                             | 6       |
| 871764473405   | 867119020464  | 0       | BIDDING_CATEGORY_L1 | Other                             | 6       |
| 871182597082   | 871074080848  | 743     | BIDDING_CATEGORY_L2 | Baby- & Kleinkindmöbel            | 6       |
| 869910327802   | 868679597925  | 10776   | BIDDING_CATEGORY_L2 | Baumaterialien                    | 6       |
| 869910327962   | 868679597925  | 3567    | BIDDING_CATEGORY_L2 | Bauzubehör                        | 6       |
| 872146295196   | 867588761184  | 36533   | BIDDING_CATEGORY_L2 | Bekleidung                        | 6       |
| 935186813757   | 867588760984  | 36533   | BIDDING_CATEGORY_L2 | Bekleidung                        | 6       |
| 869848214289   | 868679597685  | 9491    | BIDDING_CATEGORY_L2 | Bekleidungsaccessoires            | 6       |
| 866045633986   | 822856073910  | 2256    | BIDDING_CATEGORY_L2 | Betten & Zubehör                  | 6       |
| 869275043177   | 823835554239  | 407     | BIDDING_CATEGORY_L2 | Gartenmöbel                       | 6       |
| 874317417791   | 868679597725  | 7212    | BIDDING_CATEGORY_L2 | Gesundheitspflege                 | 6       |
| 871006433270   | 868679597885  | 4547    | BIDDING_CATEGORY_L2 | Haushaltsbedarf                   | 6       |
| 874317417751   | 868679597725  | 36299   | BIDDING_CATEGORY_L2 | Körperpflege                      | 6       |
| 865927607220   | 823835554079  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires             | 6       |
| 869848214329   | 868679597685  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires             | 6       |
| 872146295236   | 867588761184  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires             | 6       |
| 935186813917   | 867588760984  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires             | 6       |
| 869275043417   | 823835554239  | 899     | BIDDING_CATEGORY_L2 | Möbelgarnituren                   | 6       |
| 871182597242   | 871074080848  | 899     | BIDDING_CATEGORY_L2 | Möbelgarnituren                   | 6       |
| 865927607020   | 823835554079  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 866045633706   | 822856073910  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 869275043017   | 823835554239  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 869848214129   | 868679597685  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 869910327762   | 868679597925  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 871006433230   | 868679597885  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 871182597002   | 871074080848  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 872146295036   | 867588761184  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 874317417591   | 868679597725  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 880929010508   | 819943720246  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 887813631304   | 886736203323  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 935186813717   | 867588760984  | 0       | BIDDING_CATEGORY_L2 | Other                             | 6       |
| 866045633746   | 822856073910  | 516     | BIDDING_CATEGORY_L2 | Polsterhocker                     | 6       |
| 866045633946   | 822856073910  | 2670    | BIDDING_CATEGORY_L2 | Regalsysteme                      | 6       |
| 869275043257   | 823835554239  | 2670    | BIDDING_CATEGORY_L2 | Regalsysteme                      | 6       |
| 865927607260   | 823835554079  | 16144   | BIDDING_CATEGORY_L2 | Schmuck                           | 6       |
| 871182597282   | 871074080848  | 2577    | BIDDING_CATEGORY_L2 | Schränke                          | 6       |
| 887813631504   | 886736203323  | 2577    | BIDDING_CATEGORY_L2 | Schränke                          | 6       |
| 866045633906   | 822856073910  | 2134    | BIDDING_CATEGORY_L2 | Sofas                             | 6       |
| 869275043217   | 823835554239  | 5839    | BIDDING_CATEGORY_L2 | Stühle                            | 6       |
| 871182597042   | 871074080848  | 5839    | BIDDING_CATEGORY_L2 | Stühle                            | 6       |
| 880929010548   | 819943720246  | 5839    | BIDDING_CATEGORY_L2 | Stühle                            | 6       |
| 887813631344   | 886736203323  | 5839    | BIDDING_CATEGORY_L2 | Stühle                            | 6       |
| 880929010708   | 819943720246  | 5552    | BIDDING_CATEGORY_L2 | Tische                            | 6       |
| 875556592480   | 868496178448  | 0       | BIDDING_CATEGORY_L3 | Other                             | 6       |
| 933562026127   | 877584069356  | 0       | BIDDING_CATEGORY_L3 | Other                             | 6       |
| 933562026167   | 877584069356  | 755     | BIDDING_CATEGORY_L3 | Sägen                             | 6       |
| 875556592520   | 868496178448  | 16384   | BIDDING_CATEGORY_L3 | Shirts & Tops                     | 6       |
| 933562026207   | 877584069356  | 480     | BIDDING_CATEGORY_L3 | Werkzeugsets                      | 6       |
| 873164777195   | 868496178648  | 345     | BRAND               | b & c                             | 6       |
| 824164946320   | 825308977595  | 25      | BRAND               | casa pura                         | 6       |
| 884497710890   | 883473169103  | 246     | BRAND               | de' longhi                        | 6       |
| 870114727826   | 825245657036  | 349     | BRAND               | disney                            | 6       |
| 884497710930   | 883473169103  | 66      | BRAND               | dyson                             | 6       |
| 877366069657   | 867653899655  | 72      | BRAND               | eco-worthy                        | 6       |
| 873164777235   | 868496178648  | 41      | BRAND               | eurocarnavales                    | 6       |
| 870114727866   | 825245657036  | 8       | BRAND               | fabu-print                        | 6       |
| 824554379920   | 824831943785  | 2       | BRAND               | gravola                           | 6       |
| 824164946360   | 825308977595  | 98      | BRAND               | hori                              | 6       |
| 870114727906   | 825245657036  | 3487    | BRAND               | ikea                              | 6       |
| 824164946520   | 825308977595  | 84      | BRAND               | internethandel pfordt             | 6       |
| 873164777275   | 868496178648  | 2       | BRAND               | magic by freddys/wilbers          | 6       |
| 824554379960   | 824831943785  | 1781    | BRAND               | makita                            | 6       |
| 870114728066   | 825245657036  | 150     | BRAND               | nanyuk                            | 6       |
| 824164946280   | 825308977595  | 0       | BRAND               | Other                             | 6       |
| 824554379880   | 824831943785  | 0       | BRAND               | Other                             | 6       |
| 870114727666   | 825245657036  | 0       | BRAND               | Other                             | 6       |
| 873164777035   | 868496178648  | 0       | BRAND               | Other                             | 6       |
| 877366069617   | 867653899655  | 0       | BRAND               | Other                             | 6       |
| 884497710850   | 883473169103  | 0       | BRAND               | Other                             | 6       |
| 877366069817   | 867653899655  | 7       | BRAND               | plateart                          | 6       |
| 870114728106   | 825245657036  | 42      | BRAND               | primoliving                       | 6       |
| 824164946560   | 825308977595  | 9       | BRAND               | renoplast                         | 6       |
| 824375370121   | 877086325567  | 2       | BRAND               | rock spring / scandi              | 6       |
| 873164777435   | 868496178648  | 9       | BRAND               | roughtex                          | 6       |
| 884497711090   | 883473169103  | 189     | BRAND               | siemens                           | 6       |
| 870114728146   | 825245657036  | 19      | BRAND               | topmulti                          | 6       |
| 824164946600   | 825308977595  | 67      | BRAND               | venodo                            | 6       |
| 824164946760   | 825308977595  | 141     | BRAND               | winkhaus                          | 6       |
| 867476224440   | 822856073990  | 0       | CANONICAL_CONDITION | neu                               | 6       |
| 870455729500   | 823540981562  | 0       | CANONICAL_CONDITION | neu                               | 6       |
| 871076295972   | 864681839256  | 0       | CANONICAL_CONDITION | neu                               | 6       |
| 874615670878   | 823540981402  | 0       | CANONICAL_CONDITION | neu                               | 6       |
| 867476224280   | 822856073990  | 0       | CANONICAL_CONDITION | Other                             | 6       |
| 870455729460   | 823540981562  | 0       | CANONICAL_CONDITION | Other                             | 6       |
| 871076295812   | 864681839256  | 0       | CANONICAL_CONDITION | Other                             | 6       |
| 874615670838   | 823540981402  | 0       | CANONICAL_CONDITION | Other                             | 6       |
| 824174571249   | 824375369401  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 6       |
| 824829823905   | 461919849724  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 6       |
| 877419543727   | 821983809766  | 17042   | CUSTOM_ATTRIBUTE_0  | 0 - 5                             | 6       |
| 824829824105   | 461919849724  | 11872   | CUSTOM_ATTRIBUTE_0  | 100 - 150                         | 6       |
| 824829824065   | 461919849724  | 25060   | CUSTOM_ATTRIBUTE_0  | 10 - 15                           | 6       |
| 824293936211   | 852970137004  | 7362    | CUSTOM_ATTRIBUTE_0  | 250 - 500                         | 6       |
| 877419543767   | 821983809766  | 7362    | CUSTOM_ATTRIBUTE_0  | 250 - 500                         | 6       |
| 824174571289   | 824375369401  | 53184   | CUSTOM_ATTRIBUTE_0  | 25 - 50                           | 6       |
| 824293936251   | 852970137004  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10                            | 6       |
| 824829824145   | 461919849724  | 11828   | CUSTOM_ATTRIBUTE_0  | 75 - 100                          | 6       |
| 824174571089   | 824375369401  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 6       |
| 824293936051   | 852970137004  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 6       |
| 824829823865   | 461919849724  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 6       |
| 877419543567   | 821983809766  | 0       | CUSTOM_ATTRIBUTE_0  | Other                             | 6       |
| 874017989224   | 868679597645  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 6       |
| 920820795767   | 823540981362  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 6       |
| 1058857728192  | 812160820773  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik             | 6       |
| 867995025285   | 822856073750  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 6       |
| 874017989264   | 868679597645  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 6       |
| 920820795807   | 823540981362  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 6       |
| 1017061933872  | 812160820973  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 6       |
| 1058857728232  | 812160820773  | 18161   | PRODUCT_TYPE_L1     | haushalt                          | 6       |
| 867995025325   | 822856073750  | 45618   | PRODUCT_TYPE_L1     | heimwerken & garten               | 6       |
| 874017989424   | 868679597645  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 6       |
| 874136685777   | 868679597965  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 6       |
| 920820795967   | 823540981362  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 6       |
| 1058857728392  | 812160820773  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe                     | 6       |
| 820123612070   | 812160820773  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 867995025245   | 822856073750  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 874017989184   | 868679597645  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 874136685737   | 868679597965  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 920820795727   | 823540981362  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 1017061933832  | 812160820973  | 0       | PRODUCT_TYPE_L1     | Other                             | 6       |
| 1058857728432  | 812160820773  | 23778   | PRODUCT_TYPE_L1     | sonstige angebote unserer händler | 6       |
| 874017989464   | 868679597645  | 5080    | PRODUCT_TYPE_L1     | sport & outdoor                   | 6       |
| 826765439005   | 812160820773  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 6       |
| 865703027013   | 865558768282  | 14988   | PRODUCT_TYPE_L2     | herrenmode                        | 6       |
| 865703026973   | 865558768282  | 0       | PRODUCT_TYPE_L2     | Other| 6       |
| 872192873734   | 870798582462  | 0       | PRODUCT_TYPE_L2     | Other| 6       |
| 865703027173   | 865558768282  | 12782   | PRODUCT_TYPE_L2     | schuhe                            | 6       |
| 872192873894   | 870798582462  | 21171   | PRODUCT_TYPE_L2     | wohnaccessoires                   | 6       |
| 872192873934   | 870798582462  | 1022    | PRODUCT_TYPE_L2     | zubehör für möbel                 | 6       |
| 621311800752   | 461920052124  | 941     | PRODUCT_TYPE_L5     | akkuschrauber                     | 6       |
| 431369272026   | 461920052124  | 0       | PRODUCT_TYPE_L5     | Other                             | 6       |
| 867331144491   | 871076295812  | 93068   | BIDDING_CATEGORY_L1 | Bekleidung & Accessoires          | 7       |
| 867331144531   | 871076295812  | 57277   | BIDDING_CATEGORY_L1 | Heim & Garten                     | 7       |
| 826594800191   | 824164946560  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 7       |
| 867331144691   | 871076295812  | 35987   | BIDDING_CATEGORY_L1 | Heimwerkerbedarf                  | 7       |
| 826594800151   | 824164946560  | 0       | BIDDING_CATEGORY_L1 | Other                             | 7       |
| 867331144451   | 871076295812  | 0       | BIDDING_CATEGORY_L1 | Other                             | 7       |
| 879408456661   | 867476224280  | 7128    | BIDDING_CATEGORY_L2 | Baumaterial                       | 7       |
| 878450140774   | 870455729460  | 10776   | BIDDING_CATEGORY_L2 | Baumaterialien                    | 7       |
| 879408456461   | 867476224280  | 10776   | BIDDING_CATEGORY_L2 | Baumaterialien                    | 7       |
| 878450140934   | 870455729460  | 3567    | BIDDING_CATEGORY_L2 | Bauzubehör | 7       |
| 873245639905   | 871764473445  | 36533   | BIDDING_CATEGORY_L2 | Bekleidung | 7       |
| 879408456621   | 867476224280  | 2062    | BIDDING_CATEGORY_L2 | Elektrobedarf | 7       |
| 873245639945   | 871764473445  | 8490    | BIDDING_CATEGORY_L2 | Kostüme & Accessoires | 7       |
| 873245639745   | 871764473445  | 0       | BIDDING_CATEGORY_L2 | Other| 7 |
| 878450140734   | 870455729460  | 0       | BIDDING_CATEGORY_L2 | Other| 7 |
| 879408456421   | 867476224280  | 0       | BIDDING_CATEGORY_L2 | Other| 7 |
| 882009974628   | 935186813917  | 6428    | BIDDING_CATEGORY_L3 | Kostüme & Verkleidungen | 7       |
| 882009974788   | 935186813917  | 332     | BIDDING_CATEGORY_L3 | Masken | 7       |
| 882009974588   | 935186813917  | 0       | BIDDING_CATEGORY_L3 | Other| 7       |
| 825258498200   | 824164946280  | 743     | CUSTOM_ATTRIBUTE_0  | 2000 - 5000 | 7       |
| 825258498360   | 824164946280  | 26781   | CUSTOM_ATTRIBUTE_0  | 5 - 10 | 7       |
| 825258498160   | 824164946280  | 0       | CUSTOM_ATTRIBUTE_0  | Other | 7       |
| 879834637865   | 865927607020  | 3684    | PRODUCT_TYPE_L1     | baby & spielwaren | 7       |
| 906560653114   | 874317417751  | 42340   | PRODUCT_TYPE_L1     | gesundheit & kosmetik | 7       |
| 871734122575   | 865927607220  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe| 7       |
| 872457244535   | 869848214329  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe| 7       |
| 879834637905   | 865927607020  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe| 7       |
| 906560653154   | 874317417751  | 88021   | PRODUCT_TYPE_L1     | mode & schuhe | 7       |
| 871734122535   | 865927607220  | 0       | PRODUCT_TYPE_L1     | Other| 7       |
| 872425153905   | 869275043417  | 0       | PRODUCT_TYPE_L1     | Other| 7       |
| 872457244495   | 869848214329  | 0       | PRODUCT_TYPE_L1     | Other| 7       |
| 879834637825   | 865927607020  | 0       | PRODUCT_TYPE_L1     | Other | 7       |
| 906560652954   | 874317417751  | 0       | PRODUCT_TYPE_L1     | Other | 7       |
| 1063210964072  | 869275043257  | 0       | PRODUCT_TYPE_L1     | Other  | 7       |
| 872425154065   | 869275043417  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle                | 7       |
| 1063210964112  | 869275043257  | 57884   | PRODUCT_TYPE_L1     | wohnen & lifestyle | 7       |
| 881401802370   | 874017989224  | 6302    | PRODUCT_TYPE_L2     | gesundheit & pflege               | 7       |
| 872139811144   | 867995025325  | 4591    | PRODUCT_TYPE_L2     | haustechnik & -elektronik         | 7       |
| 870592584479   | 826765439005  | 2510    | PRODUCT_TYPE_L2     | kinder- & jugendzimmer            | 7       |
| 881401802530   | 874017989224  | 31163   | PRODUCT_TYPE_L2     | kosmetik  | 7       |
| 870592584439   | 826765439005  | 0       | PRODUCT_TYPE_L2     | Other  | 7       |
| 872139811104   | 867995025325  | 0       | PRODUCT_TYPE_L2     | Other  | 7       |
| 881401802330   | 874017989224  | 0       | PRODUCT_TYPE_L2     | Other | 7       |
| 870592584639   | 826765439005  | 2675    | PRODUCT_TYPE_L2     | schlafzimmer                      | 7       |
| 872139811184   | 867995025325  | 2464    | PRODUCT_TYPE_L2     | tierbedarf & tierfutter | 7 |
| 870592584679   | 826765439005  | 21171   | PRODUCT_TYPE_L2     | wohnaccessoires | 7  |

| 871838892413 | 871734122575 | 22360 | PRODUCT_TYPE_L2 | accessoires | 8|
| 871838892453   | 871734122575  | 12521   | PRODUCT_TYPE_L2  |damenmode| 8       |
| 871838892373   | 871734122575  | 0       | PRODUCT_TYPE_L2     | Other                             | 8       |
+----------------+---------------+---------+---------------------+---------------------------------+---------+

