# Adspert Google Ads CLI

## Commands

### Grouping

  - campaign
  - campaign --campaign-id set (--bidding-scheme='foo', --name='bar', etc...)

  - bidding strategy --bsid -> BiddingStrategy object details
  - bidding strategy campaigns -> List[Campaign] (show bidding strategy info by campaign)
    ... (other BS action commands)
  - bidding strategy get (--predicates) -> Map[bsid: BiddingStrategy]
  - bidding strategy list (--filters, *) ->
  - bidding strategy types -> List of bidding schemes(goals) ([index] for more info)

#### Command Group Taxonomy

state [meta] > acc. entity/level > (group | object) [attrs, filters, ...] <action>
....

example commands:
-----------------

`$ active [shopping] > campaign > bidding (<group>) > strategy --campaign-id=123412 ... list|get`

`$ all > ad_group > criteria (<object>) > types`

Fundamentally each command returns a mapping of valid sub-commands. This hierarchy tree leads to the final grouping where the function call of a resource (or sub-resource when a meta flag is passed) is invoked by an object from or by the group's module, with attributes and filters passed as args +  kwargs.

### Current Workspace / Context

**Status of workspaces:**

#### Message Handling

  - prototyping ideas to replace and improve upon the MessageType metaclass.
  - object classification (via metadata and/or hierarchy)
  - late initiation / lazy properties
  - accessors (data descriptors)

#### Metadata

  - script for grabbing an artifact + all direct children
  - parse, marry and map fields (to db models and API types) using results

### Bidding Strategy

> As this task won't include the generic message handling,
> it's tasks are not related to
> above road maps.

  - Campaign query (with bid-strat filter)
  - get_auto_optimized_campaigns -> List[Campaign]

### Defining a resource structure

  - An artifact at a given account level.
  - Optionally filtered - often by availability measures.
  - Some reduction and orgnaisation of the resulting data set.

Therefore, it is often as simple as, creator a query from:

(schema representing structure + filter against unwanted entities);
taking results of said query and simplifying to fit the table
structure of the entity; performing clean-up and maintenance of the resulting data.

An (over)simplified generic could be:

    resource_sync = ResourceStructureSync()

    ```python
    schema = resource_sync.get_schema('name')
    schema.filterby(Artifact.id, [...id_list...], True)

    query = resource_sync.generate_api_query()
    with api.error_handler(on_error=<fn>) as e:
        stream = (resource_sync.process(client.do_query(query)))

    # generate api query would be something like..
    # q = Query(resource=resource, fields=fields, filters=filters, order=resource.id)

    results = client.run(q)

    for r in r:
        task_q.add(r)
        # one_or_more_processers operate on data
        # dicts <<->> dicts
        # dict or dicts added to models - DB updated.

    e.g. adgroup_structure_spec: {
    >>      'resource': ad_group,
    >>      'model': AdGroup,   # adspert model
    >>      'schema' : [...],   # schema model
    >>      'fieldmap': {...},  # maybe? prefer aliasing though.
    ```

## Account monitor

Using the change status service a background process could run at a set interval and for any change found the
internal structure could be updated so to constantly have an accurate picture of the account.

```bash
$ adw monitor <account-no.> --all 5  # minutes i.e.
```

The entire changeset would be picked up in a single query - or divided by resource.

```python
status = ChangeStatusService(account)

def announce(event):
    print(f'{event.resource} has been {event.status} at {event.last_change_date_time}')
    def diff(node):
        print(f'{event.resource} updated. Diff report: {get_node(event.resource)}')
    return diff

fn = announce

status.subscribe(AdGroup, ADDED|CHANGED, presync=fn, postsync=fn)
status.subscribe(Campaign, ADDED|CHANGED, presync=fn, postsync=fn)
status.subscribe(AdGroupCriterion, ADDED|CHANGED, presync=fn, postsync=fn)
status.subscribe(CampaignCriterion, ADDED|CHANGED, presync=fn, postsync=fn)

# ... time elapses
status.poll()

# ... inside call
fields = [ChangeStatus.last_change_date_time,
          ChangeStatus.resource_type]

def elapsed_since(interval: int):
    now = time.time()
    nanos = int((now - int(interval * 60)) * 10**9)
    timestamp = Timestamp(now, nanos)
# OR
# log each change status request, use last logged time as predicate automatically.
response = ChangeStatus.stream(fields, predicate=(ChangeStatus.last_change_date_time, '>', elapsed_since(5))
```

campaigns

  -> campaign_type bucket
  -> parse each campaign type
  -> add artifacts to common pool
  -> re-distribute as per next relevant identity at next hierarchical level.

Task hierarchy:

Graph of tasks with dependencies
  nodes are:
    - procedures
    - view
    - report