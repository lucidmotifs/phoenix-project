// main db snippets

SELECT
  account_id,
  adspert_id,
  db_host,
  account_name
FROM account
WHERE optimize
  AND platform = 'adwords'
ORDER BY db_host;

// account snippets

### Shopping Stats

```sql
-- count days product ads impressions without interactions
-- (included: num related adgroups)
SELECT
    count(date) seen_days, item_id
    array_length(array_agg(DISTINCT adgroup_id), 1)
FROM product_history_adgroup
WHERE
    item_id in (SELECT DISTINCT item_id FROM adwords_offers)
  AND clicks = 0
GROUP BY item_id;
```

### Performance Metrics for ProductOffers

```sql
-- metrics for Offers active in the last ? days.
SELECT
    item_id, adgroup_id, clicks, impressions, date
FROM product_history_adgroup
JOIN adwords_offer using(item_id, adgroup_id)
ORDER BY "date" DESC
LIMIT ?;
```

### Product AdGroup View

```sql
-- ProductAdGroupView
SELECT DISTINCT item_id, adgroup_id
FROM product_history_adgroup ph
JOIN product p USING(item_id)
GROUP BY adgroup_id
ORDER BY "date" DESC;
```

```sql
-- product partition association
SELECT
    item_id,
    criterion_id,
    adgroup_id
FROM product_partition pp
JOIN criterion USING(criterion_id, adgroup_id)
JOIN criterion_product USING(crit_key, item_id)
```


### Product Group Subdivisions

```sql
select
     t2.parent_criterion_id as parent_id,
     t1.parent_criterion_id as criterion_id,
     t2.case_value as casevalue,
     jsonb_pretty(
         to_jsonb(
             array_agg(
                 jsonb_build_object(
                     'criterion_id', t1.criterion_id,
                     'case_value', t1.case_value
                 )
             )
         )
     ) as children
 from adw_product_group t1
     inner join adw_product_group t2 on (
         t1.parent_criterion_id = t2.criterion_id)
 where --t2.case_value ? 'PRODUCT_TYPE_L1' and
     t2.parent_criterion_id = 293946777986
 group by t1.parent_criterion_id, t2.case_value, t2.criterion_id
 order by t1.parent_criterion_id;
```





