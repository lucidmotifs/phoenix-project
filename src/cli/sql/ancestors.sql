SELECT DISTINCT
  "t1"."criterion_id",
  "t1"."parent_id",
  "t1"."dimension_type",
  "t1"."dimension_value"
FROM "product_partition" AS "t2"
INNER JOIN "product_partition" AS "t1"
  ON ("t2"."product_group" ILIKE ("t1"."product_group" || '%'))
WHERE (("t2"."adgroup_id" = 77715562079)
  AND ("t2"."product_group" = '* / product type = "sport & outdoor" / custom label 0 = * / brand = * / custom label 4 = * / product type = "camping & outdoor"'))
ORDER BY "t1"."parent_id"
