WITH product_groups as (
    SELECT
        criterion_id,
        parent_id,
        dimension_type,
        dimension_value,
        array_upper(
            string_to_array(product_group, ' / '), 1) as depth
    FROM product_partition
    WHERE dimension_type != 'ITEM_ID'
    GROUP BY
        dimension_type,
        dimension_value,
        criterion_id,
        product_group,
        parent_id
    ORDER BY dimension_type
)
SELECT
    depth,
    criterion_id,
    parent_id,
    pg.dimension_type,
    pg.dimension_value,
    count(item_id)
FROM product_dimension pd
    RIGHT JOIN product_groups pg ON (
        pd.dimension_type = pg.dimension_type AND
        pd.dimension_value = pg.dimension_value)
WHERE pg.dimension_type != 'ITEM_ID'
GROUP BY
    pg.dimension_type,
    pg.dimension_value,
    criterion_id,
    parent_id,
    depth,
ORDER BY depth, dimension_type;
