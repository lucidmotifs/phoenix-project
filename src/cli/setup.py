from setuptools import setup, find_packages

setup(
    name="adspert-cli",
    version='1',
    author='Paul Cooper',
    packages=find_packages('src', 'lib'),
    package_dir={'': 'src'},
)
