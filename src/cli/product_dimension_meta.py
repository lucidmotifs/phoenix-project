import abc
import dataclasses
import enum
import json
import os
from typing import ClassVar
from typing import Dict
from typing import List
from typing import Optional
from typing import Sequence
from typing import Union

from anytree import NodeMixin, RenderTree, Resolver
from anytree.importer import JsonImporter
from anytree.util import rightsibling
from adspert.adwords.constants import \
    ProductDimensionType, ShoppingProductCondition

from util import bootstrap
from dataclasses import InitVar


importer = JsonImporter()
encoder = json.JSONEncoder()
data = open(os.join(os.path.dirname(__file__),
                    'data/product_dimension_data.json'))
root = importer.import_(data.read())

dimensions = Resolver(pathattr='type')


class ProductDimensionMeta(abc.ABCMeta):
    """Metaclass to interface with dimension data."""

    # def __prepapre__

    def __new__(mcls, name, bases, ns={}):
        cls = super().__new__(mcls, name, bases, ns)
        if cls.__name__ == 'ProductDimension':
            return cls

        cls.xsi_type = name
        cls.__annotations__['xsi_type'] = str

        resource_node = dimensions.get(root, name)
        type_nodes = root.leaves

        if len(resource_node.children) == 1:
            print(f'Creating {resource_node.name} with type {resource_node.children[0].type}')
            d_type = resource_node.children[0].type
            try:
                cls._type = ProductDimensionType(d_type)
            except ValueError as e:
                print(f'{e} -> {ProductDimensionType.__members__}')

        elif NodeMixin in bases:  # hierarchical
            subtypes = [st.type for st in resource_node.children]
            cls.levels = enum.Enum(
                f'{name}Level', subtypes,
            )
            try:
                level = ns.get("level", 1)
                print(f'Creating {name} at level {level}')
                cls._type = ProductDimensionType(subtypes[level - 1])
            except ValueError:
                print(f'dimension type invalid: {ns.get("level")}')
                cls._type = None

        else:  # custom attribute
            index = ns.get('index', 0)
            print(f'Creating Custom Attribute {index}')
            child = dimensions.get(resource_node, f'CUSTOM_ATTRIBUTE_{index}')

            try:
                cls._type = child.type
            except ValueError:
                print(f'invalid index: {ns.get("index")}')
                cls._type = None

        return cls


class ProductDimension(abc.ABC, metaclass=ProductDimensionMeta):

    _type: ProductDimensionType
    value: str

    @property
    @abc.abstractmethod
    def case_value(self) -> Dict[str, Union[str, int]]:
        NotImplemented

    @classmethod
    def get_children(cls, suffix: str = ''):
        return dimensions.glob(
            root, f'/ProductDimension/{cls.__qualname__}/*{suffix}')


@dataclasses.dataclass
class ProductBrand(ProductDimension):

    value: str

    @property
    def case_value(self) -> Dict[str, Union[str, int]]:
        return dataclasses.asdict(self)


@dataclasses.dataclass
class ProductOfferId(ProductDimension):

    value: str

    @property
    def case_value(self) -> Dict[str, Union[str, int]]:
        return dataclasses.asdict(self)


@dataclasses.dataclass
class ProductCanonicalCondition(ProductDimension):

    value: InitVar[str]
    condition: ShoppingProductCondition = dataclasses.field(init=False)

    def __post_init__(self, value):
        self.condition = ShoppingProductCondition[
            value.lstrip('v:').upper()]
        # @TODO
        # requires translations added to enum...

    @property
    def case_value(self) -> Dict[str, str]:
        return dataclasses.asdict(self)


class ProductCustomAttribute(ProductDimension):

    index: int
    value: str  # customer created

    def __init__(self, value, index):
        self.index = index
        self.value = value
        self._type = self.get_children(self.index)

    @property
    def case_value(self):
        return {
            'xsi_type': self.__class__.__qualname__,
            'type': self._type,
            'value': self.value,
        }


class ProductBiddingCategory(NodeMixin, ProductDimension):

    value_map: ClassVar[Dict[str, int]]

    level: int
    value: str  # product biddig id
    localized: str = ''
    country_code: str = 'ZZ'
    canonical_value: int = 0

    def __init__(self, value, level, parent=None, **kwargs):
        self.value = value
        self.level = self.__class__.levels(level)
        self.parent = parent
        self._type = self.level.name
        vars(self).update(kwargs)

    @property
    def case_value(self) -> Dict[str, Union[str, int]]:
        return {
            'xsi_type': self.__class__.__qualname__,
            'type': self._type,
            'value': self.value,
        }

    @classmethod
    def from_string(
        cls, text: str, level: int = Optional[None],
    ) -> 'ProductBiddingCategory':
        value = None
        parent = None
        if text not in cls.value_map:
            # @todo local taxonomy lookup ...
            # also fetch parent if possible
            # parent = ...
            cls.name_map[text] = value, level
        else:
            value, level = cls.name_map[text]
        return cls(value, level, parent=parent)

    @classmethod
    def convert_categories(
        cls, names: Sequence[str], dimension_type: ProductDimensionType,
    ) -> List['ProductBiddingCategory']:
        # @ note take code from existing logic in shopping.py
        pass


if __name__ == '__main__':
    bootstrap.bootstrap()

    print(RenderTree(root).by_attr('type'))

    brand = ProductBrand('nike')
    print(brand.case_value)

    offer = ProductOfferId('1123d')
    print(offer.case_value)

    condition = ProductCanonicalCondition('New')
    condition2 = ProductCanonicalCondition('neu')
    c1 = condition.case_value['condition']
    c2 = condition2.case_value['condition']
    assert c1 == c2, f'{c1} {c2}'
    print(c2)

    category = ProductBiddingCategory(-1123, 1, None)
    print(category.case_value)

    category2 = ProductBiddingCategory(-1234, 2, category)
    print(category2.case_value)

    category3 = ProductBiddingCategory(1244, 3, category2)
    print(category3.case_value)

    custom0 = ProductCustomAttribute('anything', 0)
    print(custom0._type, custom0.case_value['value'])
    custom4 = ProductCustomAttribute('also anything', 4)
    print(custom4.case_value)

    node = dimensions.get(root, 'ProductType')

    def _pre_attach(self, parent):
        parent_type = self._subtypes_[parent._type]
        parent_node = dimensions.get(self._node_, parent_type.name)
        new_node = rightsibling(parent_node)
        self._type = self._subtypes_[new_node.type].name

        return NodeMixin._pre_attach(self, parent)

    ProductType: ProductDimension = type('ProductType', (NodeMixin, ProductDimension,), {
        '__init__': lambda self, value: setattr(self, 'value', value),
        '__doc__': 'A Product Type dimension',
        '__repr__': lambda s: f'ProductType(type={s._type}, value={s.value})',
        '_node_': node,
        'case_value': property(lambda s: dict(xsi_type=s.xsi_type, type=s._type, value=s.value)),
        '_pre_attach': _pre_attach,
    })

    ProductType._subtypes_ = enum.Enum(node.type + 'Level', [n.type for n in node.children])

    print(RenderTree(node))
    print(ProductType._subtypes_.__members__)

    n1 = ProductType('monster')
    n2 = ProductType('truck')
    n3 = ProductType('maddness')
    n3a = ProductType('Tonight')

    n2.parent = n1
    n3.parent = n2
    n3a.parent = n2

    print(RenderTree(n1))
    print(n3a.case_value)
