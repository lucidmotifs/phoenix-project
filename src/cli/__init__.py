import adspert
import util.bootstrap.db as db
import util.bootstrap.app as app

from ._types import AdspertId
from .config import Reader as rconfig


def get_accountdb_uri(adspert_id: AdspertId):
    app.init(rconfig.read('adspert'))
    db.connect_main(rconfig.read('db.main'))
    
    AccountModel = adspert.database.models.main.Account
    acc = AccountModel.get(adspert_id=adspert_id)    

    account_db_uri = f'{acc.db_host}:{acc.db_port}/{acc.db_name}'
    return account_db_uri
