import aenum
import sqlite3
import binascii
import logging
import itertools
from aenum import AutoValue
from collections import deque
from typing import Any, List, Union

import fire
import pandas as pd
from devtools import debug
from anytree import Node

# from adspert.adwords.shopping import get_adw_inventory_query as build_inventory
from adspert.adwords.shopping import get_adw_product_groups
from adspert.adwords.shopping import build_product_group_structure
from adspert.database.models.account import Campaign, Criterion
from adspert.adwords.constants import ProductDimensionType
from adspert.adwords.product import ProductDimension
from adspert.base.app import adspert_app
from adspert.database.db import configure_db, dbs
from adspert.scripts.utils import get_account
from adspert.base import tracelog


log = logging.getLogger(__name__)

conn = sqlite3.connect('shopping.db')
curs = conn.cursor()

DELIM_PATH = b' / '
DELIM_SCOPE = b'&+'
DELIM_CV = b'=='

OTHER = '*'

# Create table
curs.execute("""
    CREATE TABLE IF NOT EXISTS product_group (
        criterion_id int,
        parent_id int,
        dimension_type text,
        dimension_value text,
        product_scope text,
        depth real
    );
""")

# Insert a row of data
# c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()


class AccountLevel(int, aenum.Enum):
    ACCOUNT = 1
    CAMPAIGN = 2
    ADGROUP = 3


class PGNode(Node):
    dim = None
    val = None

    def __init__(self, casevalue: tuple, parent):
        dimension, value = casevalue

        if dimension is None:
            # root node
            self._identifier = 0
            self._tag = 'root node'
        else:
            val = value.strip(b'"') \
                if value is not None else b'*'

            self.val = val.strip()
            self.dim = dimension.strip()

            debug(casevalue)

            # move this to product scope body
            if self.dim.lower() == 'id'.encode():
                self.dim = b'item_id'
            elif self.dim.lower().startswith('category'.encode()):
                self.dim = b'bidding_%s' % self.dim
            elif self.dim.lower().startswith('custom'.encode()):
                dim = self.dim.decode()
                dim = 'custom_attribute_%s' % dim[-1]
                self.dim = dim.encode()

            self._tag = f'{self.dim.decode().upper()} == "{self.val.decode()}"'
            self._digest(parent)

        super().__init__(
            identifier=self._identifier,
            tag=self._tag,
            data=(self.dim, self.val))

    def _digest(self, parent):
        crc = binascii.crc_hqx(self.dim, parent)
        crc = binascii.crc_hqx(self.val, crc)
        self._identifier = crc

    @property
    def tag(self) -> str:
        return self._tag


def adw_get_product_groups(
    criterion_id =- 1,
    criterion_name = '*',
    case_value = (None, None),
    depth=0
):
    print(case_value)
    pass


class ProductPartitionTree(Tree):

    def __init__(self, data: pd.DataFrame):
        super().__init__(node_class = PGNode)
        self.add_node(PGNode((None, None)))

        for cv, row in data.iterrows():
            scope = ProductScope(Criterion(
                criterion_id=row[0], name=row[1]))

            parent = 0  #
            for d, n in enumerate(scope.nodes):
                self.create_node(n, parent=parent)
                parent = n.identifier


class AdwProductDimension(object):
    dtype: ProductDimensionType
    value: Any

    def __init__(self, dtype, value):
        self.dtype = ProductDimensionType(dtype.decode().upper())
        self.value = value.decode()


class ProductScope(object):

    dimensions: List[AdwProductDimension]

    def __init__(self, adwmodel: Union[Campaign, Criterion]):
        if isinstance(adwmodel, Criterion):
            self._parse(adwmodel.name)

    def _parse(self, name: str):
        """Parse a product scope string to dimensoins."""
        arr = bytearray(name.encode())
        self._scope = arr

        cvgen = (p.split(DELIM_CV)
                 for p in arr.split(DELIM_SCOPE))
        # @todo translate dimensions rather than create nodes here!
        self.nodes = deque(map(PGNode, cvgen))
        debug(self.nodes)
        self.dimensions = [AdwProductDimension(node.dim, node.val)
                           for node in self.nodes]

    def scalars(self):
        """Return dimensions as scalar query string compatible with a df."""
        ' & '.join([f'{dtype} == "{value}"'
                    for dtype, value in self.dimensions])


class ProdDimensions(str, aenum.Flag):

    _settings_ = aenum.AutoValue,
    _ignore_ = 'PRODUCT_TYPE'

    def __new__(cls, val, dtype='UNDEFINED'):
        count = len(cls.__members__)
        obj = str.__new__(cls, dtype)
        obj._count = count
        obj._value_ = val
        obj.dtype = dtype
        return obj

    @classmethod
    def _create_pseudo_member_values_(cls, members, *values):
        scope = ' / '.join(m.dtype for m in members)
        print(f'scope: {scope} values: {values}')
        return values + (scope, )

    BRAND = 'brand'                   # BRAND
    CHANNEL = 'channel'               # CHANNEL
    CONDITION = 'c_condition'         # CONDITION
    OFFER_ID = 'id'                   # ITEM_ID

    PRODUCT_TYPE_1 = 64,'product type'
    PRODUCT_TYPE_2 = PRODUCT_TYPE_1[0] << 1, 'product type'
    PRODUCT_TYPE_3 = PRODUCT_TYPE_1[0] << 2, 'product type'
    PRODUCT_TYPE_4 = PRODUCT_TYPE_1[0] << 3, 'product type'
    PRODUCT_TYPE_5 = PRODUCT_TYPE_1[0] << 4, 'product type'


pds = ProdDimensions
pd1 = ProdDimensions.PRODUCT_TYPE_1
pd2 = ProdDimensions.PRODUCT_TYPE_2
pd3 = ProdDimensions.PRODUCT_TYPE_3
pd4 = ProdDimensions.PRODUCT_TYPE_4
pd5 = ProdDimensions.PRODUCT_TYPE_5

ProductTypes = pd1|pd2|pd3|pd4|pd5


class ProductInventory(object):

    def __init__(self, dimensions, load=True, limit=None):
        self.dimensions = dimensions
        self.limit = limit
        if load:
            self._load()

    def _load(self):
        products = build_inventory(self.dimensions)
        if self.limit:
            products = products.limit(self.limit)
        self._frame = pd.DataFrame(
            products.dicts(),
            columns=[
                'item_id',
                'dimension_type',
                'dimension_value',
            ])
        self._frame.drop_duplicates()

    def item_view(self):
        return (
            self._frame
            .pivot(
                index='item_id',
                columns='dimension_type',
                values='dimension_value',
            )
            .sort_values(by=self.dimensions, na_position='last')
            .fillna('*')
        )

    def dimension_view(self):
        df = self.item_view
        return (df.reset_index().set_index(self.dimensions).sort_index())

    def display(self, fn, n: int):
        return fn().head(n)


inventory = ProductInventory(
    dimensions=(
        ProdDimensions.BRAND |
        ProdDimensions.PRODUCT_TYPE_1 |
        ProdDimensions.PRODUCT_TYPE_2
    ), load=False)


def setup_logging(log_file=None):
    logging.basicConfig(filename='logfile.log')


def init_adspert_context():
    # setup_logging()
    adspert_app.init('scripts', 'production')
    configure_db()


inventory: ProductInventory
criterion: Criterion


def inventory(dtypes, limit):
    global inventory
    inv = inventory
    inv = ProductInventory(dtypes, limit=limit)
    inv.display(inv.item_view, 100)


def main():
    init_adspert_context()
    acc = get_account(3281857)
    acc.db_port = 9432

    print(acc.db_host)
    print(acc.db_port)

    dbs.account.connect_params['port'] = 9432
    dbs.account._maintenance = False

    dbs.account.init(
        host=acc.db_host,
        user='adspert_writer',
        password='5ads',
        database=acc.account_name)

    treedata = create_product_group_nodes()
    tree = ProductPartitionTree(treedata)
    tree.show()

    dtypes = treedata.dimension_type.unique().tolist()
    debug(dtypes)

    criterion = Criterion(
        name='product_type_l1==heimwerken & garten&+'
        'product_type_l2==garten & balkon&+'
        'product_type_l3==gartenmöbel&+'
        'product_type_l4==gartenmöbel-sets')
    scope = ProductScope(criterion)


if __name__ == '__main__':
    fire.Fire(main)


"""
DB SCHEMA:


Product:
- item_id (PK)

ProductDimension:

- item_id (FK -> Product)
- dimension_type
- dimension_value

(item_id, dimension_type) PK

adw_case_value (AdwCaseValue)
adw_product_group (AdwCaseValue)
"""

