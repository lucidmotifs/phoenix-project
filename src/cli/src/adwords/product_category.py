import csv
import devtools
import heapq
import logging
import sqlite3
from collections import OrderedDict
from operator import attrgetter

import sqlalchemy
import pandas as pd
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.query import Bundle
from sqlalchemy import Column, Integer, String
from anytree import Node
import tabulate

logging.basicConfig(filename='logfile.log')
log = logging.getLogger()
log.setLevel(logging.DEBUG)
log.info('logging intialized')

DATAFILE = 'adspert/src/adspert/adwords/category-taxonomy.en.csv'

conn = sqlite3.connect('categories.db')


def drop_db(conn):
    # Drop tables
    curs = conn.cursor()
    curs.execute("""
        DROP TABLE IF EXISTS google_product_category;
    """)
    # Save (commit) the changes
    conn.commit()


def create_db(conn):
    curs = conn.cursor()

    curs.execute("""
        CREATE TABLE IF NOT EXISTS google_product_category (
            id int PRIMARY KEY,
            level1 text,
            level2 text,
            level3 text,
            level4 text,
            level5 text
        );
    """)

    curs.execute("""
        INSERT INTO google_product_category (id, level1)
        VALUES (0, 'ROOT');
    """)

    log.info([tuple(r) for r in conn.execute(
            """SELECT * FROM google_product_category""")])
    # Save (commit) the changes
    conn.commit()

BaseSchema = declarative_base()


class ProductCategorySchema(BaseSchema):
    __tablename__ = 'google_product_category'

    id = Column(Integer, primary_key=True)
    level1 = Column(String(60), nullable=False)
    level2 = Column(String(60))
    level3 = Column(String(60))
    level4 = Column(String(60))
    level5 = Column(String(60))


# Initialize inmemory db
engine = sqlalchemy.create_engine('sqlite:///categories.db')

# Create session object
Session = sessionmaker(bind=engine)

# create a configured "Session" class
session = Session()

LEVELS = ['level1', 'level2', 'level3', 'level4', 'level5']

class LevelBundle(Bundle):
    def create_row_processor(self, query, procs, labels):
        """Override create_row_processor to return values as dictionaries"""
        def proc(row):
            return ' / '.join(filter(None, (proc(row) for proc in procs)))
        return proc

Levels = LevelBundle(
    "Levels",
    ProductCategorySchema.level1,
    ProductCategorySchema.level2,
    ProductCategorySchema.level3,
    ProductCategorySchema.level4,
    ProductCategorySchema.level5)


def _write(rows: OrderedDict):
    """Write items to storage."""
    try:
        records = {int(row.pop('id')): row for row in rows}
    except KeyError:
        print(rows[:10])

    session = Session()
    session.add_all(list(
        ProductCategorySchema(id=k, **v)
        for k, v in records.items()
    ))
    log.info(f'Writing {len(session.new)} records')
    session.commit()

    # sort the items so the hierarchy is revealed
    heap = sorted(records.items(), key=lambda i: int(i[0]))
    heapq.heapify(heap)
    print(heapq.nsmallest(1, heap))
    return heap


def load_csv():

    conn = sqlite3.connect('categories.db')
    drop_db(conn)
    create_db(conn)

    with open(DATAFILE) as fp:
        rows = []
        heap = []
        reader = csv.DictReader(fp, None)

        try:
            while reader:
                row = next(reader)
                row.pop('')
                rows.append(row)
                if len(rows) >= 1000:
                    heap = heapq.merge(_write(rows), heap)
                    rows = list()

        except StopIteration:
            it = heapq.merge(_write(rows), heap)
            data = dict(filter(lambda i: i[1] != '', tuple(it)))
            return data


def export():

    data = {}
    session = Session()
    for row in (session
        .query(ProductCategorySchema)
        .filter(ProductCategorySchema.level1=='Animals & Pet Supplies')
        .all()
    ):

        data.update({
            row.id: {
                'level1': row.level1,
                'level2': row.level2,
                'level3': row.level3,
                'level4': row.level4,
                'level5': row.level5,
            },
        })

    df = pd.DataFrame(
        data=data.values(),
        columns=(LEVELS))
    df['id'] = data.keys()

    df.sort_values(LEVELS, inplace=True)
    df.reset_index(inplace=True)

    def group_by_index(orient='table'):
        # group by category level
        df.set_index([*LEVELS], inplace=True)
        df.sort_index(inplace=True)

        print(df.head(50))

        for name in filter(None, df.index[0]):
            print(name)

            lvls = df.groupby(['level1','level2'], level=1)

            for k, v in lvls.groups:

                path = (k, v)
                group = lvls.get_group(path)

                node = Node(
                    name=path.pop(),
                    id=group.id)

                last_path = set(path)
                for curr_path in child.index.values:
                    val = (last_path ^ curr_path)
                    if last_path.issubset(curr_path):
                        if len(val) > 1:
                            # child has children - go deeper
                            print('subtree stuff...')
                            print(val)
                            print(last_path | curr_path)
                        else:
                            print('child-node')
                    else:
                        # pop back up the stack
                        prev = val.pop()

            node = df.xs(name, level=0)
            # print(node.index.unique().to_list())
            # print(node.to_dict(orient='index'))
            node.reset_index(inplace=True)
            print(node)

        # return df.to_json(orient=orient, indent=4)

    def merge_columns():
        # Grouping columns indices
        df.columns = [''.join(tuple(map(str, t)))
                      for t in df.columns.values]

        df.sort_values([*df.columns], inplace=True)
        print(df.head(50))

    with open('temp.json', 'w') as f:
        f.write(df.to_json(orient='table', indent=4))

    return {
        'index': group_by_index,
        'columns': merge_columns,
    }


def _render_item(nid, depth, data):
    log.debug(data)
    n = (depth - len(data)) * 2
    for d in data:
        # print(f'{nid}: {d}')
        f = '{:>%d0}' % n
        n+=1
        print(f.format(d))


def query(id_=None, level=None, value=None):
    """Run query against category table"""

    q = session.query(ProductCategorySchema)

    if id_ is not None:

        print('querying on id')

        q = q.filter(ProductCategorySchema.id==id_)

    elif level is not None:

        log.info(f'fetching all categories at a level {level}')
        cols = attrgetter(*LEVELS[:level])(ProductCategorySchema)

        getter = attrgetter(
            f'level{max(level, 0)}',
            f'level{min(level + 1, 5)}')
        col, ncol = getter(ProductCategorySchema)

        if level < 5:
            q = q.filter(ncol == '')
        if level > 1:
            q = q.filter(col != '')

    elif value is not None:

        log.info('category value lookup')

        q = q.filter(ProductCategorySchema.value==value)

    else:
        log.info('Fetching all categories')

    q = q.order_by()

    session.flush()
    session.execute(q)

    prev = set()
    results = []
    curr_depth = 0
    for row in q.all():
        lvls = [
            row.level1,
            row.level2,
            row.level3,
            row.level4,
            row.level5,
        ]
        lvls = lvls[:level]

        new = set(lvls) - prev

        if new:
            _render_item(row.id, len(lvls), new)
            prev = set(lvls)

        results.append((row.id, lvls))

    return len(results)


if __name__ == '__main__':
    import fire

    fire.Fire({
        'load': load_csv,
        'sync': export,
        'search': query,
    })

    # Close the connection
    conn.close()
