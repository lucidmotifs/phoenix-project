import itertools
import json
import sys
from io import StringIO, BytesIO
from typing import Dict, List

import fire
from devtools import debug
from lxml import etree, objectify
from lxml.etree import XMLParser
from bs4 import BeautifulSoup, SoupStrainer,Tag


# ===========================
#
# ===========================

MAX_ITEMS = 1000

NS = {'g': 'http://base.google.com/ns/1.0'}
FEED = '/home/paulcooper/src/product-feed-data/data/export_google_de.xml'
URL = 'https://www.ostermann.de/backend/export/index/export_google_de.xml?feedID=2&hash=4ef809db82c0a484dvsdew46574573374d5c29'


def truncate_desc(descr):
    # restrict to `length` then append `...`
    pass


# item handler
class ProductItem(object):

    def __init__(self, *base, **attrs):
        self.extra = base
        self.attrs = attrs
        self._handlers = {'description': truncate_desc}
        self.finalize(attrs)

    def _parse_attr(self, tag, data=None):
        """Clean namespacing and convert to python data types."""
        # clean_tag i.e. remove namespace @TODO
        if tag in self._handlers():
            fn = self._handlers[tag]
            data = fn(data)
        else:
            self.attrs[tag] = data

    def finalize(self, attrs):
        for name, value in attrs.items():
            setattr(self, name, value)

    def tojson(self):
        d = vars(self)
        d.pop('_handlers')
        return json.dumps(d)


def _debug(elem, data={}):
    debug(elem.children)
    parser.feed(etree.tostring(elem, with_tail=False).strip())
    parser.close()


def extract_data(elem, data: Dict[str, List]={}) -> Dict:
    debug(elem)
    tag = elem.tag
    text = elem.text

    if text is not None:
        data[tag] = text
    else:
        data[tag] = []
        node_itr = elem.children
        for node in node_itr:
            data[tag].append(extract_data(node, {}))

    return data


def export(data):
    with open('products.json', 'w') as fp:
        fp.write(data)
        print(fp.__sizeof__())
    return


def main(maxitems=20):
    item_nodes = SoupStrainer('item')
    inventory = []
    with open(FEED, 'rb') as fp:
        items = BeautifulSoup(fp.read(1024 * 60), 'xml', parse_only=item_nodes)
        # for item in itertools.islice(items, 0, maxitems):
        for item in items:
            # debug(item.decode())

            attrs = []

            base = {
                'item_id': item.select_one('id', namespace=NS).string,
                'title': item.select_one('title').string,
                'description': item.select_one('description').string,
                'price': item.select_one('price').string or '',
                'availability': item.select_one('availability').string,
            }

            # help(parts['item_id'])

            itr = item.children
            for node in itr:
                #attrs.append(extract_data(node, data={}))
                if isinstance(node, Tag):
                    attrs.append((node.name, node.string))
            else:
                print('Items extracted, building model')
            product = ProductItem(*base.values(), **dict(attrs))
            inventory.append(product.attrs)
            # debug(vars(product))

            if len(inventory) >= maxitems:
                print('Hit result maximum, saving to json and exiting')
                export(json.dumps(inventory, indent=4))
                sys.exit(0)
        else:
            print('Dumping inventory')
            export(json.dumps(inventory, indent=4))
            # debug(json.loads(inventory[0]))



if __name__ == '__main__':
    maxitems = MAX_ITEMS
    fire.Fire(main)

