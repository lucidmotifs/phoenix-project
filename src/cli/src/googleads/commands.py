import dataclasses
import enum
import functools
import logging
import subprocess
import sys
import textwrap
from typing import Any
from typing import Dict
from typing import List

import fire
import tabulate
import pandas as pd

from adspert.googleads import query
from adspert.googleads.client import AdspertGoogleAdsClient as AdsClient
from adspert.googleads.customer import get_customer_manager_id
from adspert.googleads.client import get_client, get_mcc_client
from adspert.scripts.utils import get_account

from .enums import AdGroupCriterionStatusEnum
from .enums import CriterionTypeEnum
from .schema import BiddingStrategySchema
from .schema import CampaignSchema

ADS_PAGE_SIZE = 1000


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


CriterionStatus = AdGroupCriterionStatusEnum.AdGroupCriterionStatus
CriterionType = CriterionTypeEnum.CriterionType


class CommandAction(enum.IntEnum):

    LIST = 1
    ADD = 2
    REMOVE = 3
    EDIT = 4


class CmdGroup:

    def __init__(self, account_id, login: str = None):

        self.client = AdsClient.load_from_storage('google-ads.yaml')
        self.customer_id = account_id

        if not self.client.login_customer_id:
            self.client.login_customer_id = get_customer_manager_id(
                self.client, account_id,
            )

        print(f'logged in customer: {self.client.login_customer_id}')

        ads_service = self.client.get_service('GoogleAdsService')
        self.query = functools.partial(ads_service.search, str(account_id))
        self.mcc_query = functools.partial(
            ads_service.search, self.client.login_customer_id,
        )


class campaign(CmdGroup):

    def __init__(self, account_id):
        super().__init__(account_id)

    def get(self, id=0, name='', status=0, type=0):

        query = f"""
            SELECT
                campaign.campaign_id,
                campaign.name,
                campaign.status
            FROM campaign
        """
        response = self.query(query=query, page_size=ADS_PAGE_SIZE)
        data = CampaignSchema.load(response)


class customer(CmdGroup):
    """customer management command group."""

    def accessible(self):

        customer_service = self.client.get_service('CustomerService')
        accessible_customers = customer_service.list_accessible_customers()
        resource_names = accessible_customers.resource_names

        print('# results: %i' % len(resource_names))
        print(resource_names)

    def info(self):

        query = f"""
            SELECT
                customer_client.resource_name,
                customer_client.client_customer,
                customer_client.level
            FROM customer_client
        """

        template = """
            {customer_name} ({customer_id})
            {timezone} - {currency}

            manager?            {manager}
            partner?            {partner_badge}
            test account?       {test_account}

            conv. tracking id: {conversion_tracking_id}

            {resource_name}
        """

        response = self.query(query=query, page_size=ADS_PAGE_SIZE)

        for row in response:
            rn = row.customer_client.client_customer.value
            print(f'customer resource name: {rn}')

            svc = self.client.get_service('CustomerService')
            customer = svc.get_customer(rn)

            lk = {
                'customer_name': customer.descriptive_name.value,
                'customer_id': customer.id.value,
                'resource_name': customer.resource_name,
                'timezone': customer.time_zone.value,
                'currency': customer.currency_code.value,
                'test_account': customer.test_account.value,
                'manager': customer.manager.value,
                'partner_badge': customer.has_partners_badge.value,
                'conversion_tracking_id': (
                    customer
                    .conversion_tracking_setting
                    .conversion_tracking_id
                    .value,
                ),
            }

            print(textwrap.dedent(template.format_map(lk)))


class bidding(CmdGroup):

    def __init__(self, account_id):

        super().__init__(account_id)
        self.str = self.strategies()

    def strategies(self, bsid=None):

        q = query.Query(resource='bidding_strategy')
        q.fields.update(*BiddingStrategySchema.fields)
        q.orderby = f'{BiddingStrategySchema.type} ASC'
        q.limit = 10

        gql = q.gql(neat=True)
        response = self.query(query=gql, page_size=10)
        data = BiddingStrategySchema.load(response, many=True)
        return data

    def adjustments(self, criteria_type=None):
        pass


class targeting(CmdGroup):

    def dimension(self, dimension_type):
        """Get criteria specified by targeting dimensions."""
        pass

    def dimension_structure(self, critera_type, account_level):
        pass

    def bid_modifier_structure(self, criteria_type):
        pass


class criteria(CmdGroup):

    def __init__(self, account_id, account_level: str = 'campaign'):
        super().__init__(account_id)
        self.level = account_level

        print(self.client.customer_id)
        print(self.client.login_customer_id)

    def types(self):
        pass

    def view(self):
        pass

    def audience_structure_adgroup(self, *adgroup_ids):
        query = """
            SELECT
                ad_group_audience_view.resource_name,
                ad_group_criterion.criterion_id,
                ad_group_criterion.negative,
                ad_group_criterion.type,
                ad_group_criterion.status,
                ad_group_criterion.bid_modifier,
                ad_group_criterion.effective_cpc_bid_micros,
                ad_group.id
            FROM ad_group_audience_view
        """
        response = self.query(query=query, page_size=ADS_PAGE_SIZE)

        data = []
        for row in response:
            criterion = row.ad_group_criterion
            adgroup = row.ad_group

            data.append(
                {
                    'adgroup_id': adgroup.id.value,
                    'criterion_id': criterion.criterion_id.value,
                    'criterion_type': CriterionType.Name(criterion.type),
                    'is_negative': criterion.negative.value,
                    'status': CriterionStatus.Name(criterion.status),
                    'bid_modifier': criterion.bid_modifier.value,
                    'eff_cpc': criterion.effective_cpc_bid_micros.value,
                }
            )

        out = tabulate.tabulate(data, headers='keys')
        pager = subprocess.Popen(
            ['less', '-F', '-R', '-S', '-X', '-K'],
            stdin=subprocess.PIPE,
            stdout=sys.stdout,
        )
        pager.stdin.write(out.encode())
        pager.stdin.close()
        pager.wait()


