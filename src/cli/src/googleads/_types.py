"""Product group criterion object."""
import dataclasses
from abc import ABC
from dataclasses import InitVar
from typing import List, Union, ClassVar

from .enums import (
    AdGroupCriterionStatusEnum, AdGroupStatusEnum, AdTypeEnum, AdGroupTypeEnum,
    BiddingSourceEnum, CriterionTypeEnum, DeviceEnum,
    ProductGroupInfoTypeEnum, TargetingDimensionEnum,
    CriterionSystemServingStatusEnum, AdGroupCriterionApprovalStatusEnum,
    ProductTypeLevelEnum)

AdType = AdTypeEnum.AdType
AdGroupType = AdGroupTypeEnum.AdGroupType
CriterionType = CriterionTypeEnum.CriterionType
Device = DeviceEnum.Device
ListingGroupType = ProductGroupInfoTypeEnum.ProductGroupInfoType
TargetingDimension = TargetingDimensionEnum.TargetingDimension
AdGroupCriterionStatus = AdGroupCriterionStatusEnum.AdGroupCriterionStatus
AdGroupStatus = AdGroupStatusEnum.AdGroupStatus
BiddingSource = BiddingSourceEnum.BiddingSource
CriterionSystemServingStatus = CriterionSystemServingStatusEnum.platform
AdGroupCriterionApprovalStatus = AdGroupCriterionApprovalStatusEnum.platform
ProductTypeLevel = ProductTypeLevelEnum.ProductTypeLevel

class OneofUnion:
    pass


# ===========================================================================+
# Criterion Types
# ===========================================================================+

class Keyword: pass
class Placement: pass
class MobileAppCategory: pass
class MobileApplication: pass
class AgeRangeInfo: pass
class ParentalStatusInfo: pass
class UserList: pass
class ListingGroupInfo(object): pass
class GenderInfo: pass
class IncomeRangeInfo: pass
class YouTubeVideoInfo: pass
class YouTubeChannelInfo: pass
class TopicInfo: pass
class UserInterestInfo: pass
class AppPaymentModel: pass
class CustomAffinityInfo: pass
class CustomIntentInfo: pass
class Webpage: pass


# ===========================================================================+
# Ad Types
# ===========================================================================+

class AppAdInfo: pass
class TextAdInfo: pass
class GmailAdInfo: pass
class ImageAdInfo: pass
class ImageAdInfo: pass
class VideoAdInfo: pass
class ResponsiveSearchAdInfo: pass
class ExpandedDynamicSearchAdInfo: pass
class ShoppingSmartAdInfo: pass
class ShoppingProductAdInfo: pass
class ShoppingComparisonListingAdInfo: pass


# ===========================================================================+
# Type Unions
# ===========================================================================+

class Criterion(OneofUnion):

    keyword: Keyword
    placement: Placement
    mobile_app_category: MobileAppCategory
    mobile_application: MobileApplication
    listing_group: ListingGroupInfo
    age_range: AgeRangeInfo
    gender: GenderInfo
    income_range: IncomeRangeInfo
    parental_status: ParentalStatusInfo
    user_list: UserList
    youtube_video: YouTubeVideoInfo
    youtube_channel: YouTubeChannelInfo
    topic: TopicInfo
    user_interest: UserInterestInfo
    webpage: Webpage
    app_payment_model: AppPaymentModel
    custom_affinity: CustomAffinityInfo
    custom_intent: CustomIntentInfo


# ===========================================================================+
# Info Messages
# ===========================================================================+

# @resource('ListingGroup')
class ListingGroupInfo(object):
    """Nested resource describing a shopping ad group criterion."""
    pass


class UrlCollection(object):
    pass


# ===========================================================================+
# Shopping Structure Types
# ===========================================================================+

class ProductDimension(object):
    pass


class ProductBiddingCategory(ProductDimension):
    pass


@dataclasses.dataclass
class ProductScope(object):
    """Collection of product dimensions.

    When applied to an account entity (campaign / ad group) a product filter
    is applied and the products included at this level are refined.

    """
    dimensions: List[ProductDimension]


@dataclasses.dataclass(init=False)
class ProductGroup(object):
    """Refines a product partition product filter."""
    criterion_id: int
    casevalue: ProductDimension


@dataclasses.dataclass(init=True)
class ProductPartition(AdGroupCriterion):
    """Google Shopping criterion."""
    listing_group: ListingGroupInfo = None
    criterion_type: Criterion = CriterionType.LISTING_GROUP


if __name__ == "__main__":
    pass