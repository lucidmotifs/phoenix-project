import logging
import pprint
import subprocess
import sys
import typing
from typing import Any
from typing import List
from typing import Dict

import fire
import tabulate
import google.protobuf.message
from google.ads.google_ads.client import GoogleAdsClient
from google.protobuf import wrappers_pb2 as wrappers
from google.protobuf.descriptor import FieldDescriptor as Field
from google.protobuf.json_format import MessageToDict

from adspert.googleads import api
from adspert.googleads import customer
from adspert.googleads import enums
from adspert.googleads import util

PAGE_SIZE = 1000


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)


FIELD_TYPES = {
    Field.TYPE_INT32: 'INT32',
    Field.TYPE_INT64: 'INT64',
    Field.TYPE_BOOL: 'BOOL',
    Field.TYPE_STRING: 'STRING',
    Field.TYPE_GROUP: 'GROUP',
    Field.TYPE_MESSAGE: 'MESSAGE',
    Field.TYPE_ENUM: 'ENUM',
}


def render_data_with_paging(data: List[Dict[str, Any]]):
    out = tabulate.tabulate(data, headers='keys')

    pager = subprocess.Popen(
        ['less', '-F', '-R', '-S', '-X', '-K'],
        stdin=subprocess.PIPE,
        stdout=sys.stdout,
    )

    pager.stdin.write(out.encode())
    pager.stdin.close()
    pager.wait()


def get_message_field_value(field: Field, value: typing.Any):
    """Detemine the scalar value of a field."""
    # pick resource name out predictably
    if field.type == field.TYPE_STRING:
        return value

    # reached scalar field -->> return value
    if type(value) in (
        wrappers.Int64Value,
        wrappers.StringValue,
        wrappers.BoolValue,
        wrappers.BytesValue,
        wrappers.FloatValue,
    ):
        return value.value

    elif field.type == field.TYPE_MESSAGE:
        # recursively unpack messages
        flds = {
            field.name: get_message_field_value(field, value)
            for field, value in value.ListFields()
        }
        return flds

    elif field.type == Field.TYPE_ENUM:
        enum_value_map = field.enum_type.values_by_number

        if isinstance(value, typing.Sequence):
            return [enum_value_map[v].name for v in list(value)]

        value = enum_value_map[value].name

    return value


def run_query(client, query: str, customer_id: str = ''):
    """Run a query wrapped with exception handling."""
    svc = client.get_service('GoogleAdsService')
    cstid = str(customer_id)
    assert cstid, 'operating customer id not set'
    return svc.search(cstid, query=query)


def mcc_query(client, query: str, mcc_id: str = ''):
    with api.ExceptionWrapper():
        svc = client.get_service('GoogleAdsService')
        client.login_customer_id = mcc_id
        return svc.search(mcc_id, query=query)


def get_customer(client, customer_id: str):
    """Make simple service request to verify client api access."""
    customer_service = client.get_service('CustomerService')
    resource = customer_service.customer_path(customer_id)
    customer = customer_service.get_customer(resource)
    return customer


class mcc:

    def __init__(self, account_id, login: str = ''):
        self.client = GoogleAdsClient.load_from_storage('google-ads.yaml')
        self.customer_id = account_id

        self.client.__class__.run_query = run_query
        self.client.__class__.mcc_query = mcc_query

        print(f'customer details: {self.customer_id}')
        print(f'login customer: {self.client.login_customer_id}')
        customer = get_customer(self.client, self.customer_id)

        #print(pprint.pformat({
        #    field.name: get_message_field_value(field, value)
        #    for field, value in customer.ListFields()
        #}, depth=1))

    def manager_links(self, customer_id):
        customer_id = customer_id or self.customer_id
        print(f'finding manager links for: {customer_id}')

        manager_customer_id = (
            self.client.login_customer_id or
            customer.get_manager_customer_id(
                self.client,
                customer_id,
            ),
        )

        print(f'login customer id: {manager_customer_id}')

        query = f"""
            SELECT customer_manager_link.resource_name,
              customer_manager_link.manager_customer,
              customer_manager_link.manager_link_id,
              customer_manager_link.status
            FROM customer_manager_link
        """

        response = self.client.run_query(
            query=query, customer_id=customer_id,
        )

        data = []
        for row in response:
            manager_link = row.customer_manager_link

            data.append(
                {
                    'manager_customer': manager_link.manager_customer.value,
                    'manager_link_id': manager_link.manager_link_id.value,
                    'status': manager_link.status,
                },
            )

        out = tabulate.tabulate(data, headers='keys')
        pager = subprocess.Popen(
            ['less', '-F', '-R', '-S', '-X', '-K'],
            stdin=subprocess.PIPE,
            stdout=sys.stdout,
        )
        pager.stdin.write(out.encode())
        pager.stdin.close()
        pager.wait()

        yield next(iter(data))

    def client_customer_links(self, paths):
        customer_id = self.customer_id
        print(f'client links to: {customer_id}')

        manager_customer_id = (
            self.client.login_customer_id or
            customer.get_manager_customer_id(self.client, customer_id)
        )

        print(f'login customer id: {manager_customer_id}')

    def manager_nodes(self, at_depth: int = 1):
        # required code before mcc queries...
        manager_customer_id = (
            self.client.login_customer_id or
            customer.get_manager_customer_id(self.client, self.customer_id)
        )
        print(manager_customer_id)

        rows = customer.get_submanagers(
            self.client, manager_customer_id, at_depth=at_depth)

        mgr = next(rows, None)

        data = []
        while mgr:
            if mgr.ByteSize() <= 0:
                print('empty record')
                continue
            manager = mgr
            data.append(MessageToDict(manager))
            rn = data[-1].pop('resource_name')
            print(rn)
            mgr = next(rows, None)

        render_data_with_paging(data)

    def customer_client_nodes(self, distance: int = 1, exclude_managers=False):
        """Display all child members of an mcc manager customer."""
        filters = []

        if exclude_managers:
            filters.append('customer_client.manager = %s' % 'false')

        extra_filters = '\n  AND '.join(filters)

        query = f"""
            SELECT customer_client.resource_name,
              customer_client.id,
              customer_client.level,
              customer_client.manager,
              customer_client.descriptive_name,
              customer_client.client_customer
            FROM customer_client
            WHERE customer_client.level <= {distance}
              AND customer_client.test_account = false
              AND customer_client.hidden = false {extra_filters}
            ORDER BY customer_client.manager DESC,
              customer_client.level ASC,
              customer_client.descriptive_name ASC
        """

        response = self.client.mcc_query(query=query)

        data = []
        for row in response:
            customer = row.customer_client
            data.append(
                {
                    'customer_id': customer.id.value,
                    'name': customer.descriptive_name.value,
                    'manager': 'Yes' if customer.manager.value else '',
                    'resource_name': customer.client_customer.value,
                    'dist': customer.level.value,
                },
            )

        render_data_with_paging(data)

    def show_hierarchy(self, max_depth):
        """Requires an mcc customer id as the operating customer."""
        manager_customer_id = (
            self.client.login_customer_id or
            customer.get_manager_customer_id(self.client, self.customer_id)
        )

        print(f'login customer id: {manager_customer_id}')

        rows = customer.get_managed_customers(
            self.client, manager_customer_id, distance=max_depth)

        mgr = next(rows, None)

        data = []
        while mgr:
            if mgr.ByteSize() <= 0:
                print('empty record')
                continue

            manager = mgr

            data.append({
                field.name: get_message_field_value(field, value)
                for field, value in manager.ListFields()
            })
            rn = data[-1].pop('resource_name')

            mgr = next(rows, None)

        render_data_with_paging(data)

    def search(self):
        """Find an account in the mcc hierarchy."""
        customer_id = self.customer_id

        # required code before mcc queries...
        manager_customer_id = (
            self.client.login_customer_id or
            customer.get_manager_customer_id(self.client, self.customer_id)
        )
        print(f'searching for : {customer_id}')
        print(f'starting at: {manager_customer_id}')

        rows = customer.get_customer_clients(
            self.client, manager_customer_id, customer_id)
        cust = next(rows, None)

        data = []
        while cust:
            data.append({
                field.name: get_message_field_value(field, value)
                for field, value in cust.ListFields()
            })
            rn = data[-1].pop('resource_name')
            log.debug(rn)
            cust = next(rows, None)

        render_data_with_paging(data)


if __name__ == '__main__':
    fire.Fire(mcc)
