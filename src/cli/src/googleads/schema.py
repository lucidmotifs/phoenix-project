"""Resources are the core entities in the adsapi universe. Messages and services
are either directly related to them, or are the result of a direct combination of them.

They are supported by descriptors (onesof) and nested types that extend their functionality
by either a) specializing them (ad_group_criterion + oneof mixin = Keyword Criterion) and b)
providing more granular data points (nested types).

In terms of real world use cases, resources can:

- provide Schemas used by Sync objects to pull datasets. Since a schema is essentially
a composition of query and resource, and a query just a resource and certains fields (+filters)
if a resource exposes a fieldset from a callable, the schema decorator refine it into a useuable
object for a report or a sync task.

    e.g. (Campaign resource)
    >>> def sync_structure(self, status_value):
    >>>     g = (f.name for f in fields() where hastag('sync')(f))
    >>>        fi = ffilter(fields.get('status'), status_value)
    >>>     return {'fields': set(g), 'filters': list(fi)}

This could be shortened by subscribing Fields campaign.fields()['sync'] (which returns all fields)
where the metadata value for the 'key' is truthy, then campaign.field('status') > PAUSED. This works
because the status field is an enum, so value greater than paused = active and the operator overload
will return the 'correct' filter for this.

There are two main metadata points for each field: Enum or Attribute (determines operator overloads) and
its datatype. Therefore these are stored in the local context - enum string values require pinging the fieldproxy.

FieldProxy: there is only one instance each GoogleAdsField kept at any time. Even when many instances
of a resource have been spawned, they are all pointing to the same field thanks to the FieldProxy.

Once a resource has been registered, any future instances use the same class instance to spawn new instances.
Therefore, the same class level object Proxy refernces are copied across and each Field is the
same Field.

"""
import abc
import typing
import wrapt
import fire
import inspect
import dataclasses
import marshmallow
import pprint as pp
from marshmallow import fields, Schema
from typing import Dict, List, Callable, NamedTuple
from typing import get_type_hints

from google.ads.google_ads.client import GoogleAdsClient
import google.ads.google_ads.v3.types as Resources

from .enums import AccountBudgetStatusEnum
from .enums import AdGroupCriterionApprovalStatusEnum
from .enums import AdGroupCriterionStatusEnum
from .enums import AdGroupStatusEnum
from .enums import AdGroupTypeEnum
from .enums import AdTypeEnum
from .enums import BiddingSourceEnum
from .enums import BiddingStrategyTypeEnum
from .enums import CampaignStatusEnum
from .enums import CampaignTypeEnum
from .enums import CriterionSystemServingStatusEnum
from .enums import CriterionTypeEnum
from .enums import DeviceEnum
from .enums import ProductGroupInfoTypeEnum
from .enums import ProductTypeLevelEnum
from .enums import TargetingDimensionEnum
from .wrappers import Message as MessageProxy

AdType = AdTypeEnum.AdType
AdGroupType = AdGroupTypeEnum.AdGroupType
BiddingStrategyType = BiddingStrategyTypeEnum.BiddingStrategyType
CampaignType = CampaignTypeEnum.AdvertisingChannelType
CriterionType = CriterionTypeEnum.CriterionType
ListingGroupType = ProductGroupInfoTypeEnum.ProductGroupInfoType

AccountBudgetStatus = AccountBudgetStatusEnum.AccountBudgetStatus
AdGroupStatus = AdGroupStatusEnum.AdGroupStatus
AdGroupCriterionStatus = AdGroupCriterionStatusEnum.AdGroupCriterionStatus
AdGroupCriterionApprovalStatus = AdGroupCriterionApprovalStatusEnum
CampaignStatus = CampaignStatusEnum.CampaignStatus
CriterionSystemServingStatus = CriterionSystemServingStatusEnum

BiddingSource = BiddingSourceEnum.BiddingSource
Device = DeviceEnum.Device
ProductTypeLevel = ProductTypeLevelEnum.ProductTypeLevel
TargetingDimension = TargetingDimensionEnum.TargetingDimension


@wrapt.decorator
def universal(wrapped, instance, args, kwargs):
    if instance is None:
        if inspect.isclass(wrapped):
            print('Decorator was applied to a class.')
            return wrapped(*args, **kwargs)
        else:
            # Decorator was applied to a function or staticmethod.
            return wrapped(*args, **kwargs)
    else:
        if inspect.isclass(instance):
            # Decorator was applied to a classmethod.
            return wrapped(*args, **kwargs)
        else:
            # Decorator was applied to an instancemethod.
            return wrapped(*args, **kwargs)


class Resource(abc.ABC):

    __fields__: Dict[str, GoogleAdsField]

    def __init_subclass__(cls):
        print(f'Init resource type {cls}')

    def __subclasshook__(self):
        self = type(self.__name__, (Resource,), dict(vars(self)))
        print(f'Resource subhook - created a {self.__name__}')
        return True
        
    def collect_artifacts(self):
        descr = self.MESSAGE.DESCRIPTOR
        fields = metadata.get(name, attributes=True)

    @abc.abstractmethod
    def message(self):
        NotImplemented
        
    @abc.abstractmethod
    def service(self)
        NotImplemented


# =============================================================================
# Campaign
# =============================================================================

class CampaignResource(Resource):

    MESSAGE = MessageProxy('Campaign')

    def message(self):
        msg = self.MESSAGE.__new__()
        return msg


class CampaignSchema(Schema):
    """Base Campaign schema."""
    name = fields.Str()
    campaign_id: fields.Int(strict=True)
    status: CampaignStatus = fields.Integer()
    type: CampaignType = fields.Integer()


class BiddingStrategySchema(Schema):
    id = fields.Str()
    status = fields.Int()
    name = fields.Str()
    type = fields.Integer()
    campaign_count = fields.Int()


# =============================================================================
# AdGroup
# =============================================================================

class AdGroupSchema(Schema):
    """Base AdGroup schema."""
    id = fields.Int(strict=True)
    name = fields.Str()
    type: AdGroupType = fields.Integer()
    campaign = fields.Nested(
        lambda: CampaignSchema(only=('campaign_id',)),
        dump_only=True, many=True)
    cpc_bid_micros = fields.Int()
    cpm_bid_micros = fields.Int()


# =============================================================================
# Criterion
# =============================================================================

class CriterionSchema(Schema):
    id = fields.Int(strict=True)
    name = fields.Str()
    type: CriterionType = fields.Integer()


class CampaignCriterionSchema(CriterionSchema):
    campaign = fields.Nested(
        lambda: CampaignSchema(only=('campaign_id',)),
        dump_only=True)


class AdGroupCriterionSchema(CriterionSchema):
    adgroup = fields.Nested(
        lambda: AdGroupSchema(only=('adgroup_id',)),
        dump_only=True)
    # only one of the below can be set per instance.
    listing_group = fields.Nested(
        lambda: ListingGroupSchema(only=('case_value')), many=False)


# =============================================================================
# Product (Group, Scope and Dimensions)
# =============================================================================

# ProductScope
class ListingScopeSchema(Schema):
    dimensions = fields.List[ListingDimensionSchema]


# Product Group
class ListingGroupSchema(Schema):
    type: ListingGroupType = fields.Integer(CriterionType.LISTING_GROUP)
    parent_criterion = fields.Nested(
        lambda: ListingGroupSchema(only=('id',)),
        dump_only=True)
    case_value = fields.Nested(ListingDimensionSchema)


class ListingDimensionSchema(Schema):
    product_bidding_category = fields.Nested(ProductBiddingCategorySchema)
    product_brand = fields.Inferred()
    product_channel = fields.Integer()
    product_channel_exclusivity = fields.Integer()
    product_condition = fields.Integer()
    product_custom_attribute = fields.Nested(ProductCustomAttribute)
    product_item_id = fields.Str()
    product_type = fields.Nested(ProductTypeSchema)


class ProductBiddingCategorySchema(Schema):
    id = fields.Int()
    value = fields.Str()
    country_code = fields.String()
    level: ProductBiddingCategoryLevel = fields.Integer()


class ProductCustomAttribute(Schema):
    index: ProductCustomAttributeIndex = fields.Int()
    value = fields.Str()


class ProductTypeSchema(Schema):
    value = fields.Str()
    level: ProductTypeLevel = fields.Integer()

