"""Retrieve artifact metadata.

The metadata retrieved can provide additional context about the artifact,
such as whether it is selectable, filterable, or sortable. The artifact can be
either a resource (such as customer, or campaign) or a field (such as
metrics.impressions, campaign.id). It also shows the data type and artifacts
that are selectable with the artifact.

Other options are to select by resource (-r), which will return all child fields
of the provided resource, recursively.
"""


import argparse
import sys
import google.ads.google_ads.client

from google.protobuf.json_format import MessageToJson


_DEFAULT_PAGE_SIZE = 1000


def _is_or_is_not(bool_value):
    """Produces display text for whether metadata is applicable to artifact.

    Args:
        bool_value: a BoolValue instance.

    Returns:
        A str with value "is" if bool_value is True, else "is not".
    """
    return 'is' if bool_value.value else 'isn\'t'


def main(client, page_size, resource=None, artifact=None):
    from pprint import pprint
    gaf_service = client.get_service('GoogleAdsFieldService', version='v2')

    # Searches for an artifact with the specified name.
    query = """
        SELECT name,
            category,
            selectable,
            filterable,
            sortable,
            selectable_with,
            attribute_resources,
            enum_values,
            data_type,
            is_repeated
        """
    if artifact:
        query += f"WHERE name = '{artifact}'"
    elif resource:
        query += f"WHERE name LIKE '{resource}.%' AND name NOT LIKE '{resource}.%.%'"

    response = gaf_service.search_google_ads_fields(
        query=query, page_size=page_size)
    # Iterates over all rows and prints out the metadata of the returned
    # artifacts.
    
    try:
        for google_ads_field in response:
            import json

            js = MessageToJson(google_ads_field)
            metadata = json.dumps(js)
            print(js)

    except google.ads.google_ads.errors.GoogleAdsException as ex:
        print('Request with ID "%s" failed with status "%s" and includes the '
              'following errors:' % (ex.request_id, ex.error.code().name))
        for error in ex.failure.errors:
            print('\tError with message "%s".' % error.message)
            if error.location:
                for field_path_element in error.location.field_path_elements:
                    print('\t\tOn field: %s' % field_path_element.field_name)
        sys.exit(1)


if __name__ == '__main__':
    # GoogleAdsClient will read the google-ads.yaml configuration file in the
    # home directory if none is specified.
    google_ads_client = (google.ads.google_ads.client.GoogleAdsClient
                         .load_from_storage())

    parser = argparse.ArgumentParser(
        description='Lists metadata for the specified artifact.')
    # The following argument(s) should be provided to run the example.
    parser.add_argument('-a', '--artifact_name', type=str,
                        required=False, default=None,
                        help='The name of the artifact for which we are '
                        'retrieving metadata.')
    parser.add_argument('-r', '--resource', type=str,
                        required=False, default=None,
                        help='The resource group we are selecting fields'
                        'from.')
    args = parser.parse_args()

    main(google_ads_client, _DEFAULT_PAGE_SIZE, artifact=args.artifact_name, resource=args.resource)
