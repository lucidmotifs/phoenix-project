"""Resource fields."""
import abc
import dataclasses
from enum import Enum
from typing import Dict
from typing import List
from typing import ClassVar
from typing import Type, TypeVar
from types import MappingProxyType
from typing import TypedDict

import fire
import wrapt
from google.protobuf.descriptor import Descriptor as ProtocolDescriptor
from google.protobuf.message import Message as ProtocolMessage
from google.ads.google_ads.client import GoogleAdsClient

from adspert.base.data_structures import attrdict
from adspert.googleads.metadata import get_resource_fields


class ClassPropertyDescriptor(object):
    """Meta object used to create the class property decorator."""

    def __init__(self, fget, fset=None):
        self.fget = fget
        self.fset = fset

    def __get__(self, obj, klass=None):
        if klass is None:
            klass = type(obj)
        return self.fget.__get__(obj, klass)()

    def __set__(self, obj, value):
        if not self.fset:
            raise AttributeError("can't set attribute")
        type_ = type(obj)
        return self.fset.__get__(obj, type_)(value)

    def setter(self, func):
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.fset = func
        return self


def classproperty(func):
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)
    return ClassPropertyDescriptor(func)


resource_definitions = {
    # AccountBudget
    'account_budget': {
        'message': 'AccountBudget',
    },
    # Ad related to AdGroup
    'ad_group': {
        'message': 'AdGroup',
    },
    # relationship: ad group - ads
    'ad_group_ad': {
        'message': 'AdGroupAd',
        'typeof': 'relationship',
        'kind': 'composition',
        'relates': ['AdGroupAd', 'Ad'],
    },
    # A campaign.
    'campaign': {
        'message_class': 'Campaign',
    },
    # A campaign budget.
    'campaign_budget': {
        '__doc__': 'A campaign budget.',
        '__module__': '..resource',
        # meta attributes...
        'message_class': 'CampaignBudget',
    },
}


# ----------------------------------------------------------------------------
#   Metric Fields
# ----------------------------------------------------------------------------

class Metrics(object):

    prefix = 'metrics'

    fields = attrdict(
        clicks='clicks',
        cost_micros='cost_micros',
        engagements='engagements',
        impressions='impressions',
        conversions='conversions',
        conversions_value='conversions_value',
    )

    @classproperty
    def clicks(self):
        """Times your ad or your site's listing in the unpaid results was
           clicked.
        """
        return '%s.%s' % (self.prefix, self.fields.clicks)

    @classproperty
    def cost(self):
        """The sum of your cost-per-click (CPC) and cost-per-thousand
           impressions (CPM) costs during this period.
        """
        return '%s.%s' % (self.prefix, self.fields.cost_micros)

    @classproperty
    def engagements(self):
        """The number of engagements.

        An engagement occurs when a viewer expands your Lightbox ad.
        Also, in the future, other ad types may support engagement metrics.
        """
        return '%s.%s' % (self.prefix, self.fields.engagements)

    @classproperty
    def impressions(self):
        """Count of how often your ad has appeared on a search results
           page or website on the Google Network.
        """
        return '%s.%s' % (self.prefix, self.fields.impressions)

    @classproperty
    def conversions(self):
        """The number of conversions.

        This only includes conversion actions which
        include_in_conversions_metric attribute is set to true.
        """
        return '%s.%s' % (self.prefix, self.fields.conversions)

    @classproperty
    def conversions_value(self):
        """The total value of conversions.

        This only includes conversion acls.fields.engagements,
        _conversions_metric attribute is set to true.
        """
        return '%s.%s' % (self.prefix, self.fields.conversions_value)

    @classmethod
    def conversion_history(cls) -> set:
        return {
            cls.conversions,
            cls.conversions_value,
        }

    @classmethod
    def main_history(cls) -> set:
        return {
            cls.clicks,
            cls.cost,
            cls.impressions,
        }

    @classmethod
    def combined_history(cls) -> set:
        return set().union(cls.conversion_history(), cls.main_history())

    def __str__(self):
        return super().__str__()

    def __repr__(self):
        return str(self.fields)


# ----------------------------------------------------------------------------
#   Resource Fields
# ----------------------------------------------------------------------------
import pprint as pp


def _transform_resource_name(name: str) -> str:
    """Transform a resource classname into the version used in queries."""
    name = name[0].lower() + name[1:]

    repl: str = ''
    for i, char in enumerate(name):
        if char.isupper():
            repl = name.replace(char, f'_{char.lower()}', 1)
    return repl


def make_resource_fieldset(
    name: str, descr: ProtocolDescriptor,
) -> Dict[str, str]:

    fields: Dict[str, str] = dict(descr.fields_by_name)
    enum_types: List[str] = [enum for enum in descr.enum_types_by_name]
    nested_types: Dict[str, ProtocolMessage] = dict(descr.nested_types_by_name)
    oneof_fields: Dict[str, ProtocolMessage] = dict(descr.oneofs_by_name)

    # fields of primitive types, no reduction needed
    attributes = set(fields).difference(nested_types, enum_types)

    pp.pprint(f'attributes: {attributes}')
    pp.pprint(f'enum_types: {enum_types}')
    pp.pprint(f'nested_types: {nested_types}')
    pp.pprint(f'oneof_fields: {oneof_fields}')

    return list(get_resource_fields(name, attributes))


def get_typed_attributes(cls):
    attrs = vars(cls).get('__annotations__')
    typed = filter(lambda item: isinstance(item[1], type), attrs.items())
    return dict((k, v) for k, v in typed)


def create_field(idx: int, name: str, hint: type, default=None):
    return dataclasses.field(name, hint, default, metadata={'index': idx})


@dataclasses.dataclass
class Resource(object):
    _meta: ClassVar[MappingProxyType]
    name: ClassVar[str] = ''

    fields = attrdict(
        id='id',
        name='name',
        status='status',
        type='type',
    )

    @classproperty
    def meta(self):
        return attrdict(self._meta)

    @classmethod
    def field_path(cls, field: str) -> str:
        return f'{cls.name}.{field.name}'

    @classmethod
    def sync_structure(cls) -> set:
        return set(['.'.join((cls.name, v)) for v in Resource.fields.values()])

    def __init_subclass__(cls):
        cls._meta = MappingProxyType(resource_definitions.get(cls.name, {}))

    def __str__(self):
        return pp.pformat(self.fields)


class FieldProxy(wrapt.ObjectProxy):
    """Wrap a googleads protobuf messages with our own resource concrete class.

    To extend the message type with properties and methods that integrate it
    with our database models and platform independent modules.

    """
    def __init__(self, wrapped: ProtocolMessage, wrapper: Resource):
        super(FieldProxy, self).__init__(wrapped)
        self._self_wrapper = wrapper
        print(f'Wrapping protobuf message with {self.__class__}')


class ProtobufWrapper(wrapt.ObjectProxy):
    def __init__(self, wrapped: ProtocolMessage, wrapper: Resource):
        super(ProtobufWrapper, self).__init__(wrapped)
        self._self_wrapper = wrapper
        print(f'Wrapping protobuf message: {self.__class__} from {wrapper}')


@dataclasses.dataclass
class Message(abc.ABC):
    """Abstract type that facilitates communication with protocol buffers.

    Message instances should be compact and cheap by making use of proxies,
    slots and copying data rather than assigning it.
    """

    name: ClassVar[str]
    type_url: ClassVar[str]

    def __new__(cls, name, bases, ns):
        """Custom allocation for runtime-generated class types."""
        print('Creating new Messsage instance')
        return super().__new__(cls, name, bases, ns)

    def __init__(self, name, bases, ns):
        pass

    def _make_proxy(self, name: str, wrapped, wrapper):
        """Returns the name of the public property attribute."""
        vars(self).setdefault(name, FieldProxy(wrapped, wrapper))

    def _add_slots(self, message_descriptor, fields):
        """Adds a __slots__ entry to class."""
        pass

    def __subclasshook__(self):
        message_class = self.meta.get('message_class')
        setattr(self, 'prototype', GoogleAdsClient.get_type(message_class))
        return hasattr(self, 'prototype')


T = TypeVar('T')


def message(cls: Type[T]) -> Type[T]:

    typed_attrs = get_typed_attributes(cls)

    try:
        # Used to list all fields and locate fields by field number.
        cls.__resource_fields__: Dict[int, dataclasses.Field] = dict(
            create_field(idx, fld.name, typed_attrs[fld.name])
            for idx, fld in enumerate(dataclasses.fields(cls))
        )
    except KeyError as e:
        print(e)
        raise

    Message.register(cls)
    pb = ProtobufWrapper(cls.prototype, cls)
    cls.__doc__ = 'Docstring comes from meta data...'
    cls.__module__ = f'adspert.googleads.resources.{cls.name}'
    cls.fields = make_resource_fieldset(cls.name, pb.DESCRIPTOR)
    cls.pb = pb

    pb.id.value = 1
    pb.mro

    print(help(cls))

    return cls


@message
class Campaign(Resource):
    _meta: ClassVar[MappingProxyType]
    name: ClassVar[str] = 'campaign'

    fields = attrdict(
        **Resource.fields,
        bidding_strategy_type='bidding_strategy_type',
        channel_sub_type='advertising_channel_sub_type',
        channel_type='advertising_channel_type',
        labels='labels',
    )

    @classproperty
    def bidding_strategy_type(self):
        """
        """
        return self.field_path(self.fields.bidding_strategy_type)

    @classproperty
    def channel_sub_type(self):
        """
        """
        return self.field_path(self.fields.channel_sub_type)

    @classproperty
    def channel_type(self):
        """
        """
        return self.field_path(self.fields.channel_type)

    @classproperty
    def labels(self):
        """
        """
        return self.field_path(self.fields.labels)

    @classmethod
    def sync_structure(cls) -> set:
        return set.union(
            super().sync_structure(),
            {
                cls.channel_type,
                cls.channel_sub_type,
                cls.bidding_strategy_type,
            },
        )


@message
class AdGroup(Resource):

    name: str = 'ad_group'

    fields = attrdict(
        **Resource.fields,
        cpc_bid_micros='cpc_bid_micros',
        labels='labels',
    )

    @classproperty
    def max_cpc(self):
        """
        """
        return self._field_string(self.fields.cpc_bid_micros)

    @classproperty
    def labels(self):
        """
        """
        return self._field_string(self.fields.labels)

    @classmethod
    def sync_structure(cls) -> set:
        return set.union(
            super().sync_structure(), {cls.max_cpc})

    def __str__(self):
        return 'ad_group'


@message
class AdGroupCriterion(Resource):
    name: str = 'ad_group_criterion'

    fields = attrdict(
        **Resource.fields,
        cpc_bid_micros='cpc_bid_micros',
    )

    @classproperty
    def max_cpc(self):
        """
        """
        return self._field_string(self.fields.cpc_bid_micros)


listing_group_fields = attrdict(
    type='type',
    parent_ad_group_criterion='parent_ad_group_criterion',
    listing_brand='case_value.listing_brand.value',
    listing_custom_attribute=attrdict(
        index='case_value.listing_custom_attribute.index',
        value='case_value.listing_custom_attribute.value',
    ),
    product_bidding_category=attrdict(
        id='case_value.product_bidding_category.id',
        country_code='case_value.product_bidding_category.country_code',
        level='case_value.product_bidding_category.level',
    ),
    product_channel='case_value.product_channel.channel',
    product_condition='case_value.product_condition.condition',
    product_item_id='case_value.product_item_id.value',
    product_type=attrdict(
        level='case_value.product_type.level',
        value='case_value.product_type.value',
    ),
)

listing_group = Message('listing_group', (Resource,), dict(
    __str__=lambda self: 'ad_group_criterion.listing_group',
    name='ad_group_criterion.listing_group',
    **listing_group_fields,
))


@message
class ConversionAction(Resource):

    name: str = 'conversion_action'

    fields = attrdict(
        **Resource.fields,
        attribution_model='attribution_model_settings.attribution_model',
        category='category',
        include_in_conversions='include_in_conversions_metric',
        click_through_lookback='click_through_lookback_window_days',
        view_through_lookback='view_through_lookback_window_days',
    )

    @classproperty
    def attribution(self):
        """
        """
        return self.field_path(self.fields.attribution_model)

    @classproperty
    def category(self):
        """
        """
        return self.field_path(self.fields.category)

    @classproperty
    def include_in_conversions(self):
        """
        """
        return self.field_path(self.fields.include_in_conversions)

    @classproperty
    def click_through_lookback(self):
        """
        """
        return self.field_path(self.fields.click_through_lookback)

    @classproperty
    def view_through_lookback(self):
        """
        """
        return self.field_path(self.fields.view_through_lookback)


if __name__ == '__main__':
    fire.Fire()
