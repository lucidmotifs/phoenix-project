"""Enums with wrapper classes to allow aliases and custom values."""
import enum

from google.ads.google_ads.client import GoogleAdsClient

AccountBudgetStatusEnum = GoogleAdsClient.get_type('AccountBudgetStatusEnum')
AdGroupCriterionApprovalStatusEnum = GoogleAdsClient.get_type('AdGroupCriterionApprovalStatusEnum')
AdGroupStatusEnum = GoogleAdsClient.get_type('AdGroupStatusEnum')
AdGroupTypeEnum = GoogleAdsClient.get_type('AdGroupTypeEnum')
AdTypeEnum = GoogleAdsClient.get_type('AdTypeEnum')
CampaignStatusEnum = GoogleAdsClient.get_type('CampaignStatusEnum')
CampaignTypeEnum = GoogleAdsClient.get_type('AdvertisingChannelTypeEnum')
CriterionSystemServingStatusEnum = GoogleAdsClient.get_type('CriterionSystemServingStatusEnum')
DeviceEnum = GoogleAdsClient.get_type('DeviceEnum')


class AdGroupCriterionStatusEnum(object):
    class AdGroupCriterionStatus(enum.IntEnum):
        """
        The possible statuses of an AdGroupCriterion.
        Attributes:
          UNSPECIFIED (int): No value has been specified.
          UNKNOWN (int): The received value is not known in this version.
          This is a response-only value.
          ENABLED (int): The ad group criterion is enabled.
          PAUSED (int): The ad group criterion is paused.
          REMOVED (int): The ad group criterion is removed.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        ENABLED = 2
        PAUSED = 3
        REMOVED = 4


class CriterionTypeEnum(object):
    class CriterionType(enum.IntEnum):
        """
        Enum describing possible criterion types.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          KEYWORD (int): Keyword. e.g. 'mars cruise'.
          PLACEMENT (int): Placement, aka Website. e.g. 'www.flowers4sale.com'
          MOBILE_APP_CATEGORY (int): Mobile application categories to target.
          MOBILE_APPLICATION (int): Mobile applications to target.
          DEVICE (int): Devices to target.
          LOCATION (int): Locations to target.
          LISTING_GROUP (int): Listing groups to target.
          AD_SCHEDULE (int): Ad Schedule.
          AGE_RANGE (int): Age range.
          GENDER (int): Gender.
          INCOME_RANGE (int): Income Range.
          PARENTAL_STATUS (int): Parental status.
          YOUTUBE_VIDEO (int): YouTube Video.
          YOUTUBE_CHANNEL (int): YouTube Channel.
          USER_LIST (int): User list.
          PROXIMITY (int): Proximity.
          TOPIC (int): A topic target on the display network
          (e.g. "Pets & Animals").
          LISTING_SCOPE (int): Listing scope to target.
          LANGUAGE (int): Language.
          IP_BLOCK (int): IpBlock.
          CONTENT_LABEL (int): Content Label for category exclusion.
          CARRIER (int): Carrier.
          USER_INTEREST (int): A category the user is interested in.
          WEBPAGE (int): Webpage criterion for dynamic search ads.
          OPERATING_SYSTEM_VERSION (int): Operating system version.
          APP_PAYMENT_MODEL (int): App payment model.
          MOBILE_DEVICE (int): Mobile device.
          CUSTOM_AFFINITY (int): Custom affinity.
          CUSTOM_INTENT (int): Custom intent.
          LOCATION_GROUP (int): Location group.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        KEYWORD = 2
        PLACEMENT = 3
        MOBILE_APP_CATEGORY = 4
        MOBILE_APPLICATION = 5
        DEVICE = 6
        LOCATION = 7
        LISTING_GROUP = 8
        AD_SCHEDULE = 9
        AGE_RANGE = 10
        GENDER = 11
        INCOME_RANGE = 12
        PARENTAL_STATUS = 13
        YOUTUBE_VIDEO = 14
        YOUTUBE_CHANNEL = 15
        USER_LIST = 16
        PROXIMITY = 17
        TOPIC = 18
        LISTING_SCOPE = 19
        LANGUAGE = 20
        IP_BLOCK = 21
        CONTENT_LABEL = 22
        CARRIER = 23
        USER_INTEREST = 24
        WEBPAGE = 25
        OPERATING_SYSTEM_VERSION = 26
        APP_PAYMENT_MODEL = 27
        MOBILE_DEVICE = 28
        CUSTOM_AFFINITY = 29
        CUSTOM_INTENT = 30
        LOCATION_GROUP = 31


class BiddingSourceEnum(object):
    class BiddingSource(enum.IntEnum):
        """
        Indicates where a bid or target is defined. For example, an ad group
        criterion may define a cpc bid directly, or it can inherit its cpc
        bid from the ad group.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          CAMPAIGN_BIDDING_STRATEGY (int): Effective bid or target is inherited
          from campaign bidding strategy.
          AD_GROUP (int): The bid or target is defined on the ad group.
          AD_GROUP_CRITERION (int): The bid or target is defined on the ad
          group criterion.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        CAMPAIGN_BIDDING_STRATEGY = 5
        AD_GROUP = 6
        AD_GROUP_CRITERION = 7


class BidModifierSourceEnum(object):
    class BidModifierSource(enum.IntEnum):
        """
        Enum describing possible bid modifier sources.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          CAMPAIGN (int): The bid modifier is specified at the campaign level,
          on the campaign level criterion.
          AD_GROUP (int): The bid modifier is specified (overridden) at the ad
          group level.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        CAMPAIGN = 2
        AD_GROUP = 3


class BiddingStrategyErrorEnum(object):
    class BiddingStrategyError(enum.IntEnum):
        """
        Enum describing possible bidding strategy errors.
        Attributes:
          UNSPECIFIED (int): Enum unspecified.
          UNKNOWN (int): The received error code is not known in this version.
          DUPLICATE_NAME (int): Each bidding strategy must have a unique name.
          CANNOT_CHANGE_BIDDING_STRATEGY_TYPE (int): Bidding strategy type is
          immutable.
          CANNOT_REMOVE_ASSOCIATED_STRATEGY (int): Only bidding strategies not
          linked to campaigns, adgroups or adgroup
          criteria can be removed.
          BIDDING_STRATEGY_NOT_SUPPORTED (int): The specified bidding strategy
          is not supported.
          INCOMPATIBLE_BIDDING_STRATEGY_AND_BIDDING_STRATEGY_GOAL_TYPE (int):
          The bidding strategy is incompatible with the campaign's bidding
          strategy goal type.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        DUPLICATE_NAME = 2
        CANNOT_CHANGE_BIDDING_STRATEGY_TYPE = 3
        CANNOT_REMOVE_ASSOCIATED_STRATEGY = 4
        BIDDING_STRATEGY_NOT_SUPPORTED = 5
        INCOMPATIBLE_BIDDING_STRATEGY_AND_BIDDING_STRATEGY_GOAL_TYPE = 6


class BiddingStrategyStatusEnum(object):
    class BiddingStrategyStatus(enum.IntEnum):
        """
        The possible statuses of a BiddingStrategy.
        Attributes:
          UNSPECIFIED (int): No value has been specified.
          UNKNOWN (int): The received value is not known in this version.
          This is a response-only value.
          ENABLED (int): The bidding strategy is enabled.
          REMOVED (int): The bidding strategy is removed.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        ENABLED = 2
        REMOVED = 4


class BiddingStrategyTypeEnum(object):
    class BiddingStrategyType(enum.IntEnum):
        """
        Enum describing possible bidding strategy types.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          COMMISSION (int): Commission is an automatic bidding strategy in
          which the advertiser pays
          a certain portion of the conversion value.
          ENHANCED_CPC (int): Enhanced CPC is a bidding strategy that raises
          bids for clicks
          that seem more likely to lead to a conversion and lowers
          them for clicks where they seem less likely.
          MANUAL_CPC (int): Manual click based bidding where user pays per
          click.
          MANUAL_CPM (int): Manual impression based bidding
          where user pays per thousand impressions.
          MANUAL_CPV (int): A bidding strategy that pays a configurable amount
          per video view.
          MAXIMIZE_CONVERSIONS (int): A bidding strategy that automatically
          maximizes number of conversions
          given a daily budget.
          MAXIMIZE_CONVERSION_VALUE (int): An automated bidding strategy that
          automatically sets bids to maximize
          revenue while spending your budget.
          PAGE_ONE_PROMOTED (int): Page-One Promoted bidding scheme, which
          sets max cpc bids to
          target impressions on page one or page one promoted slots on
          google.com.
          This enum value is deprecated.
          PERCENT_CPC (int): Percent Cpc is bidding strategy where bids are a
          fraction of the
          advertised price for some good or service.
          TARGET_CPA (int): Target CPA is an automated bid strategy that sets
          bids
          to help get as many conversions as possible
          at the target cost-per-acquisition (CPA) you set.
          TARGET_CPM (int): Target CPM is an automated bid strategy that sets
          bids to help get
          as many impressions as possible at the target cost per one thousand
          impressions (CPM) you set.
          TARGET_IMPRESSION_SHARE (int): An automated bidding strategy that
          sets bids so that a certain percentage
          of search ads are shown at the top of the first page (or other
          targeted
          location).
          TARGET_OUTRANK_SHARE (int): Target Outrank Share is an automated
          bidding strategy that sets bids
          based on the target fraction of auctions where the advertiser
          should outrank a specific competitor.
          This enum value is deprecated.
          TARGET_ROAS (int): Target ROAS is an automated bidding strategy
          that helps you maximize revenue while averaging
          a specific target Return On Average Spend (ROAS).
          TARGET_SPEND (int): Target Spend is an automated bid strategy that
          sets your bids
          to help get as many clicks as possible within your budget.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        COMMISSION = 16
        ENHANCED_CPC = 2
        MANUAL_CPC = 3
        MANUAL_CPM = 4
        MANUAL_CPV = 13
        MAXIMIZE_CONVERSIONS = 10
        MAXIMIZE_CONVERSION_VALUE = 11
        PAGE_ONE_PROMOTED = 5
        PERCENT_CPC = 12
        TARGET_CPA = 6
        TARGET_CPM = 14
        TARGET_IMPRESSION_SHARE = 15
        TARGET_OUTRANK_SHARE = 7
        TARGET_ROAS = 8
        TARGET_SPEND = 9


class PaymentModeEnum(object):
    class PaymentMode(enum.IntEnum):
        """
        Enum describing possible payment modes.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          CLICKS (int): Pay per click.
          CONVERSION_VALUE (int): Pay per conversion value. This mode is only
          supported by campaigns with
          AdvertisingChannelType.HOTEL, BiddingStrategyType.COMMISSION, and
          BudgetType.HOTEL_ADS_COMMISSION.
          CONVERSIONS (int): Pay per conversion. This mode is only supported
          by campaigns with
          AdvertisingChannelType.DISPLAY (excluding
          AdvertisingChannelSubType.DISPLAY_GMAIL),
          BiddingStrategyType.TARGET_CPA, and BudgetType.FIXED_CPA. The
          customer
          must also be eligible for this mode. See
          Customer.eligibility_failure_reasons for details.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        CLICKS = 4
        CONVERSION_VALUE = 5
        CONVERSIONS = 6


class TargetingDimensionEnum(object):
    class TargetingDimension(enum.IntEnum):
        """
        Enum describing possible targeting dimensions.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          KEYWORD (int): Keyword criteria, e.g. 'mars cruise'. KEYWORD may be
          used as a custom bid
          dimension. Keywords are always a targeting dimension, so may not be
          set
          as a target "ALL" dimension with TargetRestriction.
          AUDIENCE (int): Audience criteria, which include user list, user
          interest, custom
          affinity,  and custom in market.
          TOPIC (int): Topic criteria for targeting categories of content, e.g.
          'category::Animals>Pets' Used for Display and Video targeting.
          GENDER (int): Criteria for targeting gender.
          AGE_RANGE (int): Criteria for targeting age ranges.
          PLACEMENT (int): Placement criteria, which include websites like
          'www.flowers4sale.com',
          as well as mobile applications, mobile app categories, YouTube
          videos,
          and YouTube channels.
          PARENTAL_STATUS (int): Criteria for parental status targeting.
          INCOME_RANGE (int): Criteria for income range targeting.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        KEYWORD = 2
        AUDIENCE = 3
        TOPIC = 4
        GENDER = 5
        AGE_RANGE = 6
        PLACEMENT = 7
        PARENTAL_STATUS = 8
        INCOME_RANGE = 9


class ManagerLinkStatusEnum(object):
    class ManagerLinkStatus(enum.IntEnum):
        """
        Possible statuses of a link.
        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Used for return value only. Represents value unknown
          in this version.
          ACTIVE (int): Indicates current in-effect relationship
          INACTIVE (int): Indicates terminated relationship
          PENDING (int): Indicates relationship has been requested by manager,
          but the client
          hasn't accepted yet.
          REFUSED (int): Relationship was requested by the manager, but the
          client has refused.
          CANCELED (int): Indicates relationship has been requested by manager,
          but manager
          canceled it.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        ACTIVE = 2
        INACTIVE = 3
        PENDING = 4
        REFUSED = 5
        CANCELED = 6


class ProductGroupInfoTypeEnum(object):
    class ProductGroupInfoType(enum.IntEnum):
        pass


class ProductBiddingCategoryLevelEnum(object):
    class ProductBiddingCategoryLevel(enum.IntEnum):
        """
        Enum describing the level of the product bidding category.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          LEVEL1 (int): Level 1.
          LEVEL2 (int): Level 2.
          LEVEL3 (int): Level 3.
          LEVEL4 (int): Level 4.
          LEVEL5 (int): Level 5.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        LEVEL1 = 2
        LEVEL2 = 3
        LEVEL3 = 4
        LEVEL4 = 5
        LEVEL5 = 6

    platform = ProductBiddingCategoryLevel


class ProductChannelEnum(object):
    class ProductChannel(enum.IntEnum):
        """
        Enum describing the locality of a product offer.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          ONLINE (int): The item is sold online.
          LOCAL (int): The item is sold in local stores.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        ONLINE = 2
        LOCAL = 3


class ProductChannelExclusivityEnum(object):
    class ProductChannelExclusivity(enum.IntEnum):
        """
        Enum describing the availability of a product offer.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          SINGLE_CHANNEL (int): The item is sold through one channel
            only, either local stores or online
          as indicated by its ProductChannel.
          MULTI_CHANNEL (int): The item is matched to its online or
            local stores counterpart, indicating
          it is available for purchase in both ShoppingProductChannels.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        SINGLE_CHANNEL = 2
        MULTI_CHANNEL = 3


class ProductConditionEnum(object):
    class ProductCondition(enum.IntEnum):
        """
        Enum describing the condition of a product offer.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          NEW (int): The product condition is new.
          REFURBISHED (int): The product condition is refurbished.
          USED (int): The product condition is used.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        NEW = 3
        REFURBISHED = 4
        USED = 5


class ProductTypeLevelEnum(object):
    class ProductTypeLevel(enum.IntEnum):
        """
        Enum describing the level of the type of a product offer.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          LEVEL1 (int): Level 1.
          LEVEL2 (int): Level 2.
          LEVEL3 (int): Level 3.
          LEVEL4 (int): Level 4.
          LEVEL5 (int): Level 5.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        LEVEL1 = 7
        LEVEL2 = 8
        LEVEL3 = 9
        LEVEL4 = 10
        LEVEL5 = 11


class ListingCustomAttributeIndexEnum(object):
    class ListingCustomAttributeIndex(enum.IntEnum):
        """
        The index of the listing custom attribute.

        Attributes:
          UNSPECIFIED (int): Not specified.
          UNKNOWN (int): Represents value unknown in this version.
          INDEX0 (int): First listing custom attribute.
          INDEX1 (int): Second listing custom attribute.
          INDEX2 (int): Third listing custom attribute.
          INDEX3 (int): Fourth listing custom attribute.
          INDEX4 (int): Fifth listing custom attribute.
        """
        UNSPECIFIED = 0
        UNKNOWN = 1
        INDEX0 = 7
        INDEX1 = 8
        INDEX2 = 9
        INDEX3 = 10
        INDEX4 = 11

    platform = ListingCustomAttributeIndex
