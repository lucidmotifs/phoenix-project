import itertools
from pprint import pprint
from operator import attrgetter

from anytree import NodeMixin, RenderTree, AnyNode
from anytree.cachedsearch import find as cachedfind
from anytree.render import AsciiStyle
from playhouse.shortcuts import model_to_dict

from adspert.database.models.account import AdwProductPartition as ProductPartition
from adspert.adwords.shopping import get_adgroup_partitions


class PartitionTree(AnyNode):

    adgroup_id: int = 0
    temp_id: int = itertools.count(-1, -1)

    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    def __init__(self, model):
        self.id = model.criterion_id
        self.case_value = (
            model.dimension_type,
            model.dimension_value,
        )
        self.model = model_to_dict(model)

    def __repr__(self):
        return (f'Partition(id={self.id}, '
                f'case_value={self.case_value})')

    def _post_attach(self, node):
        super()._post_attach(node)
        self.adgroup_id = self.parent.model['adgroup_id']

    def subtree(self, data):
        while model := next(data, None):
            pid, nodes = model
            parent = cachedfind(self, lambda n: n.id == pid)
            if parent:
                parent.children = [type(self)(n) for n in nodes]

    def render(self, nid):
        out = RenderTree(self, style=AsciiStyle())
        print(out)


def create_partition_tree(adgroup_id: int):

    criterion = get_adgroup_partitions(adgroup_id)
    criterion = criterion.order_by(
        ProductPartition.parent_id,
        ProductPartition.criterion_id,
    )

    grouped = itertools.groupby(criterion, key=attrgetter('parent_id'))
    partitions = {k: list(g) for k, g in grouped}
    nodes = iter(partitions.values())

    tree = PartitionTree(next(nodes)[0])
    tree.subtree(iter(partitions.items()))
    tree.render(0)

    return tree

