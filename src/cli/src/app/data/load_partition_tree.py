import collections
import itertools
import json
import queue
import uuid
from typing import Dict, Iterator

import devtools
from typedtree.tree import TraversalMode
from typedtree.tree import Tree
from typedtree.node import Node, Edge
from numpy.random.mtrand import rand

from py2neo import Graph, Node, Relationship


def iter_ancestors(tree: Tree, node: Node, strict: bool = False) -> Iterator:
    """Generate a non-inclusive sequence of ancestors nodes."""
    node_type = node.type
    while p := node.parent:
        node = tree[p.pointer]
        if node.type == node_type or not strict:
            yield node


def get_dimension(dtype):
    if dtype.startswith('BIDDING'):
        return 'Bidding Category'
    elif dtype.startswith('PRODUCT'):
        return 'Product Type'
    elif dtype.startswith('CUSTOM'):
        return 'Custom Attribute'
    elif dtype == 'BRAND':
        return 'Brand'


def gen_nodes(tree: Tree, component: Dict):

    items = iter(component.items())
    pid, children = next(items)
    for cid, dimensions in children.items():

        parent_node = tree[pid]
        parent_scope = parent_node.payload

        tree.add_node(cid,
            parent_pointer=pid,
            node_type='productPartition',
            edge_type='subdivision',
            payload=dimensions)

        node = tree[cid]
        dims = set(dimensions).difference(parent_scope)

        for dt in dims:
            dv = dimensions[dt]
            identifier = hash((dt, dv))
            if dv not in tree.nodes:
                tree.add_node(
                    identifier,
                    parent_pointer=get_dimension(dt),
                    node_type=dt,
                    edge_type=dv,
                    payload={'type': dt, 'value': dv, 'id': identifier})

            node.add_child(Edge(identifier, 'caseValue'))

        yield node


def load_data(fp):

    tree = Tree()
    data = json.load(fp)
    nodeq = queue.SimpleQueue()

    tree.add_node(71670483307, payload={}, node_type='adgroup')
    agnode = tree[71670483307]

    tree.add_node('Product Dimensions', node_type='concept')

    tree.add_node('Brand', parent_pointer='Product Dimensions', node_type='dimension')
    tree.add_node('Custom Attribute', parent_pointer='Product Dimensions', node_type='dimension')
    tree.add_node('Bidding Category', parent_pointer='Product Dimensions', node_type='dimension')
    tree.add_node('Product Type', parent_pointer='Product Dimensions', node_type='dimension')

    tree.add_node('root', parent_pointer = agnode.identifier, node_type='root',
        payload={'depth':0, 'dimensions': None})

    dimensions = tree['Product Dimensions']

    idata = iter(data)
    while component := next(idata, None):

        pid = next(iter(component.keys()))
        parent = tree[pid]

        nodes = list(gen_nodes(tree, component))

    node = tree['428338236638']
    tree.display('428338236638', depth=5)  # devtools.debug(tree['dimensions'].children)

    # for id in tree.traverse(node.identifier, mode=TraversalMode.DE    #    node = tree[id]
    #    print(f"{node.identifier} [{node.type or '*Undefined*'}]")
    i = hash(('CUSTOM_ATTRIBUTE_3', 'trainingsbekleidung'))
    devtools.debug(vars(tree[i]))

    custom = tree['Custom Attribute']
    devtools.debug((custom.children))
    node = tree['899882006596']

    print(node.payload)

    # @TODO select child nodes using edge-type
    product_scope = [edge.pointer for edge in node.children
                     if edge.type in node.payload]
    for n in iter_ancestors(tree, node, strict=True):
        product_scope.extend([edge.pointer for edge in n.children
                              if edge.type in node.payload])
    for i in reversed(product_scope):
        devtools.debug(vars(tree[i]))


import os

file = os.path.defpath("shopping_nodes.json")

with open(file, 'r') as fp:
    g = Graph(password='dev')
    g.merge(load_data(fp))
