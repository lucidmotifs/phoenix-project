from graphql import build_ast_schema, parse
from gql import gql, Client
from gql.transport.websockets import WebsocketsTransport

with open('data/schema.graphql') as source:
    document = parse(source.read())
    schema = build_ast_schema(document)
    sample_transport = WebsocketsTransport(url='wss://localhost')

    client = Client(
        transport=sample_transport,
        fetch_schema_from_transport=True,
    )

    query = "match (n) return n limit 25"
    for result in client.execute(query):
        print (f"result = {result!s}")
