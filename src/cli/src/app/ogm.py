""" Object-Graph Mapping """

from py2neo.ogm import GraphObject, Property, RelatedFrom, RelatedTo, Related


class Campaign(GraphObject):
    __primarykey__ = "campaignId"
    name = Property()
    status = Property()
    campaignId = Property()

    adgroups = RelatedTo("AdGroup", "CONTAINS")
    criteria = RelatedTo("Criterion", "TARGETS")
    budget = RelatedTo("Budget", "SPENDS")


class AdGroup(GraphObject):
    __primarykey__ = "adGroupId"

    name = Property()
    adgroupId = Property()

    campaign = RelatedTo('Campaign', 'PART_OF')
    criteria = Related("Criterion", 'TARGETS')


class WorksWith(Relationship): pass


class Criterion(GraphObject):
    __primarykey__ = "criterionId"

    criterionId = Property()
    negative = Property()


class ListingDimension(GroupObject):
    products = RelatedFrom('Product')
    criterion = RelatedFrom(ListingGroup)


class ListingGroup(Criterion):
    __primarykey__ = "criterionId"

    case_value: RelatedTo(ListingDimension, 'REFINES')
    parent: Related('ListingGroup', 'PARTITION')


class ProductBrand(ListingDimension):
    __primarykey__ = 'value'
    value = Property()


class ProductCategory(ListingDimension):
    __primarykey__ = 'criterionId'
    criterionId = Property()
    level = Property()
    name = Property()


class ProductType(ListingDimension):
    __primarykey__ = 'value'
    level = Proeprty()
    value = Property()


class ProductCustomAttribute(ListingDimension):
    __primarykey__ = 'value'
    index = Property()
    value = Property()


class Product(GraphObject):
    __primarykey__ = 'itemId'
    item_id = Property('itemId')

    brand = RelatedTo(ProductBrand, 'BRAND')
    category = RelatedTo(ProductCategory, 'CATEGORY')
    product_type = RelatedTo(ProductType, 'PRODUCT_TYPE')
    custom_attribute = RelatedTo(ProductCustomAttribute, 'CUSTOM_ATTRIBUTE')

