# adspert cli app
import aenum
import asyncio
import asyncssh
import dataclasses
import enum
import functools
import logging
import prompt_toolkit as prompt
import subprocess
import sys
import textwrap
from typing import Any
from typing import Dict
from typing import List

import fire
import tabulate
import pandas as pd
from devtools import debug
from pssh.utils import load_private_key
from pssh.clients import ParallelSSHClient

from adspert.base.app import adspert_app
from adspert.database.db import configure_db, dbs, close_dbs
from adspert.scripts.utils import get_account
from adspert.database.models import main
from adspert.database.models import account
from adspert.base.utils import masked

"""
Use the following magic function in ipython to reload modules.
%load_ext autoreload
%autoreload 2
"""

class AccountDbHost(aenum.Enum):

    ADB01 = aenum.auto(), 'adb-01.dc1.adspert.de', 9001
    ADB02 = aenum.auto(), 'adb-02.dc1.adspert.de', 9002
    ADB03 = aenum.auto(), 'adb-03.dc1.adspert.de', 9003
    ADB04 = aenum.auto(), 'adb-04.dc1.adspert.de', 9004
    ADB05 = aenum.auto(), 'adb-05.dc1.adspert.de', 9005
    ADB06 = aenum.auto(), 'adb-06.dc1.adspert.de', 9006
    ADB07 = aenum.auto(), 'adb-07.dc1.adspert.de', 9007
    ADB08 = aenum.auto(), 'adb-08.dc1.adspert.de', 9008
    ADB09 = aenum.auto(), 'adb-09.dc1.adspert.de', 9009
    ADB10 = aenum.auto(), 'adb-10.dc1.adspert.de', 9010
    ADB11 = aenum.auto(), 'adb-11.dc1.adspert.de', 9011


class ip(str, aenum.Enum):

     ACCOUNT =      '192.168.1.112'
     MAIN =         '192.168.1.107'
     LOCAL =        '127.0.0.1'


class db:

    @classmethod
    def connect_main(cls):
        print('Configuring MainDB')
        configure_db()

class app:

    @classmethod
    def init(cls):
        print('Initializing AdspertApp')
        adspert_app.init('scripts', 'production')


def render(data, paging: bool = True):

    def datatable(data):
      table = tabulate.tabulate(data, headers='keys')
      return table

    def pager(out):
      pager = subprocess.Popen(
          ['less', '-F', '-R', '-S', '-X', '-K'],
          stdin=subprocess.PIPE,
          stdout=sys.stdout)
      pager.stdin.write(out.encode())
      pager.stdin.close

    output = datatable(data)
    if paging:
        return pager(output)
    print(output)


def adspert_init():
    print('Initializing AdspertApp')
    adspert_app.init('scripts', 'production')
    print('Configuring MainDB')
    configure_db()


def account_setup(account: main.Account):
    print('Configuring account DB')
    dbs.account.database = account.db_name
    dbs.account.setup(account)
    dbs.account.connect_params.update({
        'port': 5432,
    })
    debug(dbs.account.connect_params)
    # dbs.account.connect()
    return dbs


def display_account_structure():
    testdata: List[Dict[str, Any]]
    testdata = account.AccountDbModel.fetchall(
        """
        SELECT
            campaign_id,
            campaign_name,
            aw_campaign_type,
            campaign.status,
            adgroup_id,
            adgroup_name
        FROM campaign
            join adgroup using (campaign_id)
        WHERE campaign.status = 'Active'
          AND aw_campaign_type = 'SHOPPING'
        ORDER BY campaign_id
    """)
    render(testdata, paging=False)


def create_ssh_tunnel(db_host: AccountDbHost):
    host = AccountDbHost[db_host]

    client = pssh.clients.ParallelSSHClient(
      ip.ACCOUNT,
      user='paul',
      proxy_host='gateway',
      proxy_user='paul',
      proxy_port=18114,
      proxy_pkey=load_private_key('id_rsa'))
    client.run_command('echo 1', use_pty=False)
    return client


def bootstrap(adspert_id=3281857):

    try:
        adspert_init()
        print(f'Fetching account record for {adspert_id}')
        acc = get_account(adspert_id)
        conn = account_setup(acc)
    except Exception as e:
        debug(e)
    else:
        print('Done.')
        return acc
    # sync_product_partition_structure(acc)
    # debug(task)
    # stree = ProductPartitionTree()
    # stree = build_product_group_structure()
    # debug(stree.render())


if __name__ == '__main__':
    bootstrap()


#################
# ASYNC ATTEMPT #
#################


async def asyncmain():
    loop = asyncio.get_running_loop()
    conn = await asyncio.wait_for(create_connection(), 10)

    #account = adspert_init(3209144)
    #print(f'Received account details. {account}')
    print('Do async things now...')

    # account_db_connect(account)
    # print(smoke_test())
    await asyncio.sleep(300)
    print('closing connection')

    conn.close()
    conn.wait_closed()


async def create_connection():
    conn = await asyncssh.connect(
        'sesam.dc1.adspert.de',
        port=18114,
        username='paul',
        client_keys=asyncssh.read_private_key(
            '/home/paulcooper/.ssh/id_rsa', passphrase='W4mbera!2'),
    )
    return conn


async def run_async():
    try:
        asyncio.run(asyncmain(), debug=True)
    except (OSError, asyncssh.Error) as exc:
        sys.exit('SSH connection failed: ' + str(exc))
