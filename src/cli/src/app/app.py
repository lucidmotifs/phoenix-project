import logging
import inspect
import importlib
import os
import pprint
import sys
import types
from importlib.abc import PathEntryFinder

import adspert
import fire

logging.basicConfig(filename='log/app.log')


ADSPTH = os.environ['ADSPERT_PATH'] or os.path.expanduser("~/src/adspert")


class AdspertFinder(PathEntryFinder):
    """[summary]

    Args:
        PathEntryFinder ([type]): [description]

    Returns:
        [type]: [description]
    """    """Finds adspert module specs."""
    _adspert_pkg: types.ModuleType

    def __init__(self, pkgpath, findtests: bool = False):
        self._adspert_pkg = self.find_module(pkgpath)

        print('Adspert Package:', self._adspert_pkg.loader)
        self._tests = findtests

    def load_package(self, fqname, pathname):
         self.msgin(2, "load_package", fqname, pathname)
         newname = replacePackageMap.get(fqname)
         if newname:
             fqname = newname
         m = self.add_module(fqname)
         m.__file__ = pathname
         m.__path__ = [pathname]

         # As per comment at top of file, simulate runtime __path__ additions.
         m.__path__ = m.__path__ + packagePathMap.get(fqname, [])

         fp, buf, stuff = self.find_module("__init__", m.__path__)
         try:
             self.load_module(fqname, fp, buf, stuff)
             self.msgout(2, "load_package ->", m)
             return m
         finally:
             if fp:
                 fp.close()

    def find_spec(self, name, target=adspert):
        logging.debug(f'finding module {name} from {target}')
        if name in sys.modules:
            print(f"{name!r} already in sys.modules")
            return

        if (spec := importlib.util.find_spec(name)) is not None:
            module = importlib.util.module_from_spec(spec)
            sys.modules[module_name] = module
            spec.loader.exec_module(module)
            print(f"{name!r} has been imported")
            logging.debug(sys.modules[name])


sys.meta_path.append(AdspertFinder('adspert'))


def main(ns={}):
    """Main entrypoint.

    [summary]
    """
    print(f'Current namespace {sys.modules}')

    module = None
    module_name = '.'.join(ns['args'][1:])
    if (spec := importlib.util.find_spec(module_name)) is not None:
        # perform the actual import ...
        module = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module)

    print(module_name)
    logging.debug(sys.modules[module_name])

    if not module:
        print(f'{module_name} cannot be found in the adspert package')
        sys.exit(1)

    print(dir(module))

    pred = lambda i: inspect.isfunction(i) or inspect.isclass(i)
    members = inspect.getmembers(module, predicate=pred)
    names = (sorted(members, key=lambda m: m[0]))
    pprint.pprint(f'Public members: {names}')

    fire.Fire(module)
    ns.update({'result': 'OK'})


if __name__ == '__main__':
    main(dict(args=sys.argv))


