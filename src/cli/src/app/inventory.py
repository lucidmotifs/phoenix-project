# Shopping inventory module
import itertools

import pandas as pd
from anytree.cachedsearch import find as cachedfind

import adspert.adwords as adw
from phoenix.models import (
    Product, AdGroup, Campaign, AdwProductPartition,
    AdwProductGroup, AdwShoppingCampaign, ProductOffer,
    ProductAdGroupView)
from google.ads.google_ads.v3.types import ProductDimension
from google.ads.google_ads.v3.types import ListingScopeInfo


# import shopping.category
# import shopping.dimension
# import shopping.inventory
# import shopping.tree.PartitionTree
# import shopping.sync.product_partition_structure
# import shopping.sync.product_dimension_relationship


class ProductInventory(object):

    def __init__(self, campaign_id, dimensions=(), limit=20000):
        self.campaign = campaign_id
        self.dimensions = dimensions
        self.limit = limit

    def _query(self, use_history=False, use_offers=True):
        if use_history or use_offers:
            model = ProductOffer if use_offers else ProductAdGroupView
        dimensions = set(self.dimensions) - {'ITEM_ID'}
        if model is None:
            c = AdwShoppingCampaign.select(AdwShoppingCampaign.product_scope_string)
            c = c.where(AdwShoppingCampaign.campaign==campaign)
            return self.filter(ProductSope(c.get().product_scope_string))
        else:
            q = (AdwProductPartition
                 .select(
                     AdwProductPartition.criterion_id,
                     AdwProductGroup.case_value)
                 .join(AdwProductGroup,
                        on=(AdwProductPartition.criterion_id ==
                            AdwProductGroup.criterion_id))
                 .where(AdwProductPartition.campaign == campaign))
            dimensions.update([r.case_value for r in q])

        adgroups = AdGroup.value_list(
            AdGroup.adgroup_id,
            where=(AdGroup.campaign == campaign))

        product = (model.select(model.item_id, model.adgroup_id).distinct()
                   .where(model.adgroup_id._in(adgroups))).cte('product')

        pdm = ProductDimension.alias('pdm')
        query = pdm.select(pdm).join(product, on(pdm.item_id == product.item_id))
        query = query.where(pdm.dimension_type << tuple(dimensions))

        return query

    def load(self):
        query = self._query()
        self._frame = pd.DataFrame(
            query.dicts(),
            columns=[
                'item_id',
                'dimension_type',
                'dimension_value',
            ]).drop_duplicates(inplace=True)

    def item_view(self):
        return (self._frame
                .pivot(
                    index='item_id',
                    columns='dimension_type',
                    values='dimension_value')
                .sort_values(by=self.dimensions, na_position='last')
                .fillna('*'))

    def dimension_view(self):
        df = self.item_view()
        return (df.reset_index()
                .set_index(self.dimensions)
                .sort_index())

    def display(self, fn, n: int):
        return fn().head(n)

    def filter(self, scope):
        dimensions = scope.dimensions
        model = ProductDimension
        q = model.select(model.item_id).distinct()
        q = q.where(model.dimension_type.is_null())
        for dt, dv in dimensions.item():
            q = q.orwhere(
                (model.dimension_type == dt) &
                (model.dimension_value == dv))
        return q

    def exclude(self, *casevalues):
        pass


def create_inventory(campaign):
    inventory = ProductInventory(campaign)
    fn = inventory.item_view
    inventory.display(fn, 100)


def filter_inventory(adgroup_id=None):
    pass
