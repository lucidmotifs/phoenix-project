BEGIN;

DROP VIEW IF EXISTS optimization.adw_product_group_v CASCADE;

CREATE OR REPLACE VIEW optimization.adw_product_group_v AS (
--  criteria in shopping campaigns are product groups
    SELECT crit_key,
        pp.criterion_id,
        pp.adgroup_id,
        pg.dimension_types[array_upper(dimension_types, 1)] as dimension_type,
        pg.dimension_values[
        array_upper(dimension_values, 1)] as dimension_value,
        pp.divisible
    FROM product_partition pp
        JOIN adw_product_group pg USING (criterion_id)
        JOIN criterion USING (criterion_id, adgroup_id)
    WHERE partition_type = 'Unit'
      AND criterion_status = 'Active'
      AND NOT negative
);


---------------------------------------------------------------


DROP VIEW IF EXISTS optimization.adw_valid_segmentation_v;

CREATE OR REPLACE VIEW optimization.adw_valid_segmentations_v AS (
   WITH lookup AS (
       SELECT
          DISTINCT dimension_types,
          valid_segmentations
       FROM adw_product_group
   ), segmentations AS (
        SELECT
          criterion_id,
          lookup.valid_segmentations
        FROM adw_product_group
        JOIN lookup using (dimension_types)
   )
   SELECT
     crit_key,
     pgv.criterion_id,
     unnest(valid_segmentations) as dimension_type
   FROM adw_product_group_v pgv
       JOIN criterion c using (crit_key)
       JOIN segmentations s on (pgv.criterion_id = s.criterion_id)
);


COMMIT;
