
-- testing
SELECT
    "depth",
    criterion_id,
    adgroup_id,
    dimension_types,
    partition_type,
    dimension_values[array_upper(dimension_values, 1)] as dimension_value
FROM adw_product_group pg 
  JOIN product_partition USING (criterion_id)
WHERE partition_type = 'Unit' 
ORDER BY "depth" DESC;
-- LIMIT 2000
-- OFFSET 2500
