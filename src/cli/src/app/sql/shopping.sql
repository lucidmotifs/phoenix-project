CREATE OR REPLACE
FUNCTION optimization.adw_existing_segmentations_fn(adgroup_ids BIGINT[])
RETURNS TABLE (
    crit_key BIGINT,
    subdivisions TEXT[],
    depth INTEGER
)
AS $$
BEGIN
    RETURN QUERY WITH RECURSIVE
        ancestors(criterion_id, adgroup_id, parent_id, partition_type,
        dimension_value, dimension_heirarchy, child_nodes) AS (
            SELECT
                pp.criterion_id AS criterion_id,
                pp.adgroup_id as adgroup_id,
                pp.parent_id AS parent_id,
                pp.partition_type AS partition_type,
                pp.dimension_value AS dimension_value,
                ARRAY[dimension_type::text] AS dimension_heirarchy,
                ARRAY[criterion_id] AS child_nodes
            FROM product_partition AS pp
            WHERE
                pp.adgroup_id = ANY (adgroup_ids) AND
                pp.parent_id = 0
            UNION ALL
            SELECT
                pp.criterion_id AS criterion_id,
                pp.adgroup_id as adgroup_id,
                pp.parent_id AS parent_id,
                pp.partition_type AS partition_type,
                pp.dimension_value AS dimension_value,
                a.dimension_heirarchy || pp.dimension_type::text,
                a.child_nodes || pp.criterion_id
            FROM product_partition AS pp
                JOIN ancestors AS a ON (
                    pp.parent_id = a.criterion_id AND
                    pp.adgroup_id = a.adgroup_id)
        )
        SELECT
            crit_key(criterion_id, adgroup_id) as crit_key,
            array_remove(
                dimension_heirarchy, NULL
            ) as subdivisions,
            array_upper(child_nodes, 1) AS depth
        FROM ancestors
        WHERE partition_type = 'Unit'
        ORDER BY child_nodes ASC;
END; $$

LANGUAGE 'plpgsql';


CREATE OR REPLACE VIEW adw_valid_segmentation_v AS (
    WITH constants AS (
        SELECT
            7 as MAX_DEPTH,
           50 as MAX_ADGROUPS
    ), dimension_types AS (
        SELECT distinct dimension_type
        FROM product_dimension
    ), crit_keys AS (
        SELECT crit_key
        FROM adw_divisible_product_group_v
    ), adgroups AS (
        SELECT distinct adgroup_id
        FROM crit_keys
            JOIN criterion USING(crit_key)
        LIMIT (SELECT MAX_ADGROUPS FROM constants)
    ), levels AS (
        -- figure out the level, for example
        -- PRODUCT_TYPE_L4 has level 4
        -- BRAND has level -1
        SELECT dimension_type,
            CASE WHEN right(dimension_type, 1) ~ '\d'
                    THEN left(dimension_type, length(dimension_type) - 1)
                ELSE dimension_type
            END AS dimension_hierarchy,
            CASE WHEN right(dimension_type, 1) ~ '\d'
                    THEN right(dimension_type, 1)::int
                ELSE -1
            END AS level
        FROM dimension_types
    ), segmentations as (
        SELECT
            crit_key,
            unnest(subdivisions) as dimension_type,
            depth
        FROM (
            SELECT
                --array_agg(adgroup_id) as adgroup_ids,
                (adw_existing_segmentations_fn(array_agg(adgroup_id))).*
            FROM adgroups
        ) q
    ), node_depth AS (
        SELECT distinct crit_key, depth
        FROM segmentations
            CROSS JOIN constants
        -- exclude partitions at max depth
        WHERE depth < MAX_DEPTH
    ), valid AS (
        SELECT
            crit_key,
            dt.dimension_type
        FROM crit_keys
            FULL JOIN dimension_types dt on (TRUE)
            LEFT JOIN segmentations s using (crit_key, dimension_type)
        WHERE s.dimension_type isnull
    )
    SELECT DISTINCT on (crit_key, dimension_hierarchy)
        crit_key,
        dimension_type,
        level
    FROM valid
        JOIN node_depth USING (crit_key)
        JOIN levels USING (dimension_type)
    -- in case of several levels pick the lowest one
    ORDER BY crit_key, dimension_hierarchy, level
);

