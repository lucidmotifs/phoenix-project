with recursive r(criterion_id, parent_criterion_id, cv, depth) as (
    select
        criterion_id, parent_criterion_id, '{}'::jsonb, 0
    from adw_product_group
    where parent_criterion_id is null
    union all
    select pg.criterion_id, pg.parent_criterion_id, cv || pg.case_value, depth+1
    from adw_product_group pg, r
    where pg.parent_criterion_id = r.criterion_id
)
select
    jsonb_build_object(
        coalesce(parent_criterion_id::text, 'root'),
        jsonb_object_agg(criterion_id, r.cv)
    ) as components
from r join product_partition using (criterion_id)
where adgroup_id = 71670483307
group by parent_criterion_id, depth
order by depth;