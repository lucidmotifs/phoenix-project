import sys
import os.path
import inspect
import fire
import itertools
import pprint as pp
from types import SimpleNamespace
from collections import ChainMap

import adspert.adwords.shopping as shopping

import bootstrap
import partitiontree


print('__main__ running')

test_accounts = {
    'name..': '4974458',
    'ebay': '3281857',
}


fns = inspect.getmembers(shopping, inspect.isfunction)
cls = inspect.getmembers(shopping, inspect.isclass)

namespace = tuple(filter(
    lambda c: c[1].__module__.endswith('shopping') and not c[0].startswith('_'),
    itertools.chain(fns, cls)))

funcs = {n: staticmethod(f) for n, f in fns}

account = bootstrap.bootstrap(4974458)
bootstrap.display_account_structure()

ns = SimpleNamespace(**dict(namespace))

pp.pprint(SimpleNamespace)

fire.Fire(partitiontree.create_partition_tree)

#
# 4974458
