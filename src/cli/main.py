import aenum
import itertools
import functools
import logging
import inspect
import json
import subprocess
import sys
# import textwrap
from typing import Any
from typing import Dict
from typing import List
from typing import Iterator

import fire
import pandas as pd
import tabulate
from google.protobuf.json_format import MessageToDict
from google.protobuf.json_format import MessageToJson

import adspert.googleads.shopping as gshopping
import adspert.googleads.client as gclient
import adspert.database.models.account as dbm
from adspert.googleads.customer import get_customer_manager_id
from adspert.googleads.schema import Resource
from adspert.googleads.enums import AdGroupCriterionStatusEnum
from adspert.googleads.enums import CriterionTypeEnum
from adspert.googleads.enums import AdvertisingChannelTypeEnum, AdGroupTypeEnum
from adspert.scripts.utils import get_account
from util import bootstrap
import os


ADS_PAGE_SIZE = 1000


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)

CriterionStatus = AdGroupCriterionStatusEnum.AdGroupCriterionStatus
CriterionType = CriterionTypeEnum.CriterionType
CampaignType = AdvertisingChannelTypeEnum.AdvertisingChannelType
AdGroupType = AdGroupTypeEnum.AdGroupType


class CommandAction(aenum.UniqueEnum):

    LIST = (1, 'ls')
    ADD = (2, 'mk')
    REMOVE = (3, 'rm')
    EDIT = (4, 'ch')


def render_data_with_paging(data: List[Dict[str, Any]]):
    out = tabulate.tabulate(data, headers='keys')

    pager = subprocess.Popen(
        ['less', '-F', '-R', '-S', '-X', '-K'],
        stdin=subprocess.PIPE,
        stdout=sys.stdout,
    )

    pager.stdin.write(out.encode())
    pager.stdin.close()
    pager.wait()


def framed(it: Iterator):
    """load messages into pandas dataframe from iterator."""
    msg = next(it)
    columns = list(MessageToDict(msg).keys())
    data = list(map(MessageToDict, itertools.chain([msg], it)))
    return pd.DataFrame(data, columns=columns)


def collect_fields(resource):
    fields = inspect.getmembers(dbm.Campaign, inspect.isdatadescriptor)
    return fields


class App(object):

    def __init__(self, account_id=None, login: str = None):

        self.client = gclient.GoogleAdsClient.load_from_storage()
        self.customer_id = account_id

        if not self.client.login_customer_id:
            self.client.login_customer_id = get_customer_manager_id(
                self.client, account_id,
            )

        print(f'logged in customer: {self.client.login_customer_id}')

        ads_service = self.client.get_service('GoogleAdsService')
        self.query = functools.partial(ads_service.search, str(account_id))
        self.mcc_query = functools.partial(
            ads_service.search,
            self.client.login_customer_id,
        )

class campaign:

    def __init__(self, client):
        # base
        self.client = client

    def sync(self):
        pass

    def ls(self):
        pass

    def update(self):
        pass


class customer:
    """customer management command group."""

    def __init__(self):
        pass


class bidding:

    def __init__(self, client):
        self.client = client

        # aliases
        self.st = self.strategy

    def strategy(self, bsid=None):

        gql = query.Query(**{
            'fields': ('id', 'name', 'type', 'status'),
            'resource': 'bidding_strategy',
        }).gql(neat=True)
        response = self.query(query=gql, page_size=10)
        msgs = [row.bidding_strategy for row in response]
        data = framed(msgs)

        render_data_with_paging(data)

        return data

    def adjustments(self, criteria=None):
        pass

    def modifiers(self, criteria=None):
        pass


class targeting:

    def __init__(self, client):
        # base
        self.client = client

    def dimensions(self, dimension_criteria):
        """Get criteria specified by targeting dimensions."""
        pass

    def structure(self, criteria, level):
        pass

    class criteria:

        types = {
            'campaign': (
                'AgeRange',
                'CustomAffinity',
                'Device',
                'Gender',
                'IncomeRange',
                'Keyword',
                'Language',
                'ListingScope',
                'Location',
            ),
            'adgroup': (
                'AgeRange',
                'Audience'
                'CustomAffinity',
                'CustomIntent',
                'Gender',
                'IncomeRange',
                'Keyword',
                'ListingGroup',
            ),
        }

        def __init__(self, level: str = 'campaign'):
            self.criterion = self.types[level.lower()]

        def structure(self):
            return


SCRIPT_DATA_DIR = 'adspert/src/adspert/scripts/'


class shopping:

    def __init__(self, client):
        # base
        self.client = client

    def criterion(self):
        return {
            'list': lambda: render_data_with_paging(self._get_product_partitions()),
            'help': lambda: print('shopping partitions'),
        }

    def product(self):
        # set-up...
        return {
            'groups': lambda: self.client.get_type('ListingGroupInfo'),
            'dimensions': lambda: self.client.get_type('ListingDimensionInfo'),
            'categories': {
                'load': ProductCategoryTaxonomy.load(
                            data=os.path.join(SCRIPT_DATA_DIR,
                                'category-taxonomy.en.csv')),
                'view': ProductCategoryTaxonomy.show(),
                'find': ProductCategoryTaxonomy.find,
            }
        }

    def inventory(self, adgroup=None, campaign=None, dimensions=()):
        """Shopping product Inventory"""
        import adspert.database.models.account as model

        product_model = dbm.Product
        pdimenion_model = dbm.ProductDimension

        # return commands
        return {
            'display': lambda: print('google_shopping.get_campaign_inventory()')
        }

    def _get_product_partitions(self, customer_id=''):
        customer_id = str(customer_id or self.client.customer_id)
        query = """
            SELECT ad_group.id,
              ad_group_criterion.criterion_id,
              ad_group_criterion.type,
              ad_group_criterion.negative,
              ad_group_criterion.listing_group.parent_ad_group_criterion,
              ad_group_criterion.listing_group.type,
              ad_group_criterion.listing_group.case_value.product_bidding_category.id,
              ad_group_criterion.listing_group.case_value.product_bidding_category.level,
              ad_group_criterion.listing_group.case_value.product_bidding_category.country_code,
              ad_group_criterion.listing_group.case_value.product_brand.value,
              ad_group_criterion.listing_group.case_value.product_custom_attribute.index,
              ad_group_criterion.listing_group.case_value.product_custom_attribute.value,
              ad_group_criterion.listing_group.case_value.product_type.level,
              ad_group_criterion.listing_group.case_value.product_type.value,
              ad_group_criterion.listing_group.case_value.product_channel.channel,
              ad_group_criterion.listing_group.case_value.product_condition.condition,
              ad_group_criterion.listing_group.case_value.product_item_id.value
            FROM ad_group_criterion
            WHERE ad_group_criterion.type = 'LISTING_GROUP'
            ORDER BY ad_group.id
            LIMIT 1000
        """
        response = self.client.run_query(query, customer_id)
        criterion = (row.ad_group_criterion for row in response)
        data = framed(criterion)
        return data


class Adspert:
    """Run Adspert code.

    Will initialize db connections and expose access to internal logic.

    """

    def __init__(self, production=False):

        state = json.load(open('.adspert', 'r+'))
        print(state)
        if state:
            bootstrap.bootstrap(state['adspert_id'])
            account = get_account(state['account_id'])
            client = self.client = gclient.get_client(account)

        self.campaign = campaign(client)
        self.bidding = bidding(client)
        self.shopping = shopping(client)
        self.targeting = targeting(client)

    def _testing(self):
        return {
            'test': lambda: print('hello there'),
        }


def init(
    account_id: str = None,
    adspert_id: int = None,
    context: str = 'google',
) -> None:
    """Initialize environment."""
    with open('.adspert', 'w') as fp:
        json.dump({
            'account_id': account_id,
            'adspert_id': adspert_id,
            'context': context,
        }, fp)

    print('environment initialized')


if __name__ == '__main__':
    app = App()
    fire.Fire({
        'init': init,
        'bidding': bidding,
        'campaign': campaign,
        'customer': customer,
        'shopping': shopping,
        'targeting': targeting,
        'adspert': Adspert,
    })

