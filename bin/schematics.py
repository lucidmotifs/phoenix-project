import schematics
from google.protobuf.wrappers_pb2 import Int64Type, StringType
from pydantic import BaseModel


class Int64ValueType(IntType):
	def __init__(self, value=None, *args, **kwargs):
 	  	super(Int64ValueType, self).__init__(*args, **kwargs)
        self.value = value


class StringValueType(StringType):
	def __init__(self, value=None, *args, **kwargs):
 	  	super(StringValueType, self).__init__(*args, **kwargs)
        self.value = value


class Campaign(Model):
	id: Int64Value = Int64ValueType()
	name: StringValue = StringValueType()
