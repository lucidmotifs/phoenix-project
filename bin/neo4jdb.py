import aenum
import devtools
import pprint
from aenum import NamedTuple
from py2neo import Graph
from py2neo.ogm import  GraphObject
from py2neo.data import Record, Walkable, walk, Table, Path


DEFAULT_HOST = "192.168.1.107"


class GraphDbURI(NamedTuple):
    scheme: str = (0, "the uri scheme", "bolt")
    host: str = (1, "db instance host", DEFAULT_HOST)
    port: int = (2, "db instance port", 7687)

    def __str__(self):
        return f"{self[0]}://{self.host}:{self.port}"


GraphDbAuth = aenum.NamedTuple("GraphDbAuth", ["user", "password"], module=__name__)
GraphDbConfig = aenum.NamedTuple(
    "GraphDbConfig", ["name", "port", "user", "password", "secure"], module=__name__
)


class GraphDb(aenum.Enum):
    """Graph DB instance enumeration."""

    DEVEL = ("neodb1", 36429, "neo4j", "dev", False)
    META = ("graphdb", 42059, "neo4j", "dev", False)

    @property
    def config(self) -> GraphDbConfig:
        return GraphDbConfig(*self.value)

    @property
    def auth(self) -> GraphDbAuth:
        cfg = self.config
        return GraphDbAuth(cfg.user, cfg.password)

    @property
    def uri(self):
        nt = GraphDbURI(port=self.config.port)
        return str(nt)


uri_1 = GraphDbURI("bolt")
gdb_1 = GraphDb.META.config


def connect(instance_label: str) -> Graph:
    gdb = GraphDb[instance_label.upper()]
    g =  Graph(gdb.uri, auth=gdb.auth)
    return g

def entities(g: Graph):
    schema = g.schema
    labels = list(sorted(schema.node_labels))
    rels = list(sorted(schema.relationship_types))
    return dict(names=labels, types=rels)


def info(g: Graph, label: str, rel: str = None):
    summary = g.call("dbms.components")
    matcher = g.nodes
    related = g.relationships
    devtools.debug(summary)

    schema = g.schema
    a = label or entities(g).get('names')[-1]
    
    devtools.debug('Label: %s' % a)
    print('Node Count:', len(matcher))
    print('Rel. Count:', len(related))
    
    devtools.debug(schema.node_labels)
    devtools.debug(schema.relationship_types)
    devtools.debug(schema.get_indexes(a))
    devtools.debug(schema.get_uniqueness_constraints(a))
    
    nodes = matcher.match(a).limit(100).all()
    matches = map(lambda x: related.match(set([x]), r_type=rel).limit(10), nodes)

    records = [Record(n) for n in nodes]
    table = Table(records, keys=records[0].keys())
    print(table)

    for m in matches:
        path = Path(Walkable(m))
        if path.relationships:
            print(path.start_node.identity, len(path.relationships))
            for r in path.relationships:
                print(r)
        else:
            print(path.start_node.identity)
            for n in path.nodes:
                print(n)


def indexes(label: str, *props: str):
    g = connect('meta')
    indexes = frozenset(g.schema.get_indexes(label))
    props = frozenset(props)
    if props and props ^ indexes:
        pprint.pp(props, indexes)
        r = g.schema.create_index(label, props - indexes)
        devtools.debug(r)
    devtools.debug(g.schema.get_indexes(label))
    if indexes & props:
        return indexes & props
    else:
        return list(g.nodes.match(label).first().keys())
    

def _resolver(socket_address):

    if socket_address == ("gdb.phoenix.dev", 9999):
        yield "::1", 7687
        yield "127.0.0.1", 7687
    else:
        from socket import gaierror

        raise gaierror("Unexpected socket address %r" % socket_address)


from functools import partial
import fire

g = connect('META')
labels = entities(g).get('names')
fns = {l: partial(info, g, l) for l in labels}
# nodes.match("Person", name="Keanu Reeves").first()

fire.Fire({
    'schema': {'index': indexes},
    'labels': fns,
    'sch': g.schema,
    'grph': g,
    'nodes': lambda label, **kwargs: g.nodes.match(label, **kwargs),
})
